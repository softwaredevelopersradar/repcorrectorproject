﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using SDS;
using System.Runtime.InteropServices;
using System.IO.Ports;
using System.Threading;

namespace CNT
{

    [StructLayout(LayoutKind.Sequential, Pack = 1)] // size = 3
    public struct MainFiel
    {
        public byte Address;
        public byte cipher;
        public byte FieldLength;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct LastPartAnswer
    {
        public byte ErrorCode;
        public ushort CRC;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SpecialPartAnswer
    {
        public byte T;
        public byte H;
        public byte Charge;
        public ushort CRC;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RequestPPRCH
    {

        public byte Step;

        public byte NumberFreq;

        public Int16 SeltusFreq;

        public byte Duration;

    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RequestFielFreq
    {
        public byte NumberFreq;

        public byte Step;

        public byte Duration;

    }

    public struct Answer
    {
        public MainFiel mainFiel;
        public object obj;
    }

    public struct StructReceiveHeter
    {
        public byte Address;
        public byte cipher;
        public byte FieldLength;

        public byte ErrorCode;

        public byte T;
        public byte H;
        public byte Charge;
        public ushort CRC;
    }

    public struct StructSendHeter
    {
        public Int32 frequency;
        public byte Code;
        public byte numberFreq;
        public byte step;
        public Int16 saltusFreq;
        public byte time;

        public StructSendHeter(Int32 frequency, byte Code, byte numberFreq, byte step, Int16 saltusFreq, byte time)
        {
            this.frequency = frequency;
            this.Code = Code;
            this.numberFreq = numberFreq;
            this.step = step;
            this.saltusFreq = saltusFreq;
            this.time = time;
        }
    }

    public class ComHTRD
    {
        private const byte LEN_HEAD = 3;


        private SerialPort _port;
        private Thread thrRead;
        const byte AddressToGen = 0x12;
        private const byte AddressToComp = 0x21;
        public const byte OffSignal = 0x10;
        public const byte FRCH = 0x21;
        public const byte AM = 0x31;
        public const byte FM = 0x41;
        public const byte CHM = 0x51;
        public const byte FielFreq = 0x61;
        public const byte PPRCH = 0x71;
        public const byte Answer = 0xAA;
        public const byte SpecialCipher = 0xF1;
        public byte[] ListCodegram = { FRCH, AM, FM, CHM };
        public byte[] listError = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x0D, 0x10, 0x11 };
        public const byte NotError = 0x00;
        public const byte ErrorCRC = 0x04;
        private static byte durationRadiation = 7; // время излучения сигнала, в течение этого времени все равно будут отправляться кдг
        private byte bErrorSend = 0; //переменная для хранения повторяющейся ошибки. Было решено, 
        //что при ошибке CRC генератор необходимо продолжать опрос, а завершать только в том случае, 
        //когда количество отправленных запросов превышает некоторую константу

        #region SpecialSend
        private static System.Threading.Timer tmSpecialSend;
        private static int iCurrentCountSend = 0;
        private static int iMaxCountSend = 10;
        private static ushort usTimeDelayHeter = 200;
        private static ushort usTimeSendHeter = 500;
        StructSendHeter item;
        #endregion

        #region Delegates

        public delegate void ConnectEventHandler();
        public delegate void ByteEventHandler(byte[] bByte);
        public delegate void CmdEventHandler(object obj);
        public delegate void CmdCommonEventHandler(StructReceiveHeter receive);
        public delegate void DelStopSend(byte error);
        
        #endregion

        #region Events


        public static event ConnectEventHandler OnConnectPort;
        public static event ConnectEventHandler OnDisconnectPort;

        public static event ByteEventHandler OnReadByte;
        public static event ByteEventHandler OnWriteByte;


       // public event CmdEventHandler OnReceiveCmd;
        public static event CmdCommonEventHandler OnReceinCommonCmd;
        public static event CmdCommonEventHandler OnReceiveCmdSpec;
        public static event CmdCommonEventHandler OnReceiveCmdOff;
        public static event CmdCommonEventHandler OnReceiveCmdOnSignal;
        public static event CmdCommonEventHandler OnReceiveCmdError;

        public static event DelStopSend eventStopSend; //завершение отправки запроса
        #endregion

        private byte[] SerializationFreq(Int32 frequency)
        {
            byte[] sendPPRCH = new byte[3];
            sendPPRCH[0] = (byte)(frequency >> 16);
            sendPPRCH[1] = (byte)(frequency >> 8);
            sendPPRCH[2] = (byte)frequency;
            return sendPPRCH;
        }

        object FindCmd(ref byte[] buffer, ref int index)
        {
            if (index >= 6)
            {
                MainFiel mainFiel = new MainFiel();
                mainFiel.Address = buffer[0];
                mainFiel.cipher = buffer[1];
                mainFiel.FieldLength = buffer[2];
                if (mainFiel.Address != AddressToComp)
                {
                    DellArray(ref buffer, 1);
                    index--;
                    return null;
                }
                else
                {
                    switch (mainFiel.cipher)
                    {
                        case OffSignal:
                        case FRCH:
                        case AM:
                        case FM:
                        case CHM:
                        case FielFreq:
                        case PPRCH:
                            if (index >= 6)
                            {
                                object answer1 = new LastPartAnswer();
                                byte[] bufAnswer = new byte[mainFiel.FieldLength];
                                Array.Copy(buffer, 3, bufAnswer, 0, mainFiel.FieldLength);
                                SdS.Deserialization(bufAnswer, ref answer1);
                                Answer comAnswer = new Answer();
                                comAnswer.mainFiel = mainFiel;
                                comAnswer.obj = answer1;
                                DellArray(ref buffer, 6);
                                index -= 6;
                                return comAnswer;//6 byte
                            }
                            break;
                        case SpecialCipher:
                            if (index >= 8)
                            {
                                object answer1 = new SpecialPartAnswer();
                                byte[] bufAnswer = new byte[mainFiel.FieldLength];
                                Array.Copy(buffer, 3, bufAnswer, 0, mainFiel.FieldLength);
                                SdS.Deserialization(bufAnswer, ref answer1);
                                Answer comAnswer = new Answer();
                                comAnswer.mainFiel = mainFiel;
                                comAnswer.obj = answer1;
                                DellArray(ref buffer, 8);
                                index -= 8;
                                return comAnswer;//8 byte
                            }
                            break;
                        default:
                            DellArray(ref buffer, 2);
                            index -= 2;
                            return null;
                        //break;
                    }
                }
            }
            return null;
        }

        public void DellArray(ref byte[] arrayBytes, int length)
        {

            Array.Clear(arrayBytes, 0, length);
            arrayBytes.Reverse();
            Array.Resize(ref arrayBytes, (arrayBytes.Length - length));
            arrayBytes.Reverse();

        }

        public string FindErr(byte error)
        {
            string messError = "";
            switch (error)
            {
                case 0x00:
                    messError = "Нет ошибки";
                    return messError;
                //break;
                case 0x01:
                    messError = "Ошибка программная неопределенная";
                    break;
                case 0x02:
                    messError = "Ошибка аппаратная неопределенная";
                    break;
                case 0x03:
                    messError = "Ошибка CRC модем";
                    break;
                case 0x04:
                    messError = "Ошибка CRC генератор";
                    break;
                case 0x05:
                    messError = "Ошибка CRC формирователь";
                    break;
                case 0x06:
                    messError = "Ошибка ввода данных";
                    break;
                case 0x0D:
                    messError = "Нет связи с Формирователем";
                    break;
                case 0x10:
                    messError = "ПР - на ЖК при приеме с ПК (внутренние для ЖК)";
                    break;
                case 0x11:
                    messError = "ПРД - на ЖК при передаче на ПК (внутренние для ЖК)";
                    break;
            }
            return messError;
        }

        public void Connect(string ComPortName, Int32 ComSpeed)
        {
            OpenPort(ComPortName, ComSpeed, Parity.None, 8, StopBits.One);
        }
        public void OpenPort(string portName, Int32 baudRate, System.IO.Ports.Parity parity, Int32 dataBits, System.IO.Ports.StopBits stopBits)
        {
            // Open COM port

            if (_port == null)
                _port = new SerialPort();

            // if port is open
            if (_port.IsOpen)

                // close it
                ClosePort();

            // try to open 
            try
            {
                // set parameters of port
                _port.PortName = portName;
                _port.BaudRate = baudRate;

                _port.Parity = parity;
                _port.DataBits = dataBits;
                _port.StopBits = stopBits;

                _port.RtsEnable = true;
                _port.DtrEnable = true;

                //_port.ReadTimeout = 1000;
                //_port.WriteTimeout = 1000;

                _port.ReceivedBytesThreshold = 1000;
                // set parameters of port
                /*  _port.PortName = portName;
                  _port.BaudRate = baudRate;

                  _port.Parity = System.IO.Ports.Parity.None;
                  _port.DataBits = 8;
                  _port.StopBits = System.IO.Ports.StopBits.One;*/


                // open it
                _port.Open();

                // create the thread for reading data from the port
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // load function of the thread for reading data from the port
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (System.Exception)
                {

                }

                // raise event
                // ConnectPort();

                // return true;
                if (OnConnectPort != null)
                    OnConnectPort();
            }
            catch (System.Exception)
            {
                if (OnDisconnectPort != null)
                    OnDisconnectPort();

                //return false;
            }
        }

        public bool PortConnect()
        {
            try
            {
                if (_port.IsOpen)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public void ClosePort()
        {
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
            }
            catch (System.Exception ex)
            {
                ex.GetBaseException();
            }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch (System.Exception ex)
            {
                ex.GetBaseException();
            }

            try
            {
                // close port
                _port.Close();

                // destroy thread of reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                if (OnDisconnectPort != null)
                    // raise event
                    OnDisconnectPort();
            }

            catch (System.Exception ex)
            {
                ex.GetBaseException();
            }
        }
        
        // Read data (byte) from port (thread)
        private void ReadData()
        {
            // buffer for reading bytes
            byte[] bBufRead;
            bBufRead = new byte[256];

            // buffer to transfer and save reading bytes
            byte[] bBufSaveS = null;
            //bBufSaveS = new byte[200];

            // length of reading bytes
            int iReadByte = -1;
            int iBufSave = 0;

            // while port open
            while (true)
            {
                try
                {
                    bBufRead = new byte[256];

                    // clear burre of reading
                    Array.Clear(bBufRead, 0, bBufRead.Length);

                    iReadByte = 0;

                    // read data
                    iReadByte = _port.Read(bBufRead, 0, bBufRead.Length);

                    // if bytes was read
                    if (iReadByte > 0)
                    {
                        Array.Resize(ref bBufRead, iReadByte);

                        if (OnReadByte != null)
                            OnReadByte(bBufRead);

                        if (bBufSaveS == null)
                            bBufSaveS = new byte[iReadByte];
                        else
                            Array.Resize(ref bBufSaveS, bBufSaveS.Length + iReadByte);


                        Array.Copy(bBufRead, 0, bBufSaveS, bBufSaveS.Length - iReadByte, iReadByte);
                        iBufSave += iReadByte;
                        try
                        {
                            while (iBufSave >= 6)
                            {
                                MainFiel mainFiel = new MainFiel();
                                mainFiel.Address = bBufSaveS[0];
                                mainFiel.cipher = bBufSaveS[1];
                                mainFiel.FieldLength = bBufSaveS[2];

                                object answers = null;
                                Answer answer = new Answer();

                                if (iBufSave >= LEN_HEAD + mainFiel.FieldLength)
                                {
                                    byte[] bData = new byte[LEN_HEAD + mainFiel.FieldLength];

                                    Array.Copy(bBufSaveS, 0, bData, 0, LEN_HEAD + mainFiel.FieldLength);
                                    int iLen = LEN_HEAD + mainFiel.FieldLength;
                                    answers = FindCmd(ref bData, ref iLen);

                                    Array.Reverse(bBufSaveS);
                                    Array.Resize(ref bBufSaveS, bBufSaveS.Length - (LEN_HEAD + mainFiel.FieldLength));
                                    Array.Reverse(bBufSaveS);

                                    iBufSave -= (LEN_HEAD + mainFiel.FieldLength);

                                    if (answers != null)
                                        answer = (Answer)answers;


                                    if ((object)answer != null)
                                    {
                                        FuncReceiveCMD(answer);

                                        /*if (OnReceiveCmd != null)
                                        {
                                            OnReceiveCmd((object)answer);
                                        }*/
                                    }
                                }
                                else
                                {
                                    Array.Reverse(bBufSaveS);
                                    Array.Resize(ref bBufSaveS, 0);
                                    Array.Reverse(bBufSaveS);
                                    iBufSave = 0;
                                }
                            }

                        }
                        catch (Exception e) 
                        {
                            var a = e;
                        }
                    }

                    else
                        if (OnDisconnectPort != null)
                            OnDisconnectPort();
                }

                catch (System.Exception)
                {

                    ClosePort();

                }

                Thread.Sleep(5);
            }
        }


        void FuncReceiveCMD(Answer answer)
        {
            var structReceive = ChangeTypeAnswe(answer);

            if (OnReceinCommonCmd != null)
            {
                OnReceinCommonCmd(structReceive);
            }
            if (structReceive.ErrorCode != 0x00)
            {
                bErrorSend = structReceive.ErrorCode;
                if (OnReceiveCmdError != null)
                    OnReceiveCmdError(structReceive);
                return;
            }
            switch (structReceive.cipher) //(answer.mainFiel.cipher)
            {
                case SpecialCipher:

                    if (OnReceiveCmdSpec != null)
                    {
                        OnReceiveCmdSpec(structReceive);
                    }
                    break;
                case FRCH: case FielFreq:

                    if (OnReceiveCmdOnSignal != null)
                    {
                        OnReceiveCmdOnSignal(structReceive);
                    }
                    break;
                case OffSignal:
                    if (OnReceiveCmdOff != null)
                    {
                        OnReceiveCmdOff(structReceive);
                    }
                    break;
            }
        }

        public bool Send(Int32 frequency, byte Code, byte numberFreq, byte step, Int16 saltusFreq, byte time) // length information  5 byte
        {
            int i = 0;
            byte[] freq = SerializationFreq(frequency);
            //RequestCipher = Code;

            switch (Code)
            {
                case FRCH:
                case AM:
                case FM:
                case CHM:
                    MainFiel mainFiel;
                    mainFiel.Address = AddressToGen;
                    mainFiel.cipher = Code;
                    mainFiel.FieldLength = 6; // было 5
                    byte[] send = SdS.Serialization(mainFiel);
                    i = send.Length;
                    Array.Resize(ref send, send.Length + freq.Length);
                    Array.Copy(freq, 0, send, i, freq.Length);
                    i = send.Length;
                    Array.Resize(ref send, send.Length + 1);
                    send[i] = time;
                    byte[] crc = CRC16(send, (ushort)(mainFiel.FieldLength + 1));
                    i = send.Length;
                    Array.Resize(ref send, send.Length + crc.Length);
                    send[i++] = crc[1];
                    send[i] = crc[0];
                    return WriteData(send);
                //break;
                case FielFreq:
                    RequestFielFreq requestFileFreq;
                    MainFiel mainFielFreq;
                    mainFielFreq.Address = AddressToGen;
                    mainFielFreq.cipher = Code;
                    mainFielFreq.FieldLength = 8;//было 7
                    byte[] sendFielFreq = SdS.Serialization(mainFielFreq);
                    i = sendFielFreq.Length;
                    Array.Resize(ref sendFielFreq, sendFielFreq.Length + freq.Length);
                    Array.Copy(freq, 0, sendFielFreq, i, freq.Length);
                    requestFileFreq.NumberFreq = numberFreq;
                    requestFileFreq.Step = step;
                    requestFileFreq.Duration = time;
                    byte[] PartFileFreq = SdS.Serialization(requestFileFreq);
                    i = sendFielFreq.Length;
                    Array.Resize(ref sendFielFreq, sendFielFreq.Length + PartFileFreq.Length);
                    Array.Copy(PartFileFreq, 0, sendFielFreq, i, PartFileFreq.Length);
                    byte[] crcFielFreq = CRC16(sendFielFreq, (ushort)(mainFielFreq.FieldLength + 1));
                    i = sendFielFreq.Length;
                    Array.Resize(ref sendFielFreq, sendFielFreq.Length + crcFielFreq.Length);
                    sendFielFreq[i++] = crcFielFreq[1];
                    sendFielFreq[i] = crcFielFreq[0];
                    return WriteData(sendFielFreq);
                case PPRCH:
                    RequestPPRCH requestPPRCH;
                    MainFiel mainFielPPRCH;
                    mainFielPPRCH.Address = AddressToGen;
                    mainFielPPRCH.cipher = PPRCH;
                    mainFielPPRCH.FieldLength = 10; // было 9
                    byte[] sendPPRCH = SdS.Serialization(mainFielPPRCH);
                    i = sendPPRCH.Length;
                    Array.Resize(ref sendPPRCH, sendPPRCH.Length + freq.Length);
                    Array.Copy(freq, 0, sendPPRCH, i, freq.Length);
                    requestPPRCH.NumberFreq = numberFreq;
                    requestPPRCH.Step = step;
                    requestPPRCH.SeltusFreq = saltusFreq;
                    requestPPRCH.Duration = time;
                    byte[] Part = SdS.Serialization(requestPPRCH);
                    i = sendPPRCH.Length;
                    Array.Resize(ref sendPPRCH, sendPPRCH.Length + Part.Length);
                    Array.Copy(Part, 0, sendPPRCH, i, Part.Length);
                    byte bFreqsaltus = sendPPRCH[sendPPRCH.Length - 4];
                    sendPPRCH[sendPPRCH.Length - 4] = sendPPRCH[sendPPRCH.Length - 5];
                    sendPPRCH[sendPPRCH.Length - 5] = bFreqsaltus;
                    byte[] crcPPRCH = CRC16(sendPPRCH, (ushort)(mainFielPPRCH.FieldLength + 1));
                    i = sendPPRCH.Length;
                    Array.Resize(ref sendPPRCH, sendPPRCH.Length + 2);
                    sendPPRCH[i++] = crcPPRCH[1];
                    sendPPRCH[i] = crcPPRCH[0];
                    return WriteData(sendPPRCH);
                //break;
                case OffSignal:
                case SpecialCipher:
                    MainFiel requestSpecial;
                    requestSpecial.Address = AddressToGen;
                    requestSpecial.cipher = Code;
                    requestSpecial.FieldLength = 2;
                    byte[] sendSpecial = SdS.Serialization(requestSpecial);
                    byte[] crcSpecial = CRC16(sendSpecial, (ushort)(requestSpecial.FieldLength + 1));
                    i = sendSpecial.Length;
                    Array.Resize(ref sendSpecial, sendSpecial.Length + crcSpecial.Length);
                    sendSpecial[i++] = crcSpecial[1];
                    sendSpecial[i] = crcSpecial[0];
                    return WriteData(sendSpecial);
                //break;
                default:
                    return false;
                //break;
            }

        }
        
        private bool WriteData(byte[] bSend)
        {
            try
            {
                _port.Write(bSend, 0, bSend.Length);

                if (OnWriteByte != null)
                    OnWriteByte(bSend);

                return true;
            }
            catch (System.Exception)
            {
                if (OnDisconnectPort != null)
                    OnDisconnectPort();
                return false;
            }

        }

        public byte[] CRC16(byte[] Buf, ushort Len)
        {
            const ushort polinom = 0x1021;
            ushort Result = 0xFFFF;

            for (int i = 0; i < Len; ++i)
            {
                Result ^= (ushort)(Buf[i] << 8);
                for (uint j = 0; j < 8; ++j)
                {
                    Result >>= 1;
                    if ((Result & 0x01) != 0) Result ^= polinom;
                }
            }
            byte[] result = BitConverter.GetBytes(Result);
            return result;
        }

        public static StructReceiveHeter ChangeTypeAnswe(object answer)
        {
            StructReceiveHeter StructAnswer = new StructReceiveHeter();
            StructAnswer.Address = ((Answer)answer).mainFiel.Address;
            StructAnswer.cipher = ((Answer)answer).mainFiel.cipher;
            StructAnswer.FieldLength = ((Answer)answer).mainFiel.FieldLength;
            try
            {
                StructAnswer.ErrorCode = ((LastPartAnswer)((Answer)answer).obj).ErrorCode;
                StructAnswer.CRC = ((LastPartAnswer)((Answer)answer).obj).CRC;
                StructAnswer.Charge = 255;
                StructAnswer.H = 255;
                StructAnswer.T = 255;
            }
            catch (InvalidCastException)
            {
                StructAnswer.ErrorCode = 0;
                StructAnswer.CRC = ((SpecialPartAnswer)((Answer)answer).obj).CRC;
                StructAnswer.Charge = ((SpecialPartAnswer)((Answer)answer).obj).Charge;
                StructAnswer.H = ((SpecialPartAnswer)((Answer)answer).obj).H;
                StructAnswer.T = ((SpecialPartAnswer)((Answer)answer).obj).T;
            }
            catch (Exception)
            {

            }
            return StructAnswer;
        }

        public static double ADC_battery(byte charge)
        {
            double adc_battery = (16.8d * charge) / 155.0d;
            return Math.Round(adc_battery, 2);
        }


        public void SpecialSend(Int32 frequencyKHz, byte Code, byte numberFreq, byte stepMHz, Int16 saltusFreq, byte time)
        {
            bErrorSend = 0;
            item = new StructSendHeter(frequencyKHz, Code, numberFreq, stepMHz, saltusFreq, time);
            tmSpecialSend = new System.Threading.Timer(TimeAutoSentByteHeter, item, usTimeDelayHeter, usTimeSendHeter);
        }

        private void TimeAutoSentByteHeter(object o)
        {
            Send(item.frequency, item.Code, item.numberFreq, item.step, item.saltusFreq, item.time);
            iCurrentCountSend++;
            if (iCurrentCountSend >= iMaxCountSend)
            {
                tmSpecialSend.Dispose();
                if (eventStopSend != null)
                    eventStopSend(bErrorSend);
                iCurrentCountSend = 0;
            }

        }

        public void StopSend()
        {
            if (tmSpecialSend != null)
                tmSpecialSend.Dispose();
            iCurrentCountSend = 0;
        }

        public static int MaxCountSend
        {
            get { return iMaxCountSend; }
            set { iMaxCountSend = value; }
        }

        public static ushort TimeDelayHeter
        {
            get { return usTimeDelayHeter; }
            set { usTimeDelayHeter = value; }
        }

        public static ushort TimeSendHeter 
        {
            get { return usTimeSendHeter; }
            set { usTimeSendHeter = value; }
        }

        public static byte DurationRadiation
        {
            get { return durationRadiation; }
            set
            { durationRadiation = value; }
        }

        public void OFFSignal()
        {
            StopSend();
            SpecialSend(0, ComHTRD.OffSignal, 0, 0, 0, 0);
        }

        public void TestAir()
        {
            StopSend();
            SpecialSend(0, ComHTRD.SpecialCipher, 0, 0, 0, 0);
        }

        public void OnFRCH(int freqKHz)
        {
            StopSend();
            SpecialSend(freqKHz, ComHTRD.FRCH, 0, 0, 0, durationRadiation);
        }

        public void OnFielFreq(int freqStartKHz, int freqStopMHz, byte step, bool IncludeShiftHeter )
        {
            StopSend();
            byte NumfreqMHz = (byte)((freqStopMHz - freqStartKHz) / (step * 1000));
            int freqCentr = (freqStopMHz + freqStartKHz) / 2;
            if (IncludeShiftHeter == true)
                freqCentr -= (1000 * step / 2);
            SpecialSend(freqCentr, ComHTRD.FielFreq, NumfreqMHz, step, 0, durationRadiation);
        }
    }
}
