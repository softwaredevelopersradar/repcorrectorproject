﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Ini;
using System.Xml;
using CNT; // namespace ComHTRD
using System.IO.Ports;

namespace Corrector
{
    partial class FormCorrector
    {
        ComHTRD ClientHeterodyn;
        System.Threading.Timer tmWriteHeter;
        System.Threading.Timer tmReadHeter;
        static ushort usTimeIndicateHeter = 300;
        string sTemp = "", sHumidid = "", sCharge = "";
        int? iRestartHeter = null;
        double? dCountBadFreqNull = null;

        void InitHeterodyn()
        {
            if (ComPortName.SelectedIndex < 0)
            {
                MessageBox.Show(TranslateDic[ExeptionChoicePortHeter]);
                ComPortName.Focus();
                return;
            }

            ClientHeterodyn = new ComHTRD();
            EventHeter();
            ClientHeterodyn.Connect((string)ComPortName.SelectedItem, Convert.ToInt32(ComSpeed.SelectedItem));

        }

        void EventHeter()
        {
            ComHTRD.OnConnectPort += ShowConnectHeter;
            ComHTRD.OnDisconnectPort += ShowDisconnectHeter;
            ComHTRD.OnWriteByte += ShowWriteByteHeter;
            ComHTRD.OnReadByte += ShowReadByteHeter;
            ComHTRD.OnReceiveCmdSpec += ShowReceiveAirTest; // событие на приход специального сообщения (Тест радиоканала) 
        }

        void GetSettingsHeter()
        {
            try
            {
                GetPortName(null, new EventArgs());
                //if (IniSettings.IniReadValue("Heterodyn", "TimeSend") != "")
                //    ComHTRD.TimeSendHeter = Convert.ToUInt16(IniSettings.IniReadValue("Heterodyn", "TimeSend"));
                //if (IniSettings.IniReadValue("Heterodyn", "MaxCountSend") != "")
                //    ComHTRD.MaxCountSend = Convert.ToUInt16(IniSettings.IniReadValue("Heterodyn", "MaxCountSend"));
                //if (IniSettings.IniReadValue("Heterodyn", "DurationRadiation") != "")
                  //  ComHTRD.DurationRadiation = Convert.ToByte(IniSettings.IniReadValue("Heterodyn", "DurationRadiation"));
            }
            catch { }
        }

        void GetPortName(object sender, EventArgs e)
        {
            ComPortName.Items.Clear();
            string[] portnames = SerialPort.GetPortNames();
            ComPortName.Items.AddRange(portnames);

            if (IniSettings.IniReadValue("Heterodyn", "Com") != "")
                ComPortName.SelectedIndex = ComPortName.FindString(IniSettings.IniReadValue("Heterodyn", "Com"));
            ComPortName.SelectedIndex = (ComPortName.SelectedIndex == -1) ? 0 : ComPortName.SelectedIndex;
            if (IniSettings.IniReadValue("Heterodyn", "Speed") != "")
                ComSpeed.SelectedIndex = ComSpeed.FindString(IniSettings.IniReadValue("Heterodyn", "Speed"));
        }

        void ConnectDisonnectToHeter()
        {
            if (ClientHeterodyn != null)
            {
                ClientHeterodyn.ClosePort();
                ClientHeterodyn = null;
            }
            else
            {
                InitHeterodyn();
                iRestartHeter++;
            }
        }

        void DisconnectHeter()
        {
            if (ClientHeterodyn != null)
            {
                ClientHeterodyn.ClosePort();
                ClientHeterodyn = null;
            }
        }

        void ShowReceiveAirTest(StructReceiveHeter StructAnswer)
        {
            ClientHeterodyn.StopSend();
            sTemp = StructAnswer.T.ToString() + " " + "\u00B0" + "C";
            sHumidid = StructAnswer.H.ToString() + " " + "\u0025";
            double dCharge = ComHTRD.ADC_battery(StructAnswer.Charge);
            sCharge = dCharge.ToString() + " " + "V";
            Invoke((MethodInvoker)(() => LabTempHeter.Text = TranslateDic[LabTempHeter.Name] + "\n" + sTemp));
            Invoke((MethodInvoker)(() => LabHumidHeter.Text = TranslateDic[LabHumidHeter.Name] + "\n" + sHumidid));
            Invoke((MethodInvoker)(() => LabChargeHeter.Text = TranslateDic[LabChargeHeter.Name] + "\n" + sCharge));
            Invoke((MethodInvoker)(() => dCharge = (int)Math.Round(dCharge * 10) > ProgressChargeHeter.Maximum  ? ProgressChargeHeter.Maximum / 10.0d : dCharge));
             Invoke((MethodInvoker)(() => dCharge = (int)Math.Round(dCharge * 10) < ProgressChargeHeter.Minimum ? ProgressChargeHeter.Minimum / 10.0d : dCharge));
             Invoke((MethodInvoker)(() => ProgressChargeHeter.Value = (int)Math.Round(dCharge * 10)));
        }

        void ShowConnectHeter()
        {
            ShowColorButton(ButHeterConnect, Color.Green);
            IniSettings.IniWriteValue("Heterodyn", "Com", ComPortName.Items[ComPortName.SelectedIndex].ToString());
            IniSettings.IniWriteValue("Heterodyn", "Speed", ComSpeed.Items[ComSpeed.SelectedIndex].ToString());
        }

        void ShowDisconnectHeter()
        {
            ClientHeterodyn = null;
            ShowColorButton(ButHeterConnect, Color.Maroon);
        }

        void ShowWriteByteHeter(byte[] WriteByte)
        {
            Invoke((MethodInvoker)(() => PicBoxHeterWrite.Image = Properties.Resources.green));
            Invoke((MethodInvoker)(() => tmWriteHeter = new System.Threading.Timer(TimerWriteCallBackHeter, null, usTimeIndicateHeter, 0)));
        }

        void ShowReadByteHeter(byte[] ReadByte)
        {
            Invoke((MethodInvoker)(() => PicBoxHeterRead.Image = Properties.Resources.red));
            Invoke((MethodInvoker)(() => tmReadHeter = new System.Threading.Timer(TimerReadCallBackHeter, null, usTimeIndicateHeter, 0)));
        }

        private void TimerReadCallBackHeter(object o)
        {
            if (PicBoxHeterRead.InvokeRequired)
            {
                PicBoxHeterRead.Invoke((MethodInvoker)(delegate()
                {
                    PicBoxHeterRead.Image = Properties.Resources.gray;
                    tmReadHeter.Dispose();
                }));

            }
            else
            {
                PicBoxHeterRead.Image = Properties.Resources.gray;
                tmReadHeter.Dispose();
            }
        }

        private void TimerWriteCallBackHeter(object o)
        {
            if (PicBoxHeterWrite.InvokeRequired)
            {
                PicBoxHeterWrite.Invoke((MethodInvoker)(delegate()
                {
                    PicBoxHeterWrite.Image = Properties.Resources.gray;
                    tmWriteHeter.Dispose();
                }));

            }
            else
            {
                PicBoxHeterWrite.Image = Properties.Resources.gray;
                tmWriteHeter.Dispose();
            }
        }

        void TestAirHeter() // Команда Инициализации
        {
            if (ClientHeterodyn == null)
            {
                MessageBox.Show(TranslateDic[ExeptionConnectHeter]);
                ButHeterConnect.Focus();
                return;
            }

            LabTempHeter.Text = TranslateDic[LabTempHeter.Name];
            sTemp = "";
            LabHumidHeter.Text = TranslateDic[LabHumidHeter.Name];
            sHumidid = "";
            LabChargeHeter.Text = TranslateDic[LabChargeHeter.Name];
            sCharge = "";

            ClientHeterodyn.TestAir();
        }

        bool IsConnectHeterodyn() // проверка подключению к КГ
        {
            if (ClientHeterodyn != null)
                return true;
            else
                return false;
        }

        void ShowAllCmdHeter()
        { 
        }
    }
}
