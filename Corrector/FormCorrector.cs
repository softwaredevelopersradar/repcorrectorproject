﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Ini;
using System.Xml;
using System.IO.Ports;
using System.Diagnostics;
using BrightIdeasSoftware;

namespace Corrector
{
    public partial class FormCorrector : Form
    {

        #region Variables

        string[] RangesEPO;
        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        Dictionary<string, string> TranslateDic;
        const string languageRus = "rus";
        const string languageEng = "eng";
        string sLanguage = "rus";
        IniFile IniSettings;
        CorrectorDataOLV dataOlv;
        CorrectorAnalysisTypes.Parameters objParam;
        FormParameters objFormParam = new FormParameters();
        CorrectorAutoCollect objClassCorrector = null;
        AnalysisAutoCollect objClassAnalysis = null;
        CorrectorOnOffHeter objOnOffHeter = null;
        AnalysisDataOLV dataAnalysisOLV;
        Stopwatch DurationWork;
        string durationWorkString = null;

        #endregion

        #region Error&Exeption
        string ErrorEntrDataEPO = "ErrorEntrDataEPO";
        string ErrorEntrDataFreq = "ErrorEntrDataFreq";
        string ExeptionFileXML = "ExeptionFileXML";
        string ExeptionFileNumberStrip = "ExeptionFileNumberStrip";
        string ExeptionChoicePortHeter = "ExeptionChoicePortHeter";
        string ExeptionConnectHeter = "ExeptionConnectHeter";
        #endregion

        #region Events

        #endregion

        private void FormCorrector_Load(object sender, EventArgs e)
        {
            //LoadDataFromXML();
            LoadDataFromXML();
            SetControlsCorrector();
            sLanguage = "eng";
            ChangeLanguage(sLanguage);
        }

        #region Translate

        private void ButLanguage_Click(object sender, EventArgs e)
        {
            switch (sLanguage)
            {
                case languageRus:
                    ButLanguage.Text = "EN";
                    sLanguage = languageEng;
                    ChangeLanguage(sLanguage);
                    break;
                case languageEng:
                    ButLanguage.Text = "RU";
                    sLanguage = languageRus;
                    ChangeLanguage(sLanguage);
                    break;
                default:
                    ButLanguage.Text = "RU";
                    sLanguage = languageRus;
                    ChangeLanguage(sLanguage);

                    break;
            }
        }

        void LoadDataFromXML() //загрузка данных в словарь
        {
            XmlDocument xDoc = new XmlDocument();
            if (System.IO.File.Exists("Translate.xml"))
                xDoc.Load("Translate.xml");
            else
            {
                MessageBox.Show(TranslateDic[ExeptionFileXML]);
                this.Close();
                return;
            }
            TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;
                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == sLanguage)
                            {
                                TranslateDic.Add(attr.Value, childnode.InnerText);
                            }

                        }
                    }
                }
            }
        }

        private void ChangeLanguage(string sLanguage)
        {
            LoadDataFromXML();

            ForeachControl(Controls);
            ForeachControl(objFormParam.GetAllControls());

            TranslateColumnHeader(ObjListViewCalibrarion.Columns);
            TranslateColumnHeader(ObjListViewAnalysis.Columns);

            TranslateToolTip(tStripWorkButton);
            TranslateToolTip(TStripAnalysis);

            UpDateContent();
        }

        void ForeachControl(Control.ControlCollection ListControl)
        {
                foreach (Control value in ListControl)
                {
                    Control control = value as Control;
                    if (control != null)
                    {
                        if (TranslateDic.ContainsKey((value as Control).Name))
                        {
                            (value as Control).Text = TranslateDic[(value as Control).Name];
                        }
                    }    
                    if(value.Controls.Count!= 0)
                    {
                        ForeachControl(value.Controls);
                    }
                }
        }

        void TranslateColumnHeader(ListView.ColumnHeaderCollection collections)
        {
            foreach(OLVColumn column in collections)
            {
                if(TranslateDic.ContainsKey(column.AspectName))
                {
                    column.Text = TranslateDic[column.AspectName];
                }
            }
        }

        void TranslateToolTip(ToolStrip toolStrip)
        {
            string addToolTip = "ToolTip";
            foreach (ToolStripButton item in toolStrip.Items)
            {
                var nameToolTip = item.Name + addToolTip;
                if (TranslateDic.ContainsKey(nameToolTip))
                {
                    item.ToolTipText = TranslateDic[nameToolTip];
                }
            }
        }

        void UpDateContent()
        {
            NumBoxAnalysisEPOStart_ValueChanged(this, null);
            NumBoxAnalysisEPOStop_ValueChanged(this, null);
            NumBoxEPOStart_ValueChanged(this, null);
            NumBoxEPOStop_ValueChanged(this, null);
            LabNumRestart.Text = iRestartHeter.HasValue ? (TranslateDic[LabNumRestart.Name] + " " + iRestartHeter):(TranslateDic[LabNumRestart.Name]);
            LabCountBadFreq.Text = dCountBadFreqNull.HasValue?(dCountBadFreqNull.Value + " " + TranslateDic[LabCountBadFreq.Name]): TranslateDic[LabCountBadFreq.Name];
            LabSumTime.Text = durationWorkString == null ? (TranslateDic[LabSumTime.Name]) : (TranslateDic[LabSumTime.Name] + " " + durationWorkString);
            labStd.Text = StdDeltaNull.HasValue ? (TranslateDic[labStd.Name] + "  " + StdDeltaNull.Value.ToString()) : TranslateDic[labStd.Name];
            LabTempHeter.Text = sTemp==""? TranslateDic[LabTempHeter.Name]: TranslateDic[LabTempHeter.Name] + "\n" + sTemp;
            LabHumidHeter.Text = sHumidid == "" ? TranslateDic[LabHumidHeter.Name] : TranslateDic[LabHumidHeter.Name] + "\n" + sHumidid;
            LabChargeHeter.Text = sCharge == "" ? TranslateDic[LabChargeHeter.Name] : TranslateDic[LabChargeHeter.Name] + "\n" + sCharge;
        }
        #endregion

        #region FormCorrector

        public FormCorrector()
        {
            InitializeComponent();
        }


        void ChangeCheckedTypeReq(object sender, EventArgs e)
        {
            GroupBoxFileFreq.Visible = RadButFileFreq.Checked;
            GroupBoxFreq.Visible = RadButFreq.Checked;
        }

        private void ButClear_Click(object sender, EventArgs e)
        {
            RichBoxHex.Clear();
        }

        private void ButAdd_Click(object sender, EventArgs e)
        {
            AddFullGroupRangeToListView();
        }

        private void ButDelete_Click(object sender, EventArgs e)
        {
            ObjListViewCalibrarion.RemoveObjects(ObjListViewCalibrarion.SelectedObjects);
        }

        private void ButHeterConnect_Click(object sender, EventArgs e)
        {
            ConnectDisonnectToHeter();
        }

        private void ComPortName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisconnectHeter();
        }

        private void ComSpeed_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisconnectHeter();
        }

        private void LabHeterConnect_MouseHover(object sender, EventArgs e)
        {
            //LabHeterConnect.Visible = false;

        }

        private void PicBoxRefresh_MouseLeave(object sender, EventArgs e)
        {
          //  LabHeterConnect.Visible = true;
        }


        private void LabHeterConnect_MouseHover(object sender, MouseEventArgs e)
        {
            //LabHeterConnect.Visible = false;
        }

        private void LabPLConnect_MouseMove(object sender, MouseEventArgs e)
        {
            //LabPLConnect.Visible = false;
        }

        private void PicBoxParam_MouseLeave(object sender, EventArgs e)
        {
           // LabPLConnect.Visible = true;
        }

        private void ChoseParam(object sender, EventArgs e)
        {
            if (objFormParam.IsHide)
            {
                objFormParam.Show();
            }
            else
                objFormParam.Hide();
        }

        private void ButTestRadio_Click(object sender, EventArgs e)
        {
            TestAirHeter();
        }

        private void ButStart_Click(object sender, EventArgs e)
        {
            CorrectorOnOffHeter.DisposeInstance(ref objOnOffHeter);

            if (!CheckParamAutoReq())
                return;
            dataOlv = new CorrectorDataOLV(ObjListViewCalibrarion, RangesEPO);
            if (objClassCorrector == null)
            {
                objClassCorrector = new CorrectorAutoCollect(ref ClientHeterodyn, ref ClientServer);
                objClassCorrector.evenErrorPauseWork += objClassAutoReq_eventStopWork;
            }
            iRestartHeter = 0;
            dCountBadFreqNull = null;
            durationWorkString = null;
            DurationWork = new Stopwatch();
            DurationWork.Start();
            objClassCorrector.Start(Convert.ToInt16(NumBoxAzimut.Value), objParam, ref dataOlv);
        }
        
        private void ButStop_Click(object sender, EventArgs e)
        {
            objClassCorrector.Stop();
        }

        private void ButPause_Click(object sender, EventArgs e)
        {
            objClassCorrector.Pause();        
        }

        private void ButContinue_Click(object sender, EventArgs e)
        {
            objClassCorrector.Continue(ref ClientHeterodyn, ref ClientServer, objParam);
        }

        private void ButPLConnect_Click(object sender, EventArgs e)
        {
            ConnectDisConnectServer();
        }

        private void ButSendData_Click(object sender, EventArgs e)
        {
            SendDataToServer();
        }

        private void CheckShowRichBoxHex_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckShowRichBoxHex.Checked)
            {
                CorrectorAutoCollect.evenStoryWork += ShowMessage;  
            }
            else
            {
                CorrectorAutoCollect.evenStoryWork -= ShowMessage;  
            }
        }

        private void ChekBoxProgress_CheckedChanged(object sender, EventArgs e)
        {
            if (ChekBoxProgress.Checked)
            {
                CorrectorDataOLV.evenChangeCurrentRow += DataOLV_evenChangeCurrentRow;
            }
            else
            {
                CorrectorDataOLV.evenChangeCurrentRow -= DataOLV_evenChangeCurrentRow;
            }
        }
        #endregion

        #region Event

        void objClassAutoReq_eventStopWork(string mess, byte ErrorCode, double dCountBadFreq)
        {
            switch (ErrorCode)
            {
                case (byte)CorrectorAnalysisTypes.ErrorCodes.StartWorkClient: // Опрос начат

                    Invoke((MethodInvoker)(() =>
                    {
                        ButAddCalibrarionRange.Enabled = false;
                        ButDeleteCalibrarionRange.Enabled = false;
                        
                        ButStartCalibrarion.Enabled = false;
                        ButStartCalibrarion.Visible = false;

                        ButSendData.Enabled = false;
                        ButSendData.Visible = false;

                        ButStopCalibrarion.Enabled = true;
                        ButStopCalibrarion.Visible = true;

                        ButPauseCalibrarion.Enabled = true;
                        ButPauseCalibrarion.Visible = true;

                        ButTestRadio.Enabled = true;

                        objFormParam.ControlsEnabled = false;
                        NumBoxAzimut.Enabled = false;

                        ButOffHeter.Enabled = false;
                        ButOffHeter.Visible = false;

                        ButOnHeter.Enabled = false;
                        ButOnHeter.Visible = false;

                        iRestartHeter = null;
                        Invoke((MethodInvoker)(() => LabNumRestart.Text = TranslateDic[LabNumRestart.Name]));
                        Invoke((MethodInvoker)(() => LabCountBadFreq.Text = TranslateDic[LabCountBadFreq.Name]));
                        Invoke((MethodInvoker)(() => LabSumTime.Text = TranslateDic[LabSumTime.Name]));
                   
                    }));
                    break;

                case (byte)CorrectorAnalysisTypes.ErrorCodes.EndWork: // Опрос завершен
                case (byte)CorrectorAnalysisTypes.ErrorCodes.StopWorkError: // остановка опроса из-за ошибки 
                case (byte)CorrectorAnalysisTypes.ErrorCodes.StopWorkClient:// остановка опроса из-за перевода клиентом
                    objClassCorrector.Dispose();
                    objClassCorrector = null;
                    DurationWork.Stop();
                    Invoke((MethodInvoker)(() =>
                        {
                            ButPauseCalibrarion.Enabled = false;
                            ButPauseCalibrarion.Visible = false;

                            ButContinueCalibrarion.Enabled = false;
                            ButContinueCalibrarion.Visible = false;

                            ButStopCalibrarion.Enabled = false;
                            ButStopCalibrarion.Visible = false;

                            ButStartCalibrarion.Enabled = true;
                            ButStartCalibrarion.Visible = true;

                            ButAddCalibrarionRange.Enabled = true;
                            ButDeleteCalibrarionRange.Enabled = true;

                            ButSendData.Enabled = true;
                            ButSendData.Visible = true;

                            ButTestRadio.Enabled = true;

                            objFormParam.ControlsEnabled = true;
                            NumBoxAzimut.Enabled = true;

                            ButOffHeter.Enabled = true;
                            ButOffHeter.Visible = true;

                            ButOnHeter.Enabled = true;
                            ButOnHeter.Visible = true;

                        }));
                    Invoke((MethodInvoker)(() => LabNumRestart.Text = TranslateDic[LabNumRestart.Name] + " " + iRestartHeter));
                    dCountBadFreqNull = dCountBadFreq;
                    Invoke((MethodInvoker)(() => LabCountBadFreq.Text = dCountBadFreq + " " + TranslateDic[LabCountBadFreq.Name]));
                    durationWorkString = DurationWork.Elapsed.Hours + ":" + DurationWork.Elapsed.Minutes + ":" + DurationWork.Elapsed.Seconds + ":" + DurationWork.Elapsed.Milliseconds;
                    Invoke((MethodInvoker)(() => LabSumTime.Text = TranslateDic[LabSumTime.Name] + " " + DurationWork.Elapsed.Hours + ":" + DurationWork.Elapsed.Minutes + ":" + DurationWork.Elapsed.Seconds + ":" + DurationWork.Elapsed.Milliseconds));
                    if (mess != "")
                    {
                        ShowMessage(mess, (byte)CorrectorAnalysisTypes.TypeMess.Attention);
                        MessageBox.Show(mess);
                    }
                    break;

                case (byte)CorrectorAnalysisTypes.ErrorCodes.PauseWorkError: //перевод в режим Паузы из-за ошибки 
                case (byte)CorrectorAnalysisTypes.ErrorCodes.PauseWorkClient: //перевод в режим паузы оператором
                    
                    Invoke((MethodInvoker)(() =>
                    {
                        ButPauseCalibrarion.Enabled = false;
                        ButPauseCalibrarion.Visible = false;

                        ButContinueCalibrarion.Visible = true;
                        ButContinueCalibrarion.Enabled = true;

                        ButTestRadio.Enabled = true;

                        objFormParam.ControlsEnabled = true;

                        ButPLConnect.Focus();
                    }));
                    if (mess != "")
                    {
                        ShowMessage(mess, (byte)CorrectorAnalysisTypes.TypeMess.Attention);
                        MessageBox.Show(mess);
                    }
                    break;

                case (byte)CorrectorAnalysisTypes.ErrorCodes.ContinueWorkClient: 

                    Invoke((MethodInvoker)(() =>
                    {
                        ButContinueCalibrarion.Visible = false;
                        ButContinueCalibrarion.Enabled = false;

                        ButPauseCalibrarion.Visible = true;
                        ButPauseCalibrarion.Enabled = true;

                        ButTestRadio.Enabled = false;

                        objFormParam.ControlsEnabled = false;
                    }));
                    break;
            }
        }

        void DataOLV_evenChangeCurrentRow(int CurrentRow)
        {
            if (ProgressWorkCalibrarion.InvokeRequired)
            {
                ProgressWorkCalibrarion.Invoke((MethodInvoker)(() =>
                {
                    ProgressWorkCalibrarion.Maximum = dataOlv.CountRow;
                    ProgressWorkCalibrarion.Value = CurrentRow;
                }));
            }
            else
            {
                ProgressWorkCalibrarion.Maximum = dataOlv.CountRow;
                ProgressWorkCalibrarion.Value = CurrentRow;
            }
        }

        void CorrectorAnalysisTypes_UpDateRelativeBearing()
        {
            if (ClientServer != null)
            Task.Run(async () => await ClientServer.SetDirectionCorrection(objParam.RelativeBearing, true)).GetAwaiter();
        }

        #endregion

        #region CommonFunction

        void SetControlsCorrector() // установка значений по умолчанию
        {
            RangesEPO = LoadRangeEPO();
            IniSettings = new IniFile(path + "\\Settings.ini");
            NumBoxEPOStart.Minimum = 1;
            NumBoxEPOStop.Minimum = 1;
            NumBoxEPOStop.Maximum = RangesEPO.Length;
            NumBoxEPOStart.Maximum = RangesEPO.Length;
            NumBoxFreqStart.Minimum = Convert.ToInt32(RangesEPO[0].Split(new char[] { '-', ' ' })[0]) + 1;
            NumBoxFreqStop.Minimum = Convert.ToInt32(RangesEPO[0].Split(new char[] { '-', ' ' })[0]) + 1;
            NumBoxFreqStop.Maximum = Convert.ToInt32(RangesEPO[RangesEPO.Length - 1].Split(new char[] { '-', ' ' })[1]) - 1;
            NumBoxFreqStart.Maximum = Convert.ToInt32(RangesEPO[RangesEPO.Length - 1].Split(new char[] { '-', ' ' })[1]) - 1;
            ColmResult.Renderer = new BrightIdeasSoftware.MappedImageRenderer(new Object[] {
                "None", Properties.Resources.gray,
                "Normally", Properties.Resources.yellow,
                "Badly", Properties.Resources.red,
                "Great", Properties.Resources.green
            });
            ColmResult.AspectGetter = delegate(object row)
            {
                switch (((CorrectorInfoTable)row).Result)
                { case 0: //gray                        
                        return "None";
                    case 1: // yellow
                        return "Normally";
                    case 2: // red
                        return "Badly";
                    case 3: //green
                        return "Great";
                    default:
                        return "Badly";
                }
            };
            objParam = new CorrectorAnalysisTypes.Parameters();

            CorrectorAnalysisTypes.UpDateRelativeBearing += CorrectorAnalysisTypes_UpDateRelativeBearing;
            SetControlsAnalysis();

            GetSettingsHeter();

            this.NumBoxStepFileFreq.Items.Add(1);
            this.NumBoxStepFileFreq.Items.Add(3);
            this.NumBoxStepFileFreq.Items.Add(5);
            this.NumBoxStepFileFreq.Items.Add(8);
            this.NumBoxStepFileFreq.Items.Add(15);
            this.NumBoxStepFileFreq.SelectedIndex = 0;
        }

        string[] LoadRangeEPO() //загрузка диапазонов ЕПО из файла
        {
            List<string> NumberStrip = new List<string>();
            try
            {
                using (StreamReader Str = new StreamReader(path + "\\NumberStrip.txt", Encoding.Default))
                {
                    string[] temp = Str.ReadToEnd().Split(new Char[] { '\n', ' ', '\r' });
                    for (int i = 0; i < temp.Length; i++)
                        if (temp[i] != "")
                            NumberStrip.Add(temp[i].Replace("		", ""));
                }
            }
            catch 
            {
                MessageBox.Show(TranslateDic[ExeptionFileNumberStrip]);
                this.Close();
            }
            return NumberStrip.ToArray();
        }

        string FuncSearchRange() //функция поиска диапазона по выбранным ЕПО или Частотам(Старт/Стоп)
        {
            string sRange = "";
            string sFreqStart = "";
            string sFreqStop = "";
            if (RadButFileFreq.Checked == true)
            {
                sFreqStart = RangesEPO[(int)NumBoxEPOStart.Value - 1].Split(new char[] { '-', ' ' })[0];
                sFreqStop = RangesEPO[(int)NumBoxEPOStop.Value - 1].Split(new char[] { '-', ' ' })[1];
                sRange = sFreqStart + "-" + sFreqStop + "(" + (NumBoxEPOStop.Value - NumBoxEPOStart.Value + 1).ToString() + ")";

            }
            else
            {
                
                sRange = NumBoxFreqStart.Value==NumBoxFreqStop.Value ? NumBoxFreqStart.Value.ToString() + "*" :
                    NumBoxFreqStart.Value.ToString() + "-" + NumBoxFreqStop.Value.ToString() + "*";
            }
            return sRange;
        }

        string FuncSerchStep() //функция нахождения шага
        {
            string step = "";

            if (RadButFileFreq.Checked == true)
            {
                step = NumBoxStepFileFreq.SelectedItem.ToString();
            }
            else 
            {
                step = NumBoxStepFreq.Value.ToString();
            }
            return step;
        }

        bool CheckRange() //проверка корректности введенных данных в ПОО Старт/Стоп или Частота Старт/Стоп
        {
            if (RadButFileFreq.Checked == true)
            {
                if(NumBoxEPOStop.Value < NumBoxEPOStart.Value)
                {
                    MessageBox.Show(TranslateDic[ErrorEntrDataEPO]);
                    NumBoxEPOStop.Focus();
                    return false;
                }
            }
            else
            {
                if (NumBoxFreqStop.Value < NumBoxFreqStart.Value)
                {
                    MessageBox.Show(TranslateDic[ErrorEntrDataFreq]);
                    NumBoxFreqStop.Focus();
                    return false;
                }
            }
            return true;

        }

        bool CheckParamAutoReq()
        {
            if (ObjListViewCalibrarion.Objects == null)
            {
                MessageBox.Show("Необходимо заполнить таблицу!");
                return false;
            }
            if (!IsConnectServer())
            {
                MessageBox.Show("Необходимо подключиться к Серверу!");
                ButPLConnect.Focus();
                return false;
            }
            if (!IsConnectHeterodyn())
            {
                MessageBox.Show("Необходимо подключиться к КГ!");
                ButHeterConnect.Focus();
                return false;
            }
            return true; 
        }

        void WriteToListView() //добавление записи в ObjectListView
        {
            if (CheckRange())
            {
                CorrectorInfoTable infoTable = new CorrectorInfoTable(FuncSearchRange(), FuncSerchStep(), 0, "");
                ObjListViewCalibrarion.AddObject(infoTable);
                ObjListViewCalibrarion.Items[ObjListViewCalibrarion.Items.Count - 1].Checked = true;
                //ObjListView.EnsureModelVisible(infoTable);                
            }
        }
 
        void AddFullGroupRangeToListView()
        {
            if (!CheckRange())
                return;
            string sRange = string.Empty;
            string sFreqStart = String.Empty;
            string sFreqStop = String.Empty;
            if (RadButFileFreq.Checked == true)
            {
                for (int i = (int)NumBoxEPOStart.Value; i <= (int)NumBoxEPOStop.Value; i++)//_+= (int)NumBoxStepFileFreq.Value)
                {
                    string[] range = RangesEPO[i - 1].Split(new char[] { '-', ' ' });
                    sFreqStart = range[0];
                    sFreqStop = range[1];
                    sRange = sFreqStart + "-" + sFreqStop;// +"(1)";
                    CorrectorInfoTable infoTable = new CorrectorInfoTable(sRange, NumBoxStepFileFreq.SelectedItem.ToString(), 0, "");
                    ObjListViewCalibrarion.AddObject(infoTable);
                    ObjListViewCalibrarion.Items[ObjListViewCalibrarion.Items.Count - 1].Checked = true;
                }

            }
            else
            {
                for (int i = (int)NumBoxFreqStart.Value; i <= (int)NumBoxFreqStop.Value; i += (int)NumBoxStepFreq.Value)
                {
                    sRange = i.ToString() + "*";
                    CorrectorInfoTable infoTable = new CorrectorInfoTable(sRange, NumBoxStepFreq.Value.ToString(), 0, "");
                    ObjListViewCalibrarion.AddObject(infoTable);
                    ObjListViewCalibrarion.Items[ObjListViewCalibrarion.Items.Count - 1].Checked = true;
                }
            }
        }


        private void ShowColorButton(Button button, Color color)
        {
            if (button.InvokeRequired)
            {
                button.Invoke((MethodInvoker)(delegate()
                {
                    button.BackColor = color;
                }));

            }
            else
            {
                button.BackColor = color;
            }
        }

        private void ShowMessRichBox(string text, Color color)
        {
            Invoke((MethodInvoker)(() =>
                {
                    if (RichBoxHex.TextLength == RichBoxHex.MaxLength - 100)
                        RichBoxHex.Clear();
                    RichBoxHex.AppendText("\n");
                    RichBoxHex.SelectionColor = color;

                }));
        }

        private async void SendDataToServer()
        {
            if (!IsConnectServer())
            {
                MessageBox.Show("Необходимо подключиться к Серверу!");
                ButPLConnect.Focus();
                return;
            }
            Protocols.CalibrationCorrectionSignal[] Data = LoadData();
            if (Data == null)
            {
                MessageBox.Show("Необходимо проверить наличие папки DATA с необходимыми данными для отправки на Сервер. Проверьте и повторите еще раз");
                return;
            }
            var answer =  await ClientServer.CalculateCalibrationCorrection(Data);//w.GetAwaiter().GetResult();
            MessageBox.Show("Результат отправки данных на Сервер:  " + ErrorMess[answer.Header.ErrorCode]);

        }

        private void ShowMessage(string mess, byte bTypeMass)
        {
            if (RichBoxHex.InvokeRequired) { RichBoxHex.Invoke((MethodInvoker)(() => RichBoxHex.AppendText("\n"))); }
            else { RichBoxHex.AppendText("\n"); }     
            switch (bTypeMass)
            {
                case (byte)CorrectorAnalysisTypes.TypeMess.Attention:
                    if (RichBoxHex.InvokeRequired) { RichBoxHex.Invoke((MethodInvoker)(() => RichBoxHex.SelectionColor = Color.Maroon)); }
                    else { RichBoxHex.SelectionColor = Color.Maroon; }                    
                    break;
                case (byte)CorrectorAnalysisTypes.TypeMess.Heterodin:
                    if (RichBoxHex.InvokeRequired) { RichBoxHex.Invoke((MethodInvoker)(() => RichBoxHex.SelectionColor = Color.Green)); }
                    else { RichBoxHex.SelectionColor = Color.Green; }    
                    break;
                case (byte)CorrectorAnalysisTypes.TypeMess.Server:
                    if (RichBoxHex.InvokeRequired) { RichBoxHex.Invoke((MethodInvoker)(() => RichBoxHex.SelectionColor = Color.MidnightBlue)); }
                    else { RichBoxHex.SelectionColor = Color.MidnightBlue; }    
                    break;
            }
            if (RichBoxHex.InvokeRequired)
            {
                RichBoxHex.Invoke((MethodInvoker)(() =>
                    {
                        RichBoxHex.AppendText(DateTime.Now.ToString("HH:mm:ss") + " " + mess);
                        RichBoxHex.AppendText("\n");
                        RichBoxHex.ScrollToCaret();
                    }));
            }
            else
            {
                RichBoxHex.AppendText(DateTime.Now.ToString("HH:mm:ss") + " " + mess);
                RichBoxHex.AppendText("\n");
                RichBoxHex.ScrollToCaret();
            }
        }
        #endregion

        private void NumBoxEPOStart_ValueChanged(object sender, EventArgs e)
        {
            LabRangeStart.Text = RangesEPO[Convert.ToInt16(NumBoxEPOStart.Value) - 1] + TranslateDic[KeyMhz];
        }

        private void NumBoxEPOStop_ValueChanged(object sender, EventArgs e)
        {
            LabRangeStop.Text = RangesEPO[Convert.ToInt16(NumBoxEPOStop.Value) - 1] + TranslateDic[KeyMhz];
        }

        private void ButOnHeter_Click(object sender, EventArgs e)
        {
            if (!IsConnectHeterodyn())
            {
                MessageBox.Show("Необходимо подключиться к КГ!");
                ButHeterConnect.Focus();
                return;
            }
            if (objOnOffHeter == null)
            {
                objOnOffHeter = new CorrectorOnOffHeter(ref ClientHeterodyn);
                CorrectorOnOffHeter.evenStoryWork += ShowMessage;
            }

            if (RadButFileFreq.Checked == true)
            {
                objOnOffHeter.OnSignalFielFreq(Convert.ToInt16((LabRangeStart.Text.Split(new char[] { ' ', ':', '-' }))[0]),
                    Convert.ToInt16((LabRangeStart.Text.Split(new char[] { ' ', ':', '-' }))[1]), 
                    Convert.ToByte(NumBoxStepFileFreq.SelectedIndex), objParam);
            }
            else
            {
                objOnOffHeter.OnSignalFRCH(Convert.ToInt16(NumBoxFreqStart.Value)*1000, objParam);
            }
        }

        private void ButOffHeter_Click(object sender, EventArgs e)
        {
            if (!IsConnectHeterodyn())
            {
                MessageBox.Show("Необходимо подключиться к КГ!");
                ButHeterConnect.Focus();
                return;
            }
            if (objOnOffHeter == null)
            {
                objOnOffHeter = new CorrectorOnOffHeter(ref ClientHeterodyn);
                CorrectorOnOffHeter.evenStoryWork += ShowMessage;
            }
            objOnOffHeter.OffSignal(objParam);
        }

    }
}
