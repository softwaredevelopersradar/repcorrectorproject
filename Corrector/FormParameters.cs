﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Corrector
{
    public partial class FormParameters : Form
    {
        public  bool IsHide = true;
        private static CorrectorAnalysisTypes.Parameters obgParam;

        public FormParameters()
        {
            InitializeComponent();
            LoadParam();
        }

        private void FormParameters_Load(object sender, EventArgs e)
        {
            InitControls();
        }

        private void FormParameters_FormClosing(object sender, FormClosingEventArgs e)
        {
            IsHide = true;
            e.Cancel = true;
            Hide();
        }

        private void FormParameters_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
                IsHide = false;
            else
                IsHide = true;
        }
        
        void InitControls()
        {
            SignalToNoisBox.Value = obgParam.SignalToNois;
            DispersionPhaseBox.Value = obgParam.DispersionPhase;
            NumAveragePelenBox.Value = obgParam.NumAveragePelen;
            NumAveragePhaseBox.Value = obgParam.NumAveragePhase;
            IncludeShiftBox.Checked = obgParam.IncludeShift;
            TimeDelayHeterBox.Value = obgParam.TimeDelayHeter;
            GreatResultBox.Value = obgParam.GreatResult;
            NormalResultStartBox.Value = obgParam.NormalResultStart;
            NormalResultStopBox.Value = obgParam.NormalResultStop;
            BadResultBox.Value = obgParam.BadResult;
            MaxCountSendBox.Value = obgParam.MaxCountSend;
            DurationRadiationBox.Value = obgParam.DurationRadiation;
            RelativeBearingBox.Value = obgParam.RelativeBearing;
        }

        void LoadParam()
        {
            obgParam = new CorrectorAnalysisTypes.Parameters();
            obgParam.LoadParameters();
            CorrectorAnalysisTypes.UpDateRelativeBearing += CorrectorAnalysisTypes_UpDateRelativeBearing;
        }

        void CorrectorAnalysisTypes_UpDateRelativeBearing()
        {
            RelativeBearingBox.Value = obgParam.RelativeBearing;
        }

        private void ChangeValueParams(object sender, EventArgs e)
        {
            Type type = sender.GetType();
            string NameSuppression;
            System.Reflection.PropertyInfo[] properties = obgParam.GetType().GetProperties();

            switch (type.Name)
            {
                case "NumericUpDown":
                    NameSuppression = (sender as NumericUpDown).Name;

                    NameSuppression = NameSuppression.Substring(0, NameSuppression.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameSuppression)
                        {
                            Int16 value = Convert.ToInt16((sender as NumericUpDown).Value);
                            try { fi.SetValue(obgParam, value); }
                            catch { fi.SetValue(obgParam, Convert.ToByte(value)); }
                            break;
                        }
                    break;
                case "CheckBox":
                    NameSuppression = (sender as CheckBox).Name;

                    NameSuppression = NameSuppression.Substring(0, NameSuppression.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameSuppression)
                        {
                            bool value = (sender as CheckBox).Checked;
                            try { fi.SetValue(obgParam, value); }
                            catch { fi.SetValue(obgParam, Convert.ToByte(value)); }
                            break;
                        }
                    break;
            }
        }

        private void KeyUpParams(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ChangeValueParams(sender, new EventArgs());
            }
        }

        public bool ControlsEnabled
        {
            set 
            {
                IncludeShiftBox.Enabled = value;
                NumAveragePelenBox.Enabled = value;
                NumAveragePhaseBox.Enabled = value;
                SignalToNoisBox.Enabled = value;
                DispersionPhaseBox.Enabled = value;
                TimeDelayHeterBox.Enabled = value;
                MaxCountSendBox.Enabled = value;
                DurationRadiationBox.Enabled = value;
            }
        }


        public Control.ControlCollection GetAllControls()
        {
            return Controls;
            //List<Control.ControlCollection> allControls = new List<Control.ControlCollection>();
            //allControls.Add(tabContrSettings.Controls);
            //allControls.Add(tabCommon.Controls);
            //allControls.Add(ParamPelengatorGroupBox.Controls);
            //allControls.Add(GcGroupBox.Controls);
            //allControls.Add(tabCalib.Controls);
            //allControls.Add(CriterExcludFreqGroupBox.Controls);
            //allControls.Add(groupBoxCalib.Controls);
            //allControls.Add(tabAnalysis.Controls);
            //allControls.Add(groupBoxAnalisis.Controls);
            //return allControls;
        }
    }
}
