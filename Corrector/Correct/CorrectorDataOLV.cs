﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using BrightIdeasSoftware;

namespace Corrector
{
    class CorrectorDataOLV : IEnumerable //: IEnumerator //
    {
        ObjectListView olv;
        int[] FirstValRangeHeter;
        int[] SecondValRangeHeter;
        int indexCurrent;       
        public List<StructDataOLV> MassData;//[] MassData;

        public delegate void DelChangeCurrentRow(int CurrentRow);
        public static event DelChangeCurrentRow evenChangeCurrentRow;

        public Status statuswork = new Status();
        public enum Status : byte
        {
            None,
            Normally,
            Badly,
            Great
        }

        public CorrectorDataOLV(ObjectListView OLV, string[] srange)
        {
            this.olv = OLV;
                          
            GetRangeHeter(srange);
            LoadData();
        }

        public StructDataOLV Current
        {
            get
            {
                try
                {
                    return MassData[indexCurrent];
                }
                catch { return null; }
            }
        }

        public int CountFreq
        {
            get
            {
                int count = 0;
                for (int i = 0; i < MassData.Count; i++)
                { count += MassData[i].CountFreq; }
                return count;
            }
        }

        public int CountRow
        {
            get
            {
                return MassData.Count;
            }
        }

        public StructDataOLV GetNextRow()
        {
            indexCurrent++;
            if (evenChangeCurrentRow != null)
                evenChangeCurrentRow(indexCurrent);

            if (indexCurrent < MassData.Count)
            {
                if (!MassData[indexCurrent].IncludRow)
                   return GetNextRow();
                else
                    return MassData[indexCurrent];
            }
            else
                return StructDataOLV.null_DataOLV;
        }

        public bool IsFRCH
        {
            get
            {
                if (MassData[indexCurrent].FreqStart == MassData[indexCurrent].FreqEnd)
                    return true;
                return false;
            }
        }

        public void Reset()
        {
            indexCurrent = -1;
            if (evenChangeCurrentRow != null)
                evenChangeCurrentRow(0);
            try
            {
                for (int i = 0; i < MassData.Count; i++ )
                    if (olv.InvokeRequired)
                    {
                        olv.Invoke((System.Windows.Forms.MethodInvoker)(delegate()
                        {
                            olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(i).Result = (byte)CorrectorDataOLV.Status.None;
                            olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(i).Time = 0.ToString();
                            olv.RefreshObject(olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(i));
                            //olv.Refresh();
                        }));
                    }
                    else
                    {
                        olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(i).Result = (byte)CorrectorDataOLV.Status.None;
                        olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(i).Time = 0.ToString();
                        olv.RefreshObject(olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(i));
                        //olv.Refresh();
                    }
            }
            catch{}
        }

        public byte Result
        {
            set
            {
                try
                {
                    if (indexCurrent < 0)
                        return;
                    if (olv.InvokeRequired)
                    {
                        olv.Invoke((System.Windows.Forms.MethodInvoker)(delegate()
                        {
                            olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(indexCurrent).Result = value;
                            olv.Refresh();
                        }));
                    }
                    else
                    {
                        olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(indexCurrent).Result = value;
                        olv.Refresh();
                    }
                }
                catch { }
            }
        }

        public int Duration
        {
            set
            {
                try
                {
                    if (indexCurrent < 0)
                        return;
                    if (olv.InvokeRequired)
                    {
                        olv.Invoke((System.Windows.Forms.MethodInvoker)(delegate()
                        {
                            olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(indexCurrent).Time = value.ToString() + " мс";
                            olv.RefreshObject(olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(indexCurrent));
                        }));
                    }
                    else
                    {
                        olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(indexCurrent).Time = value.ToString() + " мс";
                        olv.RefreshObject(olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(indexCurrent));
                    }
                }
                catch { }
            }
        }

        public int GetIndex
        {
            get
            {
                return indexCurrent;
            }
        }

        public void SetBackRow()
        {
            indexCurrent--;
            indexCurrent = (indexCurrent < -1) ? -1 : indexCurrent;
        }

        /*public DataOLV this[int index] //индексатор для присвоения результата 
        {
            
            set
            {
                olv.Objects.Cast<InfoTable>().ElementAt<InfoTable>(index).Result = value.Result;
                /*
                try
                {
                    if (olv.InvokeRequired)
                    {
                        olv.Invoke((System.Windows.Forms.MethodInvoker)(delegate()
                        {
                            olv.Objects.Cast<InfoTable>().ElementAt<InfoTable>(index).Result = value;
                            olv.Refresh();
                        }));
                    }
                    else
                    {
                        olv.Objects.Cast<InfoTable>().ElementAt<InfoTable>(index).Result = value;
                        olv.Refresh();
                    }
                }
                catch { }
            }
        }*/

        public int MaxValRangeHeter
        {
            get { return SecondValRangeHeter[SecondValRangeHeter.Length - 1] * 1000; }
        }
        
        public int MinValRangeHeter
        {
            get { return FirstValRangeHeter[0] * 1000; }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < MassData.Count; i++)
                yield return MassData[i];
        }

        private void GetRangeHeter(string[] rangesHeter)
        {
            FirstValRangeHeter = new int[rangesHeter.Length];
            SecondValRangeHeter = new int[rangesHeter.Length];
            for (int i = 0; i < rangesHeter.Length; i++)
            {
                string[] sRange = rangesHeter[i].Split(new Char[] { ' ', '-', '\n', '\r' });
                FirstValRangeHeter[i] = Convert.ToInt32(sRange[0]);
                SecondValRangeHeter[i] = Convert.ToInt32(sRange[1]);
            }
        }

        private List<StructDataOLV> SerchRangesForReq(string range, byte step, bool checkedRow)
        {
            List<StructDataOLV> ListstructDataOLV = new List<StructDataOLV>();
            StructDataOLV structDataOLV;
            string[] stemp = range.Split(new Char[] { ' ', '-', '(', ')'});
            string simv = stemp.Length == 1 ? stemp[0].Substring(stemp[0].Length - 1) : stemp[1].Substring(stemp[1].Length - 1);
            if (simv == "*")
            {
                if (stemp.Length == 1)
                    stemp[0] = stemp[0].Remove(stemp[0].Length - 1);
                else
                    stemp[1] = stemp[1].Remove(stemp[1].Length - 1);
                
                structDataOLV = new StructDataOLV();
                for (int i = Convert.ToInt32(stemp[0]); i <= (stemp.Length == 1 ? Convert.ToInt32(stemp[0]) : Convert.ToInt32(stemp[1])); i += step)
                {
                    structDataOLV = new StructDataOLV();
                    structDataOLV.FreqStart = i * 1000;
                    structDataOLV.FreqEnd = i * 1000;
                    structDataOLV.StepMHz = step;
                    structDataOLV.CountFreq = 1;
                    structDataOLV.IncludRow = checkedRow;
                    ListstructDataOLV.Add(structDataOLV);
                }
            }
            else //ПОО
            {
                Predicate<int> pre  = delegate(int x) { return x == Convert.ToInt32(stemp[0]);};
                int indexFirstEPO = Array.FindIndex(FirstValRangeHeter, (int x) => x == Convert.ToInt32(stemp[0]));
                int indexLastEPO = Array.FindIndex(SecondValRangeHeter, (int x) => x == Convert.ToInt32(stemp[1]));
                for (int i = indexFirstEPO; i <= indexLastEPO; i++)
                {
                    structDataOLV = new StructDataOLV();
                    structDataOLV.FreqStart = FirstValRangeHeter[i] * 1000;
                    structDataOLV.FreqEnd = SecondValRangeHeter[i] * 1000;
                    structDataOLV.StepMHz = step;
                    structDataOLV.CountFreq = (int)((SecondValRangeHeter[i] - FirstValRangeHeter[i]) / structDataOLV.StepMHz) + 1;
                    structDataOLV.IncludRow = checkedRow;
                    ListstructDataOLV.Add(structDataOLV);
                }
            }
            return ListstructDataOLV;
        }

        private void LoadData()
        {
            List<StructDataOLV> temp;
            if (olv.InvokeRequired)
            {
                olv.Invoke((System.Windows.Forms.MethodInvoker)(delegate()
                {
                    MassData = new List<StructDataOLV>();
                    for (int i = 0; i < olv.Items.Count; i++)
                    {
                        temp = new List<StructDataOLV>();
                        
                        temp = SerchRangesForReq(olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(i).Range,
                                Convert.ToByte(olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(i).Step), olv.Items[i].Checked);
                        MassData = MassData.Concat(temp).ToList();
                    }
                }));
            }
            else
            {
                MassData = new List<StructDataOLV>();
                for (int i = 0; i < olv.Items.Count; i++)
                {
                    temp = new List<StructDataOLV>();
                    temp = SerchRangesForReq(olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(i).Range,
                            Convert.ToByte(olv.Objects.Cast<CorrectorInfoTable>().ElementAt<CorrectorInfoTable>(i).Step), olv.Items[i].Checked);
                    MassData = MassData.Concat(temp).ToList();
                }
            }
            indexCurrent = -1;
        }
    }

}
