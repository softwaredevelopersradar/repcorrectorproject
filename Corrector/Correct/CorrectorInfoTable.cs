﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corrector
{
    public class CorrectorInfoTable
    {
        public string Choice { get; set; }
        public string Range { get; set; }
        public string Step { get; set; }
        public int Result { get; set; }
        public string Time { get; set; }
        public CorrectorInfoTable(string range, string step, int result, string time)
        {
            Choice = "";
            Range = range;
            Result = result;
            Step = step;
            Time = time;
        }
    }
}
