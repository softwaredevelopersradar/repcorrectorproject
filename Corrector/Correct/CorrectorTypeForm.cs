﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ini;

namespace Corrector
{
    public class CorrectorAnalysisTypes
    {
        static IniFile IniParam = new IniFile((System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
            + "\\IniParam.ini"));
        public delegate void DelUpDateRelativeBearing();
        public static event DelUpDateRelativeBearing UpDateRelativeBearing = () => { };
        public struct Parameters
        {
            private static byte bSignalToNois;
            private static byte bDispersionPhase;
            private static byte bNumAveragePhase;
            private static byte bNumAveragePelen;
            private static bool bIncludeShift;
            private static Int32 iTimeDelayHeter;
            private static int iGreatResult;
            private static int iNormalResultStart;
            private static int iNormalResultStop;
            private static int iBadResult;
            private static byte bMaxCountSend;
            private static byte bDurationRadiation;
            private static UInt16 uTimeSend;
            private static int iRelativeBearing;

            public  void LoadParameters()
            {
                this.SignalToNois = Convert.ToByte(IniParam.IniReadValue("Exclude", "SignalNoise"));
                this.DispersionPhase = Convert.ToByte(IniParam.IniReadValue("Exclude", "DispPhase"));
                this.NumAveragePhase = Convert.ToByte(IniParam.IniReadValue("Server", "NumAverPhase"));
                this.NumAveragePelen = Convert.ToByte(IniParam.IniReadValue("Server", "NanAverPeleng"));
                this.RelativeBearing = Convert.ToInt16(IniParam.IniReadValue("Server", "RelativeBearing"));
                this.IncludeShift =  Convert.ToBoolean(Convert.ToByte(IniParam.IniReadValue("Heterodyn", "IncludeShift")));
                this.TimeDelayHeter = Convert.ToInt32(IniParam.IniReadValue("Heterodyn", "TimeDelayHete"));
                this.GreatResult = Convert.ToInt32(IniParam.IniReadValue("Analysis", "GreatResult"));
                this.NormalResultStart = Convert.ToInt32(IniParam.IniReadValue("Analysis", "NormalResultStart"));
                this.NormalResultStop = Convert.ToInt32(IniParam.IniReadValue("Analysis", "NormalResultStop"));
                this.BadResult = Convert.ToInt32(IniParam.IniReadValue("Analysis", "BadResult"));
                this.MaxCountSend = Convert.ToByte(IniParam.IniReadValue("Heterodyn", "MaxCountSend"));
                this.DurationRadiation = Convert.ToByte(IniParam.IniReadValue("Heterodyn", "DurationRadiation"));
                this.TimeSend = Convert.ToUInt16(IniParam.IniReadValue("Heterodyn", "TimeSend"));
            }

            public byte SignalToNois
            {
                get { return bSignalToNois; }
                set
                {
                    if (value == bSignalToNois)
                        return;

                    if (value > 10 || value < 3)
                        value = 3;

                    bSignalToNois = value;
                    IniParam.IniWriteValue("Exclude", "SignalNoise", bSignalToNois.ToString());
                }
            }

            public byte DispersionPhase 
            {
                get { return bDispersionPhase; }
                set
                {
                    if (value == bDispersionPhase)
                        return;

                    if (value > 180 || value < 0)
                        value = 10;
                    bDispersionPhase = value;
                    IniParam.IniWriteValue("Exclude", "DispPhase", bDispersionPhase.ToString());
                }
            }

            public byte NumAveragePhase 
            {
                get { return bNumAveragePhase; }
                set 
                {
                    if (value == bNumAveragePhase)
                        return;

                    if (value > 100 || value < 1)
                        value = 5;
                    bNumAveragePhase = value;
                    IniParam.IniWriteValue("Server", "NumAverPhase", bNumAveragePhase.ToString());
                }
            }

            public byte NumAveragePelen 
            { 
                get { return bNumAveragePelen; }
                set
                {
                    if (value == bNumAveragePelen)
                        return;

                    if (value > 100 || value < 1)
                        value = 5;
                    bNumAveragePelen = value;
                    IniParam.IniWriteValue("Server", "NanAverPeleng", bNumAveragePelen.ToString());
                }
            }
            public int RelativeBearing
            {
                get { return iRelativeBearing; }
                set
                {
                    if (value == RelativeBearing)
                        return;
                    while (value < 0)
                        value += 360;
                    while (value > 360)
                        value -= 360;
                    iRelativeBearing = value;
                    IniParam.IniWriteValue("Server", "RelativeBearing", iRelativeBearing.ToString());
                    UpDateRelativeBearing();
                }
            }

            public bool IncludeShift
            {
                get { return bIncludeShift; }
                set 
                {
                    if (bIncludeShift == value)
                        return;
                    
                    bIncludeShift = value;
                    IniParam.IniWriteValue("Heterodyn", "IncludeShift", Convert.ToByte(bIncludeShift).ToString());
                }
            }

            public Int32 TimeDelayHeter
            {
                get { return iTimeDelayHeter; }
                set
                {
                    if (iTimeDelayHeter == value)
                        return;
                    iTimeDelayHeter = value;
                    IniParam.IniWriteValue("Heterodyn", "TimeDelayHete", iTimeDelayHeter.ToString());
                }
            }

            public int GreatResult
            {
                get { return iGreatResult; }
                set
                {
                    if (iGreatResult == value)
                        return;
                    iGreatResult = value;
                    IniParam.IniWriteValue("Analysis", "GreatResult", iGreatResult.ToString());
                }
            }

            public int NormalResultStart
            {
                get { return iNormalResultStart; }
                set
                {
                    if (iNormalResultStart == value)
                        return;
                    iNormalResultStart = value;
                    IniParam.IniWriteValue("Analysis", "NormalResultStart", iNormalResultStart.ToString());
                }
            }

            public int NormalResultStop
            {
                get { return iNormalResultStop; }
                set
                {
                    if (iNormalResultStop == value)
                        return;
                    iNormalResultStop = value;
                    IniParam.IniWriteValue("Analysis", "NormalResultStop", iNormalResultStop.ToString());
                }
            }

            public int BadResult
            {
                get { return iBadResult; }
                set
                {
                    if (iBadResult == value)
                        return;
                    iBadResult = value;
                    IniParam.IniWriteValue("Analysis", "BadResult", iBadResult.ToString());
                }
            }

            public byte MaxCountSend
            {
                get { return bMaxCountSend; }
                set {

                    if (bMaxCountSend == value) return;
                    bMaxCountSend = value;
                    IniParam.IniWriteValue("Heterodyn", "MaxCountSend", bMaxCountSend.ToString());
                }
            }

            public byte DurationRadiation
            {
                get { return bDurationRadiation; }
                set
                {

                    if (bDurationRadiation == value) return;
                    bDurationRadiation = value;
                    IniParam.IniWriteValue("Heterodyn", "DurationRadiation", bDurationRadiation.ToString());
                }
            }

            public UInt16 TimeSend
            {
                get { return uTimeSend; }
                set 
                {
                    if (uTimeSend == value)
                        return;
                    uTimeSend = value;
                    IniParam.IniWriteValue("Heterodyn", "TimeSend", uTimeSend.ToString());
                }
            }

        }


        public enum TypeMess : byte
        {
            Heterodin,
            Server,
            Attention
        }

        public enum ErrorCodes : byte
        {
            EndWork,
            PauseWorkError, // при любой ошибке, связанной с КГ или ПЛ, переводим в режим ожидания - возможность переподключиться к ПЛ или/и КГ и продолжить опрос 
            StopWorkError,// перевод в стартовое положение      
            StartWorkClient,
            StopWorkClient,
            PauseWorkClient, // эта переменная отопражает перевод работы в режим ожидания Оператором 
            ContinueWorkClient
        }

        public enum Status : byte
        {
            None,
            Normally,
            Badly,
            Great
        }

        public struct MessStory
        {
            static public string OffHeter = "Запрос КГ: Отключение излучения.";
            static public string OnHeter = "Запрос КГ: Включение излучения.";
            static public string ReciveHeter = "Ответ КГ: запрос прошел без ошибок.";
            static public string ReqServer = "Запрос ПЛ: запрос источников.";
            static public string ReciveServer = "Ответ ПЛ: запрос прошел без ошибок.";
            static public string EndWork = "Опрос завершен!";
            static public string StopWork = "Опрос Остановлен! ";
            static public string PauseWork = "Опрос Приостановлен! ";
            static public string ContinueWork = "Опрос Возобнавлен! ";
            static public string StartWork = "Опрос Начался!";
            static public string ServerErrGetSource = "Ошибка сервера! Сбор источников с включенным КГ.";
            static public string ServerErrSourceClearAir = "Ошибка Сервера!Не удалось сборать источники в чистом эфире.";
            static public string HeterError = "Ошибка КГ! ";
        }


    }
}
