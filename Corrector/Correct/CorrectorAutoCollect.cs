﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CNT;
using DspDataModel;
using DspProtocols;
using System.Diagnostics;

namespace Corrector
{
    class CorrectorAutoCollect : IDisposable
    {
        ComHTRD clientHeter;
        AWPtoBearingDSPprotocolNew clientServer;
        CorrectorDataOLV data;
        int NumAveragePelen, NumAveragePhase;
        bool IncludeShift;
        private static bool bWork = false;
        CorrectorAnalysSaveData objAnalySave;
        Stopwatch DurationWork;
        Int32 iTimeDelay = 50;

        public delegate void DelStoryWork(string mess, byte typeDevice); // typeDevice : 0 - KГ; 1 - ПЛ
        static public event DelStoryWork evenStoryWork;//отправка каждого шага работы

        private struct strStory
        {
            public string mess;
            public byte type;
            public double countBadFreq;
            public strStory(string sMess, byte iType)
            {
                this.mess = sMess;
                this.type = iType;
                this.countBadFreq = 0.0d;
            }
            public strStory(string sMess, byte iType,  double dCountBadFreq)
            {
                this.mess = sMess;
                this.type = iType;
                this.countBadFreq = Math.Round(dCountBadFreq,1);
            }
        }

        private strStory StoryWork
        {
            set
            {
                if (evenStoryWork != null)
                { evenStoryWork(value.mess, value.type); }
            }
        }
        
        public delegate void DelStopWork(string mess, byte error, double countBadFreq);
        public event DelStopWork evenErrorPauseWork; //Перевод режима работы - Пауза, для переподключения КГ или ПЛ из-зи ошибок

        private strStory ErrorPauseWork
        {
            set 
            {
                if (evenErrorPauseWork != null)
                    evenErrorPauseWork(value.mess, value.type, value.countBadFreq);
            }
        }

        public delegate void DelProgressResult(int CurrResult, int MaxValue);
        public event DelProgressResult evenGetResult; //событи на получение результата(кол-во пройденых частот + одна итерация на сохранение) 
        
        private struct strProgress 
        {
           public int iProgressResult;
           public int iMaxValue;
           public strProgress(int iResult, int imaxVal)
           {
               this.iProgressResult = iResult;
               this.iMaxValue = imaxVal;
           }
        }
        private strProgress ProgressResult
        {
            set 
            {
                if (evenGetResult != null)
                    evenGetResult(value.iProgressResult, value.iMaxValue);
            }
        }

        public CorrectorAutoCollect(ref ComHTRD ClientHeter, ref AWPtoBearingDSPprotocolNew ClientServer)//, ref DataOLV dataOLV)
        {
            this.clientHeter = ClientHeter;
            this.clientServer = ClientServer;      
            InitEventHeter();
        }

        public void Dispose()
        {
            ComHTRD.OnReceiveCmdOff -= Heter_ReceiveOFF;
            ComHTRD.OnReceiveCmdOnSignal -= Heter_ReceiveOnSignal;
            ComHTRD.OnReceiveCmdError -= Heter_Error;
            ComHTRD.eventStopSend -= Heter_StopSend;
        }
        
        private void SetParam(CorrectorAnalysisTypes.Parameters param)
        {
            this.NumAveragePelen = param.NumAveragePelen;
            this.NumAveragePhase = param.NumAveragePhase;
            this.IncludeShift = param.IncludeShift;
            this.iTimeDelay = param.TimeDelayHeter;
            ComHTRD.TimeSendHeter = param.TimeSend;
            ComHTRD.MaxCountSend = param.MaxCountSend;
            ComHTRD.DurationRadiation = param.DurationRadiation;
        }

        public void Start(short azimut, CorrectorAnalysisTypes.Parameters param, ref CorrectorDataOLV dataOLV)
        {
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.StartWork, (byte)CorrectorAnalysisTypes.TypeMess.Attention); // обрабатывает каждый шаг работы
            ErrorPauseWork = new strStory("", (byte)CorrectorAnalysisTypes.ErrorCodes.StartWorkClient);
            this.data = dataOLV;
            SetParam(param);
            objAnalySave = new CorrectorAnalysSaveData(azimut, param); // объект класса Анилиза и Сохранения данных. 
            bWork = true;
            data.Reset();
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.OffHeter, (byte)CorrectorAnalysisTypes.TypeMess.Heterodin);
            clientHeter.OFFSignal(); //сперва отключаем излучение в КГ для того, чтобы собрать данные в чистом эфире
        }

        public void Stop()
        {
            clientHeter.StopSend();
            objAnalySave.Save();// сохраняем уже собранные данные
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.StopWork, (byte)CorrectorAnalysisTypes.TypeMess.Attention);
            ErrorPauseWork = new strStory("", (byte)CorrectorAnalysisTypes.ErrorCodes.StopWorkClient, (100 - (objAnalySave.CountDataSave / (double)data.CountFreq) * 100));
            bWork = false;
            data.Reset();
        }

        public void Pause()
        {
            clientHeter.StopSend();
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.PauseWork, (byte)CorrectorAnalysisTypes.TypeMess.Attention);
            data.SetBackRow();
            ErrorPauseWork = new strStory("", (byte)CorrectorAnalysisTypes.ErrorCodes.PauseWorkClient);
            bWork = false;
        }

        public void Continue(ref ComHTRD ClientHeter, ref AWPtoBearingDSPprotocolNew ClientServer, CorrectorAnalysisTypes.Parameters param)
        {
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.ContinueWork, (byte)CorrectorAnalysisTypes.TypeMess.Attention);
            ErrorPauseWork = new strStory("", (byte)CorrectorAnalysisTypes.ErrorCodes.ContinueWorkClient);
            this.clientHeter = ClientHeter;
            this.clientServer = ClientServer;

            SetParam(param);
            objAnalySave.SetParams = param;

            bWork = true;
            OnSignal();
        }

        private void InitEventHeter()
        {
            ComHTRD.OnReceiveCmdOff += Heter_ReceiveOFF;
            ComHTRD.OnReceiveCmdOnSignal += Heter_ReceiveOnSignal;
            ComHTRD.OnReceiveCmdError += Heter_Error;
            ComHTRD.eventStopSend += Heter_StopSend;
        }

        //////////////////////WORK///////////////////////////

        private void FuncWork() // главная функция управления АвтоОпросом 
        {
            if (!bWork)//При переводе режима работы в Паузу или Стоп процесс завершит начатый шаг, но к следующему не перейдет 
               return;

            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.ReqServer, (byte)CorrectorAnalysisTypes.TypeMess.Server); 

            Protocols.HeterodyneRadioSouce[] currentRadioSources = new Protocols.HeterodyneRadioSouce[] { };
            bool answerServer = RadioSources(ref currentRadioSources);
            DurationWork.Stop();
            if (!bWork) 
                return;
            if (!answerServer) // получение источников с ВКЛ КГ
            {
                ErrorPauseWork = new strStory(CorrectorAnalysisTypes.MessStory.ServerErrGetSource, (byte)CorrectorAnalysisTypes.ErrorCodes.PauseWorkError);  //перевод в режим ожидания с ошибкой
                return;
            }
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.ReciveServer, (byte)CorrectorAnalysisTypes.TypeMess.Server);
            data.Duration = DurationWork.Elapsed.Seconds*1000 + DurationWork.Elapsed.Milliseconds;
            data.Result = objAnalySave.Analysis(currentRadioSources); // анализ полученных данных
            if (!OnSignal()) // включение сигнала
            {
                //если прошли все диапазоны
                if (objAnalySave.CountDataSave == 0)
                {
                    ErrorPauseWork = new strStory(CorrectorAnalysisTypes.MessStory.EndWork + " Нет частот на сохранение", (byte)CorrectorAnalysisTypes.ErrorCodes.EndWork,
                        (100 - (objAnalySave.CountDataSave / (double)data.CountFreq) * 100));  //завершен опрос
                    return;
                }
                bool answer = objAnalySave.Save(); //сохранение данных
                ErrorPauseWork = new strStory(CorrectorAnalysisTypes.MessStory.EndWork + ((answer == true) ? " Сохранение прошло успешно" : " При сохранениии произошли ошибки"),
                    (byte)CorrectorAnalysisTypes.ErrorCodes.EndWork,
                    (100 - (objAnalySave.CountDataSave / (double)data.CountFreq) * 100));  //завершен опрос
                return;
            }
        }

        private bool RadioSources(ref Protocols.HeterodyneRadioSouce[] currentRadioSources) // получение источников 
        {
            try
            {
                Protocols.HeterodyneRadioSourcesResponse temp = new Protocols.HeterodyneRadioSourcesResponse();
                int stepKHz = 1000;
                if (data.IsFRCH) // определяем тип кодограммы для КГ
                {
                    temp = Task.Run(
                        async () => await clientServer.HeterodyneRadioSources(
                                        (data.Current.FreqStart - stepKHz),
                                        (data.Current.FreqEnd + stepKHz),
                                        stepKHz,
                                        NumAveragePhase,
                                        NumAveragePelen)).GetAwaiter().GetResult();
                    //при запросе фрч, мы запршиваем у сервера диапазон [Freq-1MHz, Freq+1MHz], таким образом получаем на выходе три частоты с центральной
                    currentRadioSources = new Protocols.HeterodyneRadioSouce[] { temp.RadioSources[1] };

                }
                else // Сетка частот
                {
                    //из-за встроенного смещения в КГ, необходимо добавлять некоторое собственное смещение при запрсе данных у ПЛ
                    int Shift = 0;
                    if (data.Current.StepMHz == 8)
                    {
                        if (!this.IncludeShift)
                            Shift = 3750;
                    }
                    else if (data.Current.StepMHz % 2 != 0)
                    {
                        if (!IncludeShift)
                            Shift = data.Current.StepMHz * 1000 / 2;
                    }
                    else if (IncludeShift)
                        Shift = data.Current.StepMHz * 1000 / 2;

                    if (data.Current.StepMHz == 8)
                    {
                        temp = Task.Run(
                            async () => await clientServer.HeterodyneRadioSources(
                                            data.Current.FreqStart + Shift,
                                            (data.Current.FreqEnd + Shift) > data.MaxValRangeHeter
                                                ? (data.MaxValRangeHeter)
                                                : (data.Current.FreqEnd + Shift),
                                            (7500),
                                            NumAveragePhase,
                                            NumAveragePelen)).GetAwaiter().GetResult();
                    }
                    else
                    {
                        temp = Task.Run(
                            async () => await clientServer.HeterodyneRadioSources(
                                            data.Current.FreqStart + Shift,
                                            (data.Current.FreqEnd + Shift) > data.MaxValRangeHeter
                                                ? (data.MaxValRangeHeter)
                                                : (data.Current.FreqEnd + Shift),
                                            (data.Current.StepMHz * 1000),
                                            NumAveragePhase,
                                            NumAveragePelen)).GetAwaiter().GetResult();
                    }
                    
                    currentRadioSources = temp.RadioSources;

                }

                if (temp.Header.ErrorCode != 0)
                    return false;
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool OnSignal() //включение следующего диапазона 
        {
            DurationWork = new Stopwatch();
            DurationWork.Start();
            if (data.GetNextRow() == StructDataOLV.null_DataOLV)
                return false;  //завершен опрос
            else
            {
                StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.OnHeter, (byte)CorrectorAnalysisTypes.TypeMess.Heterodin); 

                if (data.IsFRCH)// определяем тип кодограммы для КГ
                {
                    clientHeter.OnFRCH(data.Current.FreqStart); //запрос фрч
                }
                else
                {
                    clientHeter.OnFielFreq(data.Current.FreqStart, data.Current.FreqEnd, data.Current.StepMHz, IncludeShift);//запросить сетку частот
                }
                return true;
            }
        }

        void Heter_ReceiveOFF(StructReceiveHeter receive) // ответ на ВЫКЛ излучения 
        {
            clientHeter.StopSend(); // пришло подтверждение отключения сигнала без ошибки, поэтому отключаем автоопрос 
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.ReciveHeter, (byte)CorrectorAnalysisTypes.TypeMess.Heterodin);
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.ReqServer, (byte)CorrectorAnalysisTypes.TypeMess.Server);

            Thread.Sleep(iTimeDelay); // добавляю задержку для проверки

            Protocols.HeterodyneRadioSourcesResponse radioSoursClierAir = new Protocols.HeterodyneRadioSourcesResponse();
            int stepKHz = 500;
            radioSoursClierAir = Task.Run(async () => await clientServer.HeterodyneRadioSources(data.MinValRangeHeter,
                data.MaxValRangeHeter - 1, stepKHz, 1, 1)).GetAwaiter().GetResult(); //при выключеном гетеродине собираем данные эфира 
            if (radioSoursClierAir.Header.ErrorCode != 0)
            {
                ErrorPauseWork = new strStory(CorrectorAnalysisTypes.MessStory.ServerErrSourceClearAir, (byte)CorrectorAnalysisTypes.ErrorCodes.StopWorkError);//в этом случае необходимо все начать с самого начала
                return;
            }
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.ReciveServer, (byte)CorrectorAnalysisTypes.TypeMess.Server); 

            objAnalySave.SetRadioSourcesСlierAir = radioSoursClierAir.RadioSources; // задаем лишь один раз источники в чистом эфире
            
            OnSignal();// работа начинается с включения первого диапазона
        }
        
        private void Heter_ReceiveOnSignal(StructReceiveHeter receive) // ответ на включение излучения
        {
            clientHeter.StopSend();
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.ReciveHeter, (byte)CorrectorAnalysisTypes.TypeMess.Heterodin);

            Thread.Sleep(iTimeDelay); // добавляю задержку для проверки

            FuncWork();

        }
        
        void Heter_Error(StructReceiveHeter receive)
        {
            if (receive.ErrorCode != ComHTRD.ErrorCRC)
            {
                clientHeter.StopSend();
                if (receive.cipher == ComHTRD.OffSignal)
                {
                    ErrorPauseWork = new strStory(CorrectorAnalysisTypes.MessStory.StopWork + CorrectorAnalysisTypes.MessStory.HeterError + clientHeter.FindErr(receive.ErrorCode),
                        (byte)CorrectorAnalysisTypes.ErrorCodes.StopWorkError);  //опрос завершен из-за ошибки КГ
                    return;
                }
               // objAnalySave.Save(); //сохраняем собранные до этого данные
                data.SetBackRow();//переводим на предыдущую строку, чтобы опрос продолжился с диапазона, на котором пришла ошибка от КГ
                ErrorPauseWork = new strStory(CorrectorAnalysisTypes.MessStory.PauseWork + CorrectorAnalysisTypes.MessStory.HeterError + clientHeter.FindErr(receive.ErrorCode),
                    (byte)CorrectorAnalysisTypes.ErrorCodes.PauseWorkError);  //опрос приостановлен из-за ошибки КГ
                return;
            }
        }

        void Heter_StopSend(byte bErrorSend) // в случае, когда был превышено кол-во запросов, а ответ не пришел или все время приходила ошибка 4 CRC Генератор
        {
            if (objAnalySave.IsClierAirNull)
            {
                ErrorPauseWork = new strStory(CorrectorAnalysisTypes.MessStory.StopWork + CorrectorAnalysisTypes.MessStory.HeterError + (bErrorSend == ComHTRD.NotError ? " Нет ответа от КГ" : clientHeter.FindErr(bErrorSend)),
                    (byte)CorrectorAnalysisTypes.ErrorCodes.StopWorkError);  //опрос завершен из-за КГ нет ответа или все время приходила ошибка СРС
                return;
            }

            //objAnalySave.Save(); //сохраняем собранные до этого данные
            string mess = CorrectorAnalysisTypes.MessStory.PauseWork + CorrectorAnalysisTypes.MessStory.HeterError + (bErrorSend == ComHTRD.NotError ? " Нет ответа от КГ" : clientHeter.FindErr(bErrorSend));

            data.SetBackRow();//переводим на предыдущую строку, чтобы опрос продолжился с диапазона, на котором пришла ошибка от КГ
            ErrorPauseWork = new strStory(mess, (byte)CorrectorAnalysisTypes.ErrorCodes.PauseWorkError);  //опрос приостановлен из-за КГ нет ответа или все время приходила ошибка СРС
        }
        /////////////////////////////////////////////////////        
    }    
}