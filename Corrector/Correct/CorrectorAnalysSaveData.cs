﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Corrector
{
    public class CorrectorAnalysSaveData
    {
        private short Azimut;
        Protocols.HeterodyneRadioSouce[] SourcesClierAir;
        int DispersionPhase;
        int SignalToNois;
        int deltaSearch = 150; // 150 = 15КГц * 10. погрешность частоы(кг гетеродин генерирует сигнал на определенной частоте, а приемник определяет ее с некоторой погрешностью)
        Dictionary<int, Protocols.CalibrationCorrectionSignal> DictionarySaveData = new Dictionary<int, Protocols.CalibrationCorrectionSignal>();

        public Protocols.HeterodyneRadioSouce[] SetRadioSourcesСlierAir
        {
            set { SourcesClierAir = value; }
        }
        public bool IsClierAirNull
        {
            get { return SourcesClierAir == null; }
        }
        public int CountDataSave
        {
            get { return DictionarySaveData.Count; }
        }
        public CorrectorAnalysSaveData(short azimut, CorrectorAnalysisTypes.Parameters param)
        {
            this.Azimut = azimut;
            this.DispersionPhase = param.DispersionPhase;
            this.SignalToNois = param.SignalToNois;
        }

        public CorrectorAnalysisTypes.Parameters SetParams
        {
            set
            {
                this.DispersionPhase = value.DispersionPhase;
                this.SignalToNois = value.SignalToNois;
            }
        }
        public bool Save() //Сохранение данных из DictionaryDataSave 
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            path += @"\\DATA" + @"\";
            System.IO.Directory.CreateDirectory(path);
            path += DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".txt";
            using (FileStream fstream = new FileStream(path, FileMode.Append))
            {
                if (DictionarySaveData.Count != 0)
                {
                    foreach (int key in DictionarySaveData.Keys)
                    {
                        string strLine = "\r" + "\n" + DictionarySaveData[key].Frequency.ToString() + " " + DictionarySaveData[key].Direction.ToString();
                        for (int i = 0; i < DictionarySaveData[key].Phases.Length; i++)
                        {
                            strLine += " " + DictionarySaveData[key].Phases[i].ToString();
                        }
                        // преобразуем строку в байты
                        byte[] array = System.Text.Encoding.Default.GetBytes(strLine);
                        // запись массива байтов в файл
                        fstream.Write(array, 0, array.Length);
                    }
                    return true;
                }
                return false;//нет данных для сохранения 
            }
        }

        public byte Analysis(Protocols.HeterodyneRadioSouce[] currentSources)
        {
            List<Protocols.CalibrationCorrectionSignal> BufDataSave = new List<Protocols.CalibrationCorrectionSignal>(); //лист, в котором будут храниться отфильтрованные данные
            Protocols.CalibrationCorrectionSignal temp; // временная переменная, для хранения данных на текущем шаге
            if (currentSources.Length != 0)
            {
                for (int i = 0; i < currentSources.Length; i++)
                {
                    // сперва необходимо определить индекс частоты( в массиве с источниками в чистом эфире), соответствующей частоте найдено с включенным гетеродином
                    int index = GetIndexFreqClierAir(currentSources[i].Frequency, deltaSearch);

                    if (index > 0) // если частота найдена в массиве источников, собранных в чистом эфире
                    {
                        //Определение занятости частоты
                        if (currentSources[i].SignalToNoiseRatio - SourcesClierAir[index].SignalToNoiseRatio <= SignalToNois)
                            continue;
                    }
                    //Отсеивание сигналов с низким уровнем
                    if (currentSources[i].SignalToNoiseRatio <= SignalToNois)
                        continue;
                    //Отсеивание сигналов с высокой долей разброса
                    if (currentSources[i].PhasesDeviation > DispersionPhase)
                        continue;

                    temp = new Protocols.CalibrationCorrectionSignal();
                    temp.Frequency = currentSources[i].Frequency;
                    temp.Direction = Azimut;
                    temp.Phases = currentSources[i].Phases;
                    BufDataSave.Add(temp);
                }
            }
            LoadDataInDictionary(BufDataSave);
            return CalculateResult(currentSources.Length, BufDataSave.Count); // передаем результат
        }

        byte CalculateResult(int lengthOriginal, int lengthFiltered) //Результат зависит от кол-ва пораженных частот 
        {
            double NumBadFreq = ((lengthOriginal - lengthFiltered) * 100) / lengthOriginal;
            if (NumBadFreq <= 10.0d && NumBadFreq >= 0)
                return (byte)CorrectorDataOLV.Status.Great;
            else if (NumBadFreq > 10.0d && NumBadFreq <= 50.0d)
                return (byte)CorrectorDataOLV.Status.Normally;
            return (byte)CorrectorDataOLV.Status.Badly;
        }

        // модифицированный метод двоичного поиска, который возвращает индекс 
        //частоты из массива Эфира приблизительно равный частоте, полученной после генерации, 
        // частоты с КГ х10 => 30МГц - 30 000 0. Погрешность зависит от приемников, но примерно 300 КГц, следовательно дельта = 3000
        private int GetIndexFreqClierAir(int FreqKHz, int delta) //ModifMethodDoubleSearch 
        {
            int left = 0;
            int right = SourcesClierAir.Length - 1;
            int midd;
            while (true)
            {
                midd = (int)Math.Floor((left + right) / 2.0d);
                if (Math.Abs(SourcesClierAir[midd].Frequency - FreqKHz) <= delta)
                    return midd;
                else if (Math.Abs(SourcesClierAir[midd].Frequency - FreqKHz) > delta && (SourcesClierAir[midd].Frequency - FreqKHz) < 0)
                {
                    left = midd + 1;
                }
                else if (Math.Abs(SourcesClierAir[midd].Frequency - FreqKHz) > delta && (SourcesClierAir[midd].Frequency - FreqKHz) > 0)
                {
                    right = midd - 1;
                }
                if (right < left) // в случае, границы сомкнулись
                    return -1;
            }
        }

        private int AverFreq(int FreqKHzX10)
        {
            double deltaAver = 10000.0d;
            double temp = Math.Round(FreqKHzX10 / deltaAver, 1);
            if (Math.IEEERemainder(temp, Math.Floor(temp)) > 0.5)
            {
                temp = Math.Round(temp);
            }
            return (int)(deltaAver * temp);
        }

        private void LoadDataInDictionary(List<Protocols.CalibrationCorrectionSignal> Buf) // выгрузка данных в словарь 
        {
            for (int i = 0; i < Buf.Count; i++)
            {
                int key = AverFreq(Buf[i].Frequency);
                if (DictionarySaveData.ContainsKey(key))
                    DictionarySaveData[key] = Buf[i];
                else
                    DictionarySaveData.Add(key, Buf[i]);
            }
        }
    }
}
