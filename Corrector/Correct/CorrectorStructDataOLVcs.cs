﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corrector
{
    public class StructDataOLV
    {
        public static StructDataOLV null_DataOLV = new StructDataOLV(0, 0, 0, 0, true);
        public int FreqStart;
        public int FreqEnd;
        public byte StepMHz;
        public bool IncludRow;
        public int CountFreq;
        public StructDataOLV(int freqStart, int freqEnd, byte stepMHz, int countFreq, bool includeRow)
        {
            this.FreqStart = freqStart;
            this.FreqEnd = freqEnd;
            this.StepMHz = stepMHz;
            this.IncludRow = includeRow;
            this.CountFreq = countFreq;
        }
        public StructDataOLV()
        {
            this.FreqStart = 0;
            this.FreqEnd = 0;
            this.StepMHz = 0;
            this.CountFreq = 0;
            this.IncludRow = true;
        }

        public static bool operator ==(StructDataOLV stract1, StructDataOLV stract2)
        {
            if (stract1.FreqStart == stract2.FreqStart &&
                stract1.FreqEnd == stract2.FreqEnd &&
                stract1.StepMHz == stract2.StepMHz)
                return true;
            else
                return false;
        }

        public static bool operator !=(StructDataOLV stract1, StructDataOLV stract2)
        {
            if (stract1.FreqStart != stract2.FreqStart &&
                stract1.FreqEnd != stract2.FreqEnd &&
                stract1.StepMHz != stract2.StepMHz)
                return true;
            else
                return false;
        }
    }
}
