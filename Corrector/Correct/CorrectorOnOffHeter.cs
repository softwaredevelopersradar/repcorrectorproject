﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CNT;

namespace Corrector
{
    class CorrectorOnOffHeter 
    {
        ComHTRD clientHeter;
        CorrectorAnalysisTypes.Parameters Params;
        public delegate void DelStoryWork(string mess, byte type); 
        static public event DelStoryWork evenStoryWork;//отправка каждого шага работы
        
        private struct strStory
        {
            public string mess;
            public strStory(string sMess)
            {
                this.mess = sMess;
            }
        }

        private strStory StoryWork
        {
            set
            {
                if (evenStoryWork != null)
                { evenStoryWork(value.mess, (byte)CorrectorAnalysisTypes.TypeMess.Heterodin); }
            }
        }


        public CorrectorOnOffHeter(ref ComHTRD ClientHeter )
        {
            this.clientHeter = ClientHeter;
            InitEventHeter();
        }

        private void SetParamHeter()
        {
            ComHTRD.TimeSendHeter = Params.TimeSend;
            ComHTRD.MaxCountSend = Params.MaxCountSend;
            ComHTRD.DurationRadiation = Params.DurationRadiation;
        }

        private void InitEventHeter()
        {
            ComHTRD.OnReceiveCmdOff += Heter_ReceiveOFF;
            ComHTRD.OnReceiveCmdOnSignal += Heter_ReceiveOnSignal;
            ComHTRD.OnReceiveCmdError += Heter_Error;
            ComHTRD.eventStopSend += Heter_StopSend;
        }

        public void Dispose()
        {
            clientHeter.StopSend();
            ComHTRD.OnReceiveCmdOff -= Heter_ReceiveOFF;
            ComHTRD.OnReceiveCmdOnSignal -= Heter_ReceiveOnSignal;
            ComHTRD.OnReceiveCmdError -= Heter_Error;
            ComHTRD.eventStopSend -= Heter_StopSend;
        }


        public static void DisposeInstance(ref CorrectorOnOffHeter instance)
        {
            if (instance != null)
            {
                instance.Dispose();
                instance = null;
            }
        }

        public void OnSignalFRCH(int FrequencyKHz, CorrectorAnalysisTypes.Parameters param) //запрос фрч
        {
            this.Params = param;
            SetParamHeter();
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.OnHeter + " Частота: " + FrequencyKHz.ToString());
            clientHeter.OnFRCH(FrequencyKHz);             
        }

        public void OnSignalFielFreq(int FreqStartKHz, int FreqEndKHz, byte StepMHz, CorrectorAnalysisTypes.Parameters param) //запрос сетки частот
        {
            this.Params = param;
            SetParamHeter();
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.OnHeter + " Диапазон: " + 
                FreqStartKHz.ToString() + " - " + FreqEndKHz.ToString() + " Шаг: " + StepMHz.ToString());
            clientHeter.OnFielFreq(FreqStartKHz, FreqEndKHz, StepMHz, Params.IncludeShift);
        }

        public void OffSignal(CorrectorAnalysisTypes.Parameters param)
        {
            this.Params = param;
            SetParamHeter();
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.OffHeter);
            clientHeter.OFFSignal();
        }

        void Heter_ReceiveOFF(StructReceiveHeter receive) // ответ на ВЫКЛ излучения 
        {
            clientHeter.StopSend(); // пришло подтверждение отключения сигнала без ошибки, поэтому отключаем автоопрос 
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.ReciveHeter);   
        }

        private void Heter_ReceiveOnSignal(StructReceiveHeter receive) // ответ на включение излучения
        {
            clientHeter.StopSend();
            StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.ReciveHeter);
        }

        void Heter_Error(StructReceiveHeter receive)
        {
            if (receive.ErrorCode != ComHTRD.ErrorCRC)
            {
                clientHeter.StopSend();
                if (receive.cipher == ComHTRD.OffSignal)
                {
                    StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.StopWork + CorrectorAnalysisTypes.MessStory.HeterError + clientHeter.FindErr(receive.ErrorCode));  //опрос завершен из-за ошибки КГ
                    return;
                }             
                
                StoryWork = new strStory(CorrectorAnalysisTypes.MessStory.PauseWork + CorrectorAnalysisTypes.MessStory.HeterError + clientHeter.FindErr(receive.ErrorCode));  //опрос приостановлен из-за ошибки КГ
                return;
            }
        }

        void Heter_StopSend(byte bErrorSend) // в случае, когда был превышено кол-во запросов, а ответ не пришел или все время приходила ошибка 4 CRC Генератор
        {
            string mess = CorrectorAnalysisTypes.MessStory.PauseWork + CorrectorAnalysisTypes.MessStory.HeterError + (bErrorSend == ComHTRD.NotError ? " Нет ответа от КГ" : clientHeter.FindErr(bErrorSend));
                        
            StoryWork = new strStory(mess);  //опрос приостановлен из-за КГ нет ответа или все время приходила ошибка СРС
        }
        
    }
}
