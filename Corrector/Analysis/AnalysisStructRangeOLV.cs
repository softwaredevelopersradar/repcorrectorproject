﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corrector
{
    class AnalysisStructRangeOLV
    {
        public static StructDataOLV null_DataOLV = new StructDataOLV(0, 0, 0, 0, true);
        public int FreqStart;
        public int FreqEnd;
        public byte StepMHz;
        public AnalysisArrayFreq ArrayFrequencies;
        public AnalysisStructRangeOLV(int freqStart, int freqEnd, byte stepMHz, AnalysisArrayFreq massivRows)
        {
            this.FreqStart = freqStart;
            this.FreqEnd = freqEnd;
            this.ArrayFrequencies = massivRows;
            this.StepMHz = stepMHz;
        }
        public AnalysisStructRangeOLV()
        {
            this.FreqStart = 0;
            this.FreqEnd = 0;
            this.StepMHz = 0;
            this.ArrayFrequencies = null;
        }

    }
}
