﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using BrightIdeasSoftware;

namespace Corrector
{
    class AnalysisArrayFreq : IEnumerable
    {
        List<AnalysisSetFreq> ArrayFreq;

        public delegate void DelChangeCurrentRow(int CurrentRow);

        public static event DelChangeCurrentRow evenChangeCurrentRow = (int row) => { };

        public AnalysisArrayFreq(ObjectListView OLV, List<int> listIndex)
        {
            ArrayFreq = new List<AnalysisSetFreq>();
            for (int i = 0; i < listIndex.Count; i++)
            {
                ArrayFreq.Add(new AnalysisSetFreq(OLV, listIndex[i]));
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < ArrayFreq.Count; i++)
                yield return ArrayFreq[i];
        }

        public AnalysisSetFreq this[int index]
        {
            set { ArrayFreq[index] = value; }
            get { return ArrayFreq[index]; }
        }

        public int Length
        {
            get { return ArrayFreq.Count; }
        }

        //public bool GoToNextRow()
        //{
        //    indexCurrentRow++;
        //    evenChangeCurrentRow(indexCurrentRow);

        //    if (indexCurrentRow > ListIndex.Count)
        //        return false;
        //    else
        //        return true; 
        //}

        //public void Reset()
        //{
        //    indexCurrentRow = -1;
        //}

        //public byte Result
        //{
        //    set
        //    {
        //        try
        //        {
        //            if (indexCurrentRow < 0)
        //                return;
        //            if (olv.InvokeRequired)
        //            {
        //                olv.Invoke((System.Windows.Forms.MethodInvoker)(delegate()
        //                {
        //                    olv.Objects.Cast<InfoTableAnalysis>().ElementAt<InfoTableAnalysis>(ListIndex[indexCurrentRow]).Result = value;
        //                    olv.Refresh();
        //                }));
        //            }
        //            else
        //            {
        //                olv.Objects.Cast<InfoTableAnalysis>().ElementAt<InfoTableAnalysis>(ListIndex[indexCurrentRow]).Result = value;
        //                olv.Refresh();
        //            }
        //        }
        //        catch { }
        //    }
        //}
    }
}
