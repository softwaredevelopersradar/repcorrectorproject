﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using BrightIdeasSoftware;
using System.IO;

namespace Corrector
{
    class AnalysisDataOLV : IEnumerable
    {
        ObjectListView olv;
        List<AnalysisStructRangeOLV> MassivRanges;
        int indexCurrentRange;
        int maxValFreq;
        int minValFreq;

        public delegate void DelChangeCurrentRow(int CurrentRow);

        public static event DelChangeCurrentRow evenChangeCurrentRow = (int row) => { };
        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < MassivRanges.Count; i++)
                yield return MassivRanges[i];
        }

        public AnalysisDataOLV(ObjectListView OLV, int maxValueFeqMHz, int minValueFreqMHz)
        {            
            this.olv = OLV;
            maxValFreq = maxValueFeqMHz * 1000;
            minValFreq = minValueFreqMHz * 1000;
            LoadData();   
        }

        void LoadData()
        {
            indexCurrentRange = -1;
            MassivRanges = new List<AnalysisStructRangeOLV>();
            AnalysisStructRangeOLV temp;

            for (int i = 0; i < olv.Items.Count; i++)
            {
                temp = new AnalysisStructRangeOLV();
                List<int> indexes = new List<int>();
                temp.StepMHz = Convert.ToByte(olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(i).Step);
                temp.FreqStart =(int)Math.Round(Convert.ToDouble(olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(i).Frequency),MidpointRounding.AwayFromZero);
                if (olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(i).HiddenRange == "")
                {
                    indexes.Add(i);
                    temp.FreqEnd = temp.FreqStart;
                }
                else
                {
                    string[] stemp = olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(i).HiddenRange.Split(new Char[] { ' ', '-' });
                    temp.FreqStart = Convert.ToInt32(stemp[0]) * 1000;
                    temp.FreqEnd = Convert.ToInt32(stemp[1]) * 1000;
                    int lastIndex = i + (int)((Convert.ToInt32(stemp[1]) - Convert.ToInt32(stemp[0])) / temp.StepMHz);
                    int counter = 0;
                    for (; i <= lastIndex; i++)
                    {
                        olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(i).Frequency = (temp.FreqStart + temp.StepMHz*1000 * counter).ToString();
                        counter++;
                        indexes.Add(i);
                    }
                    i--;
                }
                temp.ArrayFrequencies = new AnalysisArrayFreq(olv, indexes);
                MassivRanges.Add(temp);
            }
        }

        public AnalysisStructRangeOLV this[int index]
        {
            get { return MassivRanges[index]; }
            set { MassivRanges[index] = value; }
            
        }

        public int Length
        {
            get { return MassivRanges.Count; }
        }

        public AnalysisStructRangeOLV GetNextRange()
        {
            indexCurrentRange++;
            evenChangeCurrentRow(indexCurrentRange);
            if (indexCurrentRange < MassivRanges.Count)
                return MassivRanges[indexCurrentRange];
            else return null;

        }

        public void Reset()
        {
            indexCurrentRange = -1;
            try
            {
                foreach(AnalysisStructRangeOLV arrayFreq in MassivRanges)
                    foreach(AnalysisSetFreq setFreq in arrayFreq.ArrayFrequencies )
                    {
                        setFreq.Result = (byte)CorrectorAnalysisTypes.Status.None;
                        setFreq.Delta = "";
                        setFreq.Direction = "";
                    }
            }
            catch { }
        }

        public void SetBackRow()
        {
            indexCurrentRange--;
            indexCurrentRange = (indexCurrentRange < -1) ? -1 : indexCurrentRange;
        }

        public AnalysisStructRangeOLV CurrentRange
        {
            get
            {
                try
                {
                    return MassivRanges[indexCurrentRange];
                }
                catch { return null; }
            }
        }
        private List<double> massDelta;
        public List<double> Delta
        {
            get
            {
                try
                {
                    if (massDelta == null)
                        massDelta = (from t in olv.Objects.Cast<AnalysisInfoTable>() select Convert.ToDouble(t.Delta)).ToList<double>();
                    return massDelta;
                }
                catch
                { return null; }
            }
        }

        public bool IsFRCH
        {
            get
            {
                if (MassivRanges[indexCurrentRange].FreqStart == MassivRanges[indexCurrentRange].FreqEnd)
                    return true;
                return false;
            }
        }

        public int MaxValueFreq
        { get { return maxValFreq; } }

        public int MinValueFreq
        { get { return minValFreq; } }

        public int CountRow
        {
            get
            {
                return MassivRanges.Count;
            }
        }
        
        public bool Save(int azimut) //Сохранение данных из DictionaryDataSave 
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            path += @"\\Analysis" + @"\";
            System.IO.Directory.CreateDirectory(path);
            path +=DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".txt";
            using (FileStream fstream = new FileStream(path, FileMode.Append))
            {
                if (olv.Items.Count != 0)
                {
                    string strLine = "\r" + "\n" + "Azimut = " + azimut.ToString(); 
                    byte[] array = System.Text.Encoding.Default.GetBytes(strLine);
                    fstream.Write(array, 0, array.Length);
                    for (int i = 0; i<olv.Items.Count; i++)
                    {
                        strLine = "\r" + "\n" + Convert.ToDouble(olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(i).Frequency).ToString("########.#")
                            + "     " + olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(i).Direction.ToString();

                        // преобразуем строку в байты
                        array = System.Text.Encoding.Default.GetBytes(strLine);
                        // запись массива байтов в файл
                        fstream.Write(array, 0, array.Length);
                    }
                    return true;
                }
                return false;//нет данных для сохранения 
            }
        }

    }

}
