﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CNT;
using DspDataModel;
using DspProtocols;
using System.Diagnostics;

namespace Corrector
{
    class AnalysisAutoCollect
    {
        ComHTRD clientHeter;
        AWPtoBearingDSPprotocolNew clientServer;
        AnalysisDataOLV data;
        CorrectorAnalysisTypes.Parameters Params;
        private static bool bWork = false;
        int Azimut;
                
        public delegate void DelStoryWork(string mess, byte typeDevice); // typeDevice : 0 - KГ; 1 - ПЛ
        static public event DelStoryWork evenStoryWork;//отправка каждого шага работы

        private struct strStory
        {
            public string mess;
            public byte type;
            public double std;
            public strStory(string sMess, byte iType, double dstd)
            {
                this.mess = sMess;
                this.type = iType;
                this.std = dstd;
            }
        }

        private strStory StoryWork
        {
            set
            {
                if (evenStoryWork != null)
                { evenStoryWork(value.mess, value.type); }
            }
        }
        
        public delegate void DelStopWork(string mess, byte error, double std);
        public event DelStopWork evenErrorPauseWork; //Перевод режима работы - Пауза, для переподключения КГ или ПЛ из-зи ошибок

        private strStory ErrorPauseWork
        {
            set 
            {
                if (evenErrorPauseWork != null)
                    evenErrorPauseWork(value.mess, value.type, value.std);
            }
        }

        public delegate void DelProgressResult(int CurrResult, int MaxValue);
        public event DelProgressResult evenGetResult; //событи на получение результата(кол-во пройденых частот + одна итерация на сохранение) 
        
        private struct strProgress 
        {
           public int iProgressResult;
           public int iMaxValue;
           public strProgress(int iResult, int imaxVal)
           {
               this.iProgressResult = iResult;
               this.iMaxValue = imaxVal;
           }
        }
        private strProgress ProgressResult
        {
            set 
            {
                if (evenGetResult != null)
                    evenGetResult(value.iProgressResult, value.iMaxValue);
            }
        }
        
        struct MessStory
        {
            static public string OffHeter = "Запрос КГ: Отключение излучения";
            static public string OnHeter = "Запрос КГ: Включение излучения";
            static public string ReciveHeter = "Ответ КГ: запрос прошел без ошибок";
            static public string ReqServer = "Запрос ПЛ: запрос источников";
            static public string ReciveServer = "Ответ ПЛ: запрос прошел без ошибок";
            static public string EndWork = "Опрос завершен!";
            static public string StopWork = "Опрос Остановлен! ";
            static public string PauseWork = "Опрос Приостановлен! ";
            static public string ContinueWork = "Опрос Возобнавлен! ";
            static public string StartWork = "Опрос Начался";
            static public string ServerErrGetSource = "Ошибка сервера! Сбор источников с включенным КГ";
            static public string ServerErrSourceClearAir = "Ошибка Сервера!Не удалось сборать источники в чистом эфире";
            static public string HeterError = "Ошибка КГ! ";
        }
        struct AnalysisResult
        {
            public int greatResult;
            public int normalResultStart;
            public int normalResultStop;
            public int badResult;
            public AnalysisResult(int GreatResult, int NormResultStart, int NormResultStop, int BadResult)
            {
                this.greatResult = GreatResult;
                this.normalResultStart = NormResultStart;
                this.normalResultStop = NormResultStop;
                this.badResult = BadResult;
            }
        }
        public AnalysisAutoCollect(ref ComHTRD ClientHeter, ref AWPtoBearingDSPprotocolNew ClientServer)
        {
            this.clientHeter = ClientHeter;
            this.clientServer = ClientServer;  
            InitEventHeter();
        }

        public void Dispose()
        {
            ComHTRD.OnReceiveCmdOnSignal -= Heter_ReceiveOnSignal;
            ComHTRD.OnReceiveCmdError -= Heter_Error;
            ComHTRD.eventStopSend -= Heter_StopSend;
        }

        private void SetParamHeter()
        {
            ComHTRD.TimeSendHeter = Params.TimeSend;
            ComHTRD.MaxCountSend = Params.MaxCountSend;
            ComHTRD.DurationRadiation = Params.DurationRadiation;
        }

        public void Start(short azimut, CorrectorAnalysisTypes.Parameters param, ref AnalysisDataOLV dataOLV)
        {
            StoryWork = new strStory(MessStory.StartWork, (byte)CorrectorAnalysisTypes.TypeMess.Attention, 0); // обрабатывает каждый шаг работы
            ErrorPauseWork = new strStory("", (byte)CorrectorAnalysisTypes.ErrorCodes.StartWorkClient, 0);
            this.Azimut = azimut;
            this.data = dataOLV;
            this.Params = param;
            SetParamHeter();
            bWork = true;
            data.Reset();
            OnSignal();
        }

        public void Stop()
        {
            clientHeter.StopSend();
            StoryWork = new strStory(MessStory.StopWork, (byte)CorrectorAnalysisTypes.TypeMess.Attention, 0);
            ErrorPauseWork = new strStory("", (byte)CorrectorAnalysisTypes.ErrorCodes.StopWorkClient, 0);
            bWork = false;
            data.Reset();
        }

        public void Pause()
        {
            clientHeter.StopSend();
            StoryWork = new strStory(MessStory.PauseWork, (byte)CorrectorAnalysisTypes.TypeMess.Attention, 0);
            data.SetBackRow();
            ErrorPauseWork = new strStory("", (byte)CorrectorAnalysisTypes.ErrorCodes.PauseWorkClient, 0);
            bWork = false;
        }

        public void Continue(ref ComHTRD ClientHeter, ref AWPtoBearingDSPprotocolNew ClientServer, CorrectorAnalysisTypes.Parameters param)
        {
            StoryWork = new strStory(MessStory.ContinueWork, (byte)CorrectorAnalysisTypes.TypeMess.Attention, 0);
            ErrorPauseWork = new strStory("", (byte)CorrectorAnalysisTypes.ErrorCodes.ContinueWorkClient, 0);
            this.clientHeter = ClientHeter;
            this.clientServer = ClientServer;
            this.Params = param;
            SetParamHeter();
            bWork = true;
            OnSignal();
        }

        private void InitEventHeter()
        {
            ComHTRD.OnReceiveCmdOnSignal += Heter_ReceiveOnSignal;
            ComHTRD.OnReceiveCmdError += Heter_Error;
            ComHTRD.eventStopSend += Heter_StopSend;
        }

        //////////////////////WORK///////////////////////////

        private void FuncWork() // главная функция управления АвтоОпросом 
        {
            if (!bWork)//При переводе режима работы в Паузу или Стоп процесс завершит начатый шаг, но к следующему не перейдет 
               return;

            StoryWork = new strStory(MessStory.ReqServer, (byte)CorrectorAnalysisTypes.TypeMess.Server, 0); 

            Protocols.HeterodyneRadioSouce[] currentRadioSources = new Protocols.HeterodyneRadioSouce[] { };
            bool answerServer = RadioSources(ref currentRadioSources);
            if (!bWork) 
                return;
            if (!answerServer) // получение источников с ВКЛ КГ
            {
                ErrorPauseWork = new strStory(MessStory.ServerErrGetSource, (byte)CorrectorAnalysisTypes.ErrorCodes.PauseWorkError, 0);  //перевод в режим ожидания с ошибкой
                return;
            }
            StoryWork = new strStory(MessStory.ReciveServer, (byte)CorrectorAnalysisTypes.TypeMess.Server, 0);
            for (int i = 0; i < currentRadioSources.Length; i++) 
            {
                data.CurrentRange.ArrayFrequencies[i].Direction = (currentRadioSources[i].Direction/10.0d).ToString();
                data.CurrentRange.ArrayFrequencies[i].Frequency = (currentRadioSources[i].Frequency / 10.0d).ToString();
                Int16 delta = (Int16)(Azimut - currentRadioSources[i].Direction / 10.0);

                if (currentRadioSources[i].SignalToNoiseRatio == 0)
                {
                    delta = -361;
                    data.CurrentRange.ArrayFrequencies[i].Direction = 361.ToString();
                }
                else
                {
                    if (delta > 180) { delta -= 360; }
                    if (delta < -180) { delta += 360; }
                    delta = (Int16)(0 - delta); 
                }
                data.CurrentRange.ArrayFrequencies[i].Delta =Math.Abs(delta).ToString();
                data.CurrentRange.ArrayFrequencies[i].Result = FindAnalysisResult(delta);
            }
            if (!OnSignal()) // включение сигнала
            {

                ErrorPauseWork = new strStory(MessStory.EndWork, (byte)CorrectorAnalysisTypes.ErrorCodes.EndWork, CalculateStd(data.Delta));  //завершен опрос
                return;
            }
        }

        private bool RadioSources(ref Protocols.HeterodyneRadioSouce[] currentRadioSources) // получение источников 
        {
            try
            {
                Protocols.HeterodyneRadioSourcesResponse temp = new Protocols.HeterodyneRadioSourcesResponse();
                int stepKHz = 1000;
                if (data.IsFRCH) // определяем тип кодограммы для КГ
                {
                    temp = Task.Run(
                        async () => await clientServer.HeterodyneRadioSources(
                                        (data.CurrentRange.FreqStart - stepKHz),
                                        (data.CurrentRange.FreqEnd + stepKHz),
                                        stepKHz,
                                        Params.NumAveragePhase,
                                        Params.NumAveragePelen)).GetAwaiter().GetResult();
                    //при запросе фрч, мы запршиваем у сервера диапазон [Freq-1MHz, Freq+1MHz], таким образом получаем на выходе три частоты с центральной
                    currentRadioSources = new Protocols.HeterodyneRadioSouce[] { temp.RadioSources[1] };

                }
                else // Сетка частот
                {
                    //из-за встроенного смещения в КГ, необходимо добавлять некоторое собственное смещение при запрсе данных у ПЛ
                    int Shift = 0;
                    if (data.CurrentRange.StepMHz == 8)
                    {
                        if (!this.Params.IncludeShift)
                            Shift = 3750;
                    }
                    else if (data.CurrentRange.StepMHz % 2 != 0)
                    {
                        if (!Params.IncludeShift)
                            Shift = data.CurrentRange.StepMHz * 1000 / 2;
                    }
                    else if (Params.IncludeShift)
                        Shift = data.CurrentRange.StepMHz * 1000 / 2;

                    if (data.CurrentRange.StepMHz == 8)
                    {
                        temp = Task.Run(
                            async () => await clientServer.HeterodyneRadioSources(
                                            data.CurrentRange.FreqStart + Shift,
                                            (data.CurrentRange.FreqEnd + Shift) > data.MaxValueFreq
                                                ? (data.MaxValueFreq)
                                                : (data.CurrentRange.FreqEnd + Shift),
                                            (7500),
                                            Params.NumAveragePhase,
                                            Params.NumAveragePelen)).GetAwaiter().GetResult();
                    }
                    else
                    {
                        temp = Task.Run(
                            async () => await clientServer.HeterodyneRadioSources(
                                            data.CurrentRange.FreqStart + Shift,
                                            (data.CurrentRange.FreqEnd + Shift) > data.MaxValueFreq
                                                ? (data.MaxValueFreq)
                                                : (data.CurrentRange.FreqEnd + Shift),
                                            (data.CurrentRange.StepMHz * 1000),
                                            Params.NumAveragePhase,
                                            Params.NumAveragePelen)).GetAwaiter().GetResult();
                    }

                    currentRadioSources = temp.RadioSources;
                }

                if (temp.Header.ErrorCode != 0)
                    return false;
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool OnSignal() //включение следующего диапазона 
        {
            if (data.GetNextRange() == null)
                return false;  //завершен опрос
            else
            {
                StoryWork = new strStory(MessStory.OnHeter, (byte)CorrectorAnalysisTypes.TypeMess.Heterodin, 0); 

                if (data.IsFRCH)// определяем тип кодограммы для КГ
                {
                    clientHeter.OnFRCH(data.CurrentRange.FreqStart); //запрос фрч
                }
                else
                {
                    clientHeter.OnFielFreq(data.CurrentRange.FreqStart, data.CurrentRange.FreqEnd, data.CurrentRange.StepMHz, Params.IncludeShift);//запросить сетку частот
                }
                return true;
            }
        }
                
        private void Heter_ReceiveOnSignal(StructReceiveHeter receive) // ответ на включение излучения
        {
            clientHeter.StopSend();
            StoryWork = new strStory(MessStory.ReciveHeter, (byte)CorrectorAnalysisTypes.TypeMess.Heterodin, 0);

            Thread.Sleep(Params.TimeDelayHeter); // добавляю задержку для проверки

            FuncWork();

        }
        
        void Heter_Error(StructReceiveHeter receive)
        {
            if (receive.ErrorCode != ComHTRD.ErrorCRC)
            {
                clientHeter.StopSend();
                if (receive.cipher == ComHTRD.OffSignal)
                {
                    ErrorPauseWork = new strStory(MessStory.StopWork + MessStory.HeterError + clientHeter.FindErr(receive.ErrorCode),
                        (byte)CorrectorAnalysisTypes.ErrorCodes.StopWorkError, 0);  //опрос завершен из-за ошибки КГ
                    return;
                }
                data.SetBackRow();//переводим на предыдущую строку, чтобы опрос продолжился с диапазона, на котором пришла ошибка от КГ
                ErrorPauseWork = new strStory(MessStory.PauseWork + MessStory.HeterError + clientHeter.FindErr(receive.ErrorCode),
                    (byte)CorrectorAnalysisTypes.ErrorCodes.PauseWorkError, 0);  //опрос приостановлен из-за ошибки КГ
                return;
            }
        }

        void Heter_StopSend(byte bErrorSend) // в случае, когда был превышено кол-во запросов, а ответ не пришел или все время приходила ошибка 4 CRC Генератор
        {
            string mess = MessStory.PauseWork + MessStory.HeterError + (bErrorSend == ComHTRD.NotError ? " Нет ответа от КГ" : clientHeter.FindErr(bErrorSend));

            data.SetBackRow();//переводим на предыдущую строку, чтобы опрос продолжился с диапазона, на котором пришла ошибка от КГ
            ErrorPauseWork = new strStory(mess, (byte)CorrectorAnalysisTypes.ErrorCodes.PauseWorkError, 0);  //опрос приостановлен из-за КГ нет ответа или все время приходила ошибка СРС
        }

        private byte FindAnalysisResult(double delta)
        { 
            if (Math.Abs(delta) < Params.GreatResult)
                return (byte)CorrectorAnalysisTypes.Status.Great;
            if (Math.Abs(delta) >= Params.NormalResultStart && Math.Abs(delta) < Params.NormalResultStop)
                return (byte)CorrectorAnalysisTypes.Status.Normally;
            else
                return (byte)CorrectorAnalysisTypes.Status.Badly;
        } //поиск резултата

        double CalculateStd(List<double> data)
        {
            if (data == null)
                return 0;
            List<double> newData = (from t in data where t != 361 select t).ToList();
            if (newData.Count == 0)
                return 361;
            double aver = newData.Average();
            double disp = (from t in data where t != 361 select Math.Pow((t - aver), 2)).Sum() / newData.Count();
            double std = Math.Round(Math.Sqrt(disp), 4);
            return std;
        }
        /////////////////////////////////////////////////////        
    }
}
