﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrightIdeasSoftware;

namespace Corrector
{
    class AnalysisSetFreq
    {
        ObjectListView olv;

        int indexCurrentFreq;

        public AnalysisSetFreq(ObjectListView OLV, int Index)
        {
            this.olv = OLV;
            this.indexCurrentFreq = Index;
        }

        public byte Result
        {
            set
            {
                try
                {
                    if (olv.InvokeRequired)
                    {
                        olv.Invoke((System.Windows.Forms.MethodInvoker)(delegate()
                        {
                            olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(indexCurrentFreq).Result = value;
                            olv.Refresh();
                        }));
                    }
                    else
                    {
                        olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(indexCurrentFreq).Result = value;
                        olv.Refresh();
                    }
                }
                catch { }
            }
        }

        public string Direction
        {
            set
            {
                try
                {
                    if (olv.InvokeRequired)
                    {
                        olv.Invoke((System.Windows.Forms.MethodInvoker)(delegate()
                        {

                            olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(indexCurrentFreq).Direction = value.ToString();
                            olv.RefreshObject(olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(indexCurrentFreq));
                        }));
                    }
                    else
                    {

                        olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(indexCurrentFreq).Direction = value.ToString();
                        olv.RefreshObject(olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(indexCurrentFreq));
                    }
                }
                catch { }
            }
        }

        public string Delta
        {
            set
            {
                try
                {
                    if (olv.InvokeRequired)
                    {
                        olv.Invoke((System.Windows.Forms.MethodInvoker)(delegate()
                        {

                            olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(indexCurrentFreq).Delta = value.ToString();
                            olv.RefreshObject(olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(indexCurrentFreq));

                        }));
                    }
                    else
                    {

                        olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(indexCurrentFreq).Delta = value.ToString();
                        olv.RefreshObject(olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(indexCurrentFreq));

                    }
                }
                catch { }
            }
        }

        public string Frequency
        {
            set
            {
                try
                {
                    if (olv.InvokeRequired)
                    {
                        olv.Invoke((System.Windows.Forms.MethodInvoker)(delegate()
                        {
                            olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(indexCurrentFreq).Frequency = value;
                            olv.Refresh();
                        }));
                    }
                    else
                    {
                        olv.Objects.Cast<AnalysisInfoTable>().ElementAt<AnalysisInfoTable>(indexCurrentFreq).Frequency = value;
                        olv.Refresh();
                    }
                }
                catch { }
            }
        }

    }
}
