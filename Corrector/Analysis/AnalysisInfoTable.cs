﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corrector
{
    class AnalysisInfoTable
    {
        public string Frequency { get; set; }
        public string Direction { get; set; }
        public int Result { get; set; }
        public string Delta { get; set; }
        public string HiddenRange { get; set; }
        public string Step { get; set; }

        public AnalysisInfoTable(string frequency, string direction, int result, string delta, string hiddenRange, string step)
        {
            Frequency = frequency;
            Result = result;
            Direction = direction;
            Delta = delta;
            HiddenRange = hiddenRange;
            Step = step;
        }
    }
}
