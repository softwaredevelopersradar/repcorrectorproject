﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Ini;
using System.Xml;
using System.IO.Ports;
using System.Diagnostics;

namespace Corrector
{
    partial class FormCorrector
    {
        private string KeyMhz = "KeyMHz";
        private double? StdDeltaNull = null;

        void SetControlsAnalysis() // установка значений по умолчанию
        {
            NumBoxAnalysisEPOStart.Minimum = 1;
            NumBoxAnalysisEPOStop.Minimum = 1;
            NumBoxAnalysisEPOStop.Maximum = RangesEPO.Length;
            NumBoxAnalysisEPOStart.Maximum = RangesEPO.Length;
            NumBoxAnalysisFreqStart.Minimum = Convert.ToInt32(RangesEPO[0].Split(new char[] { '-', ' ' })[0]) + 1;
            NumBoxAnalysisFreqStop.Minimum = Convert.ToInt32(RangesEPO[0].Split(new char[] { '-', ' ' })[0]) + 1;
            NumBoxAnalysisFreqStop.Maximum = Convert.ToInt32(RangesEPO[RangesEPO.Length - 1].Split(new char[] { '-', ' ' })[1]) - 1;
            NumBoxAnalysisFreqStart.Maximum = Convert.ToInt32(RangesEPO[RangesEPO.Length - 1].Split(new char[] { '-', ' ' })[1]) - 1;
            ColmAnalysisResult.Renderer = new BrightIdeasSoftware.MappedImageRenderer(new Object[] {
                "None", Properties.Resources.gray,
                "Normally", Properties.Resources.yellow,
                "Badly", Properties.Resources.red,
                "Great", Properties.Resources.green
            });
            ColmAnalysisResult.AspectGetter = delegate(object row)
            {
                switch (((AnalysisInfoTable)row).Result)
                {
                    case 0: //gray                        
                        return "None";
                    case 1: // yellow
                        return "Normally";
                    case 2: // red
                        return "Badly";
                    case 3: //green
                        return "Great";
                    default:
                        return "Badly";
                }
            };
        }

        void ChangeCheckedTypeReqAnalysis(object sender, EventArgs e)
        {
            GroupBoxFileFreqAnalysis.Visible = RadButAnalysisFileFreq.Checked;
            GroupBoxFreqAnalysis.Visible = RadButAnalysisFreq.Checked;
        }
        
        private void NumBoxAnalysisEPOStart_ValueChanged(object sender, EventArgs e)
        {
            LabAnalysisRangeStart.Text = RangesEPO[Convert.ToInt16(NumBoxAnalysisEPOStart.Value) - 1] + TranslateDic[KeyMhz];
        }

        private void NumBoxAnalysisEPOStop_ValueChanged(object sender, EventArgs e)
        {
            LabAnalysisRangeStop.Text = RangesEPO[Convert.ToInt16(NumBoxAnalysisEPOStop.Value) - 1] + TranslateDic[KeyMhz];
        }

        void AddRangeToAnalysisListView(object sender, EventArgs e)
        {
            if (!CheckRangeAnalysis())
                return;
            string sFrequency = "";
            if (RadButAnalysisFileFreq.Checked == true)
            {
                for (int i = (int)NumBoxAnalysisEPOStart.Value; i <= (int)NumBoxAnalysisEPOStop.Value; i++)//_+= (int)NumBoxStepFileFreq.Value)
                {
                    int FreqStart = Convert.ToInt32(RangesEPO[i - 1].Split(new char[] { '-', ' ' })[0]);
                    int FreqStop = Convert.ToInt32(RangesEPO[i - 1].Split(new char[] { '-', ' ' })[1]);

                    string sRange = FreqStart.ToString() + "-" + FreqStop.ToString();
                    for (int iFreq = FreqStart; iFreq <= FreqStop; iFreq += (int)NumBoxAnalysisEPOStep.Value)
                    {
                        sFrequency = (iFreq * 1000).ToString();
                        AnalysisInfoTable infoTable = new AnalysisInfoTable(sFrequency, "", 0, "", sRange, NumBoxAnalysisEPOStep.Value.ToString());
                        ObjListViewAnalysis.AddObject(infoTable);
                    }
                }
            }
            else
            {
                for (int i = (int)NumBoxAnalysisFreqStart.Value; i <= (int)NumBoxAnalysisFreqStop.Value; i += (int)NumBoxAnalysisFreqStep.Value)
                {
                    sFrequency = (i * 1000).ToString();
                    AnalysisInfoTable infoTable = new AnalysisInfoTable(sFrequency, "", 0, "", "", NumBoxAnalysisFreqStep.Value.ToString());
                    ObjListViewAnalysis.AddObject(infoTable);
                }
            }
        }

        private void ButDeleteAnalysisRange_Click(object sender, EventArgs e)
        {
            ObjListViewAnalysis.RemoveObjects(ObjListViewAnalysis.SelectedObjects);
        }

        private void ButClearAnalysis_Click(object sender, EventArgs e)
        {
            ObjListViewAnalysis.ClearObjects();
        }
        void objClassAnalysis_evenErrorPauseWork(string mess, byte ErrorCode, double StdDelta)
        {
            switch (ErrorCode)
            {
                case (byte)CorrectorAnalysisTypes.ErrorCodes.StartWorkClient: // Опрос начат

                    Invoke((MethodInvoker)(() =>
                    {
                        ButAddAnalysisRange.Enabled = false;
                        ButDeleteAnalysisRange.Enabled = false;
                        ButStartAnalysis.Enabled = false;
                        ButSaveDataAnalysis.Enabled = false;
                        ButStopAnalysis.Enabled = true;
                        ButPauseAnalysis.Enabled = true;
                        ButTestRadio.Enabled = true;

                        objFormParam.ControlsEnabled = false;
                        NumBoxAzimut.Enabled = false;
                        StdDeltaNull = null;
                        Invoke((MethodInvoker)(() => labStd.Text = TranslateDic[labStd.Name]));

                    }));
                    break;

                case (byte)CorrectorAnalysisTypes.ErrorCodes.EndWork: // Опрос завершен
                case (byte)CorrectorAnalysisTypes.ErrorCodes.StopWorkError: // остановка опроса из-за ошибки 
                case (byte)CorrectorAnalysisTypes.ErrorCodes.StopWorkClient:// остановка опроса из-за перевода клиентом
                    objClassAnalysis.Dispose();
                    objClassAnalysis = null;
                    Invoke((MethodInvoker)(() =>
                    {
                        ButPauseAnalysis.Enabled = false;
                        ButPauseAnalysis.Visible = true;
                        ButContinueAnalysis.Visible = false;
                        ButStopAnalysis.Enabled = false;
                        ButStartAnalysis.Enabled = true;
                        ButAddAnalysisRange.Enabled = true;
                        ButDeleteAnalysisRange.Enabled = true;
                        ButSaveDataAnalysis.Enabled = true;
                        ButTestRadio.Enabled = true;

                        objFormParam.ControlsEnabled = true;
                        NumBoxAzimut.Enabled = true;
                    }));
                    StdDeltaNull = StdDelta;
                    Invoke((MethodInvoker)(() => labStd.Text = TranslateDic[labStd.Name] + "  " + StdDelta.ToString()));
                    if (mess != "")
                    {
                        ShowMessage(mess, (byte)CorrectorAnalysisTypes.TypeMess.Attention);
                        MessageBox.Show(mess);
                    }
                    break;

                case (byte)CorrectorAnalysisTypes.ErrorCodes.PauseWorkError: //перевод в режим Паузы из-за ошибки 
                case (byte)CorrectorAnalysisTypes.ErrorCodes.PauseWorkClient: //перевод в режим паузы оператором

                    Invoke((MethodInvoker)(() =>
                    {
                        ButPauseAnalysis.Visible = false;
                        ButContinueAnalysis.Visible = true;
                        ButTestRadio.Enabled = true;

                        objFormParam.ControlsEnabled = true;

                        ButPLConnect.Focus();
                    }));
                    if (mess != "")
                    {
                        ShowMessage(mess, (byte)CorrectorAnalysisTypes.TypeMess.Attention);
                        MessageBox.Show(mess);
                    }
                    break;

                case (byte)CorrectorAnalysisTypes.ErrorCodes.ContinueWorkClient:

                    Invoke((MethodInvoker)(() =>
                    {
                        ButContinueAnalysis.Visible = false;
                        ButPauseAnalysis.Visible = true;
                        ButTestRadio.Enabled = false;

                        objFormParam.ControlsEnabled = false;
                    }));
                    break;
            }
        }

        bool CheckRangeAnalysis() //проверка корректности введенных данных в ПОО Старт/Стоп или Частота Старт/Стоп
        {
            if (RadButAnalysisFileFreq.Checked == true)
            {
                if (NumBoxAnalysisEPOStop.Value < NumBoxAnalysisEPOStart.Value)
                {
                    MessageBox.Show(TranslateDic[ErrorEntrDataEPO]);
                    NumBoxAnalysisEPOStop.Focus();
                    return false;
                }
            }
            else
            {
                if (NumBoxAnalysisFreqStop.Value < NumBoxAnalysisFreqStart.Value)
                {
                    MessageBox.Show(TranslateDic[ErrorEntrDataFreq]);
                    NumBoxAnalysisFreqStop.Focus();
                    return false;
                }
            }
            return true;

        }

        bool CheckParamAnalysis()
        {
            if (ObjListViewAnalysis.Objects == null)
            {
                MessageBox.Show("Необходимо заполнить таблицу!");
                return false;
            }
            if (!IsConnectServer())
            {
                MessageBox.Show("Необходимо подключиться к Серверу!");
                ButPLConnect.Focus();
                return false;
            }
            if (!IsConnectHeterodyn())
            {
                MessageBox.Show("Необходимо подключиться к КГ!");
                ButHeterConnect.Focus();
                return false;
            }
            return true;
        }

        private void ButStartAnalysis_Click(object sender, EventArgs e)
        {
            if (!CheckParamAnalysis())
                return;
            int minFreq = Convert.ToInt32(RangesEPO[0].Split(new char[] { '-', ' ' })[1]);
            int maxFreq = Convert.ToInt32(RangesEPO[RangesEPO.Length - 1].Split(new char[] { '-', ' ' })[1]);
            dataAnalysisOLV =  new AnalysisDataOLV(ObjListViewAnalysis, maxFreq, minFreq);
            if (objClassAnalysis == null)
            {
                objClassAnalysis = new AnalysisAutoCollect(ref ClientHeterodyn, ref ClientServer);
                objClassAnalysis.evenErrorPauseWork += objClassAnalysis_evenErrorPauseWork;
            }
            objClassAnalysis.Start(Convert.ToInt16(NumBoxAnalysisAzimut.Value), objParam, ref dataAnalysisOLV);
            //foreach (AnalysisStructRangeOLV Ranges in dataAnalysisOLV)
            //{
            //    foreach (AnalysisSetFreq frequency in Ranges.ArrayFrequencies)
            //    {
            //        frequency.Direction = (new Random()).Next(1, 100).ToString();
            //        frequency.Delta = (new Random()).Next(1, 100).ToString();
            //        frequency.Result = (byte)(new Random()).Next(1, 4);
            //    }
            //}
        }

        private void ButStopAnalysis_Click(object sender, EventArgs e)
        {
            objClassAnalysis.Stop();
        }

        private void ButPauseAnalysis_Click(object sender, EventArgs e)
        {
            objClassAnalysis.Pause();
        }

        private void ButContinueAnalysis_Click(object sender, EventArgs e)
        {
            objClassAnalysis.Continue(ref ClientHeterodyn, ref ClientServer, objParam);
        }

        private void ButSaveDataAnalysis_Click(object sender, EventArgs e)
        {
            if (dataAnalysisOLV.Save(Convert.ToInt16(NumBoxAnalysisAzimut.Value)))
                ShowMessage(" Сохранение прошло успешно", (byte)CorrectorAnalysisTypes.TypeMess.Attention);
            else
                ShowMessage(" При сохранениии произошли ошибки", (byte)CorrectorAnalysisTypes.TypeMess.Attention);
        }


        private void CheckShowDataExchange_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckShowDataExchange.Checked)
            {
                AnalysisAutoCollect.evenStoryWork += ShowMessage;
            }
            else
            {
                AnalysisAutoCollect.evenStoryWork -= ShowMessage;
            }
        }

        private void CheckBoxProgressAnalysis_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBoxProgressAnalysis.Checked)
            {
                AnalysisDataOLV.evenChangeCurrentRow += AnalysisDataOLV_evenChangeCurrentRow;
            }
            else 
            {
                AnalysisDataOLV.evenChangeCurrentRow -= AnalysisDataOLV_evenChangeCurrentRow;
            }
        }

        void AnalysisDataOLV_evenChangeCurrentRow(int CurrentRow)
        {
            if (ProgressAnalysis.InvokeRequired)
            {
                ProgressAnalysis.Invoke((MethodInvoker)(() =>
                {
                    ProgressAnalysis.Maximum = dataAnalysisOLV.CountRow;
                    ProgressAnalysis.Value = CurrentRow;
                }));
            }
            else
            {
                ProgressAnalysis.Maximum = dataAnalysisOLV.CountRow;
                ProgressAnalysis.Value = CurrentRow;
            }
        }
    }
}
