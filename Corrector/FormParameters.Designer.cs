﻿namespace Corrector
{
    partial class FormParameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormParameters));
            this.CriterExcludFreqGroupBox = new System.Windows.Forms.GroupBox();
            this.SignalToNoisBox = new System.Windows.Forms.NumericUpDown();
            this.DispersionPhaseBox = new System.Windows.Forms.NumericUpDown();
            this.thresholdSignalNoiseLab = new System.Windows.Forms.Label();
            this.ThreadshDispPhaseLab = new System.Windows.Forms.Label();
            this.NumAveragePhaseBox = new System.Windows.Forms.NumericUpDown();
            this.NumAverPhaseLab = new System.Windows.Forms.Label();
            this.NumAveragePelenBox = new System.Windows.Forms.NumericUpDown();
            this.NumAverBearingLab = new System.Windows.Forms.Label();
            this.ParamPelengatorGroupBox = new System.Windows.Forms.GroupBox();
            this.RelativeBearingBox = new System.Windows.Forms.NumericUpDown();
            this.RelativeBearingLab = new System.Windows.Forms.Label();
            this.IncludeShiftBox = new System.Windows.Forms.CheckBox();
            this.CgReadyLab = new System.Windows.Forms.Label();
            this.TimeDelayHeterBox = new System.Windows.Forms.NumericUpDown();
            this.groupBoxAnalisis = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BadResultBox = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.NormalResultStopBox = new System.Windows.Forms.NumericUpDown();
            this.NormalResultStartBox = new System.Windows.Forms.NumericUpDown();
            this.GreatResultBox = new System.Windows.Forms.NumericUpDown();
            this.PicBoxRed = new System.Windows.Forms.PictureBox();
            this.PicBoxYellow = new System.Windows.Forms.PictureBox();
            this.PicBoxGreen = new System.Windows.Forms.PictureBox();
            this.tabContrSettings = new System.Windows.Forms.TabControl();
            this.tabCommon = new System.Windows.Forms.TabPage();
            this.GcGroupBox = new System.Windows.Forms.GroupBox();
            this.MaxNumSendLab = new System.Windows.Forms.Label();
            this.MaxCountSendBox = new System.Windows.Forms.NumericUpDown();
            this.RadDurationLab = new System.Windows.Forms.Label();
            this.DurationRadiationBox = new System.Windows.Forms.NumericUpDown();
            this.tabCalib = new System.Windows.Forms.TabPage();
            this.groupBoxCalib = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tabAnalysis = new System.Windows.Forms.TabPage();
            this.CriterExcludFreqGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SignalToNoisBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DispersionPhaseBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAveragePhaseBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAveragePelenBox)).BeginInit();
            this.ParamPelengatorGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RelativeBearingBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeDelayHeterBox)).BeginInit();
            this.groupBoxAnalisis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BadResultBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NormalResultStopBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NormalResultStartBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GreatResultBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxRed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxYellow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxGreen)).BeginInit();
            this.tabContrSettings.SuspendLayout();
            this.tabCommon.SuspendLayout();
            this.GcGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaxCountSendBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurationRadiationBox)).BeginInit();
            this.tabCalib.SuspendLayout();
            this.groupBoxCalib.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tabAnalysis.SuspendLayout();
            this.SuspendLayout();
            // 
            // CriterExcludFreqGroupBox
            // 
            this.CriterExcludFreqGroupBox.Controls.Add(this.SignalToNoisBox);
            this.CriterExcludFreqGroupBox.Controls.Add(this.DispersionPhaseBox);
            this.CriterExcludFreqGroupBox.Controls.Add(this.thresholdSignalNoiseLab);
            this.CriterExcludFreqGroupBox.Controls.Add(this.ThreadshDispPhaseLab);
            this.CriterExcludFreqGroupBox.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CriterExcludFreqGroupBox.ForeColor = System.Drawing.Color.MidnightBlue;
            this.CriterExcludFreqGroupBox.Location = new System.Drawing.Point(6, 15);
            this.CriterExcludFreqGroupBox.Name = "CriterExcludFreqGroupBox";
            this.CriterExcludFreqGroupBox.Size = new System.Drawing.Size(269, 86);
            this.CriterExcludFreqGroupBox.TabIndex = 40;
            this.CriterExcludFreqGroupBox.TabStop = false;
            this.CriterExcludFreqGroupBox.Text = "Критерии исключения частоты";
            // 
            // SignalToNoisBox
            // 
            this.SignalToNoisBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SignalToNoisBox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SignalToNoisBox.Location = new System.Drawing.Point(210, 17);
            this.SignalToNoisBox.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.SignalToNoisBox.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.SignalToNoisBox.Name = "SignalToNoisBox";
            this.SignalToNoisBox.Size = new System.Drawing.Size(56, 23);
            this.SignalToNoisBox.TabIndex = 36;
            this.SignalToNoisBox.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.SignalToNoisBox.ValueChanged += new System.EventHandler(this.ChangeValueParams);
            this.SignalToNoisBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpParams);
            // 
            // DispersionPhaseBox
            // 
            this.DispersionPhaseBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DispersionPhaseBox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DispersionPhaseBox.Location = new System.Drawing.Point(210, 51);
            this.DispersionPhaseBox.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.DispersionPhaseBox.Name = "DispersionPhaseBox";
            this.DispersionPhaseBox.Size = new System.Drawing.Size(56, 23);
            this.DispersionPhaseBox.TabIndex = 35;
            this.DispersionPhaseBox.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.DispersionPhaseBox.ValueChanged += new System.EventHandler(this.ChangeValueParams);
            this.DispersionPhaseBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpParams);
            // 
            // thresholdSignalNoiseLab
            // 
            this.thresholdSignalNoiseLab.AutoSize = true;
            this.thresholdSignalNoiseLab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.thresholdSignalNoiseLab.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.thresholdSignalNoiseLab.ForeColor = System.Drawing.Color.Black;
            this.thresholdSignalNoiseLab.Location = new System.Drawing.Point(6, 18);
            this.thresholdSignalNoiseLab.Name = "thresholdSignalNoiseLab";
            this.thresholdSignalNoiseLab.Size = new System.Drawing.Size(181, 16);
            this.thresholdSignalNoiseLab.TabIndex = 33;
            this.thresholdSignalNoiseLab.Text = "Порог Сигнал/Шум КГ, дБ";
            // 
            // ThreadshDispPhaseLab
            // 
            this.ThreadshDispPhaseLab.AutoSize = true;
            this.ThreadshDispPhaseLab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ThreadshDispPhaseLab.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ThreadshDispPhaseLab.ForeColor = System.Drawing.Color.Black;
            this.ThreadshDispPhaseLab.Location = new System.Drawing.Point(5, 52);
            this.ThreadshDispPhaseLab.Name = "ThreadshDispPhaseLab";
            this.ThreadshDispPhaseLab.Size = new System.Drawing.Size(155, 16);
            this.ThreadshDispPhaseLab.TabIndex = 34;
            this.ThreadshDispPhaseLab.Text = "Порог дисперсии фаз";
            // 
            // NumAveragePhaseBox
            // 
            this.NumAveragePhaseBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumAveragePhaseBox.Location = new System.Drawing.Point(206, 17);
            this.NumAveragePhaseBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumAveragePhaseBox.Name = "NumAveragePhaseBox";
            this.NumAveragePhaseBox.Size = new System.Drawing.Size(56, 22);
            this.NumAveragePhaseBox.TabIndex = 38;
            this.NumAveragePhaseBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NumAveragePhaseBox.ValueChanged += new System.EventHandler(this.ChangeValueParams);
            this.NumAveragePhaseBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpParams);
            // 
            // NumAverPhaseLab
            // 
            this.NumAverPhaseLab.AutoSize = true;
            this.NumAverPhaseLab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NumAverPhaseLab.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumAverPhaseLab.ForeColor = System.Drawing.Color.Black;
            this.NumAverPhaseLab.Location = new System.Drawing.Point(5, 19);
            this.NumAverPhaseLab.Name = "NumAverPhaseLab";
            this.NumAverPhaseLab.Size = new System.Drawing.Size(120, 16);
            this.NumAverPhaseLab.TabIndex = 31;
            this.NumAverPhaseLab.Text = "Кол-во уср. фаз";
            // 
            // NumAveragePelenBox
            // 
            this.NumAveragePelenBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumAveragePelenBox.Location = new System.Drawing.Point(206, 55);
            this.NumAveragePelenBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumAveragePelenBox.Name = "NumAveragePelenBox";
            this.NumAveragePelenBox.Size = new System.Drawing.Size(56, 22);
            this.NumAveragePelenBox.TabIndex = 37;
            this.NumAveragePelenBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NumAveragePelenBox.ValueChanged += new System.EventHandler(this.ChangeValueParams);
            this.NumAveragePelenBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpParams);
            // 
            // NumAverBearingLab
            // 
            this.NumAverBearingLab.AutoSize = true;
            this.NumAverBearingLab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NumAverBearingLab.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumAverBearingLab.ForeColor = System.Drawing.Color.Black;
            this.NumAverBearingLab.Location = new System.Drawing.Point(5, 55);
            this.NumAverBearingLab.Name = "NumAverBearingLab";
            this.NumAverBearingLab.Size = new System.Drawing.Size(156, 16);
            this.NumAverBearingLab.TabIndex = 32;
            this.NumAverBearingLab.Text = "Кол-во уср. пеленгов";
            // 
            // ParamPelengatorGroupBox
            // 
            this.ParamPelengatorGroupBox.Controls.Add(this.RelativeBearingBox);
            this.ParamPelengatorGroupBox.Controls.Add(this.RelativeBearingLab);
            this.ParamPelengatorGroupBox.Controls.Add(this.NumAveragePhaseBox);
            this.ParamPelengatorGroupBox.Controls.Add(this.NumAverPhaseLab);
            this.ParamPelengatorGroupBox.Controls.Add(this.NumAveragePelenBox);
            this.ParamPelengatorGroupBox.Controls.Add(this.NumAverBearingLab);
            this.ParamPelengatorGroupBox.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ParamPelengatorGroupBox.ForeColor = System.Drawing.Color.MidnightBlue;
            this.ParamPelengatorGroupBox.Location = new System.Drawing.Point(5, 6);
            this.ParamPelengatorGroupBox.Name = "ParamPelengatorGroupBox";
            this.ParamPelengatorGroupBox.Size = new System.Drawing.Size(269, 125);
            this.ParamPelengatorGroupBox.TabIndex = 41;
            this.ParamPelengatorGroupBox.TabStop = false;
            this.ParamPelengatorGroupBox.Text = "Параметры пеленгатора";
            // 
            // RelativeBearingBox
            // 
            this.RelativeBearingBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RelativeBearingBox.Location = new System.Drawing.Point(206, 92);
            this.RelativeBearingBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.RelativeBearingBox.Name = "RelativeBearingBox";
            this.RelativeBearingBox.Size = new System.Drawing.Size(56, 22);
            this.RelativeBearingBox.TabIndex = 40;
            this.RelativeBearingBox.ValueChanged += new System.EventHandler(this.ChangeValueParams);
            this.RelativeBearingBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpParams);
            // 
            // RelativeBearingLab
            // 
            this.RelativeBearingLab.AutoSize = true;
            this.RelativeBearingLab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RelativeBearingLab.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RelativeBearingLab.ForeColor = System.Drawing.Color.Black;
            this.RelativeBearingLab.Location = new System.Drawing.Point(5, 93);
            this.RelativeBearingLab.Name = "RelativeBearingLab";
            this.RelativeBearingLab.Size = new System.Drawing.Size(107, 16);
            this.RelativeBearingLab.TabIndex = 39;
            this.RelativeBearingLab.Text = "Курсовой угол";
            // 
            // IncludeShiftBox
            // 
            this.IncludeShiftBox.AutoSize = true;
            this.IncludeShiftBox.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.IncludeShiftBox.ForeColor = System.Drawing.Color.Black;
            this.IncludeShiftBox.Location = new System.Drawing.Point(36, 116);
            this.IncludeShiftBox.Name = "IncludeShiftBox";
            this.IncludeShiftBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.IncludeShiftBox.Size = new System.Drawing.Size(183, 20);
            this.IncludeShiftBox.TabIndex = 0;
            this.IncludeShiftBox.Text = "Добавление смещения";
            this.IncludeShiftBox.UseVisualStyleBackColor = true;
            this.IncludeShiftBox.CheckedChanged += new System.EventHandler(this.ChangeValueParams);
            // 
            // CgReadyLab
            // 
            this.CgReadyLab.AutoSize = true;
            this.CgReadyLab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CgReadyLab.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CgReadyLab.ForeColor = System.Drawing.Color.Black;
            this.CgReadyLab.Location = new System.Drawing.Point(5, 24);
            this.CgReadyLab.Name = "CgReadyLab";
            this.CgReadyLab.Size = new System.Drawing.Size(131, 16);
            this.CgReadyLab.TabIndex = 37;
            this.CgReadyLab.Text = "Готовность КГ, мс";
            // 
            // TimeDelayHeterBox
            // 
            this.TimeDelayHeterBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TimeDelayHeterBox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TimeDelayHeterBox.Location = new System.Drawing.Point(206, 22);
            this.TimeDelayHeterBox.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.TimeDelayHeterBox.Name = "TimeDelayHeterBox";
            this.TimeDelayHeterBox.Size = new System.Drawing.Size(56, 23);
            this.TimeDelayHeterBox.TabIndex = 36;
            this.TimeDelayHeterBox.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.TimeDelayHeterBox.ValueChanged += new System.EventHandler(this.ChangeValueParams);
            this.TimeDelayHeterBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpParams);
            // 
            // groupBoxAnalisis
            // 
            this.groupBoxAnalisis.Controls.Add(this.label12);
            this.groupBoxAnalisis.Controls.Add(this.label11);
            this.groupBoxAnalisis.Controls.Add(this.label10);
            this.groupBoxAnalisis.Controls.Add(this.label2);
            this.groupBoxAnalisis.Controls.Add(this.BadResultBox);
            this.groupBoxAnalisis.Controls.Add(this.label3);
            this.groupBoxAnalisis.Controls.Add(this.NormalResultStopBox);
            this.groupBoxAnalisis.Controls.Add(this.NormalResultStartBox);
            this.groupBoxAnalisis.Controls.Add(this.GreatResultBox);
            this.groupBoxAnalisis.Controls.Add(this.PicBoxRed);
            this.groupBoxAnalisis.Controls.Add(this.PicBoxYellow);
            this.groupBoxAnalisis.Controls.Add(this.PicBoxGreen);
            this.groupBoxAnalisis.Font = new System.Drawing.Font("Verdana", 9F);
            this.groupBoxAnalisis.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxAnalisis.Location = new System.Drawing.Point(6, 6);
            this.groupBoxAnalisis.Name = "groupBoxAnalisis";
            this.groupBoxAnalisis.Size = new System.Drawing.Size(239, 111);
            this.groupBoxAnalisis.TabIndex = 44;
            this.groupBoxAnalisis.TabStop = false;
            this.groupBoxAnalisis.Text = "Градация Результатов Анализа";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label12.Location = new System.Drawing.Point(153, 85);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 18);
            this.label12.TabIndex = 25;
            this.label12.Text = "359°";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label11.Location = new System.Drawing.Point(74, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 18);
            this.label11.TabIndex = 24;
            this.label11.Text = "0°";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label10.Location = new System.Drawing.Point(116, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 18);
            this.label10.TabIndex = 23;
            this.label10.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(116, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 18);
            this.label2.TabIndex = 22;
            this.label2.Text = "-";
            // 
            // BadResultBox
            // 
            this.BadResultBox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BadResultBox.Location = new System.Drawing.Point(60, 83);
            this.BadResultBox.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.BadResultBox.Name = "BadResultBox";
            this.BadResultBox.Size = new System.Drawing.Size(43, 23);
            this.BadResultBox.TabIndex = 21;
            this.BadResultBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.BadResultBox.ValueChanged += new System.EventHandler(this.ChangeValueParams);
            this.BadResultBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpParams);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label3.Location = new System.Drawing.Point(116, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 18);
            this.label3.TabIndex = 19;
            this.label3.Text = "-";
            // 
            // NormalResultStopBox
            // 
            this.NormalResultStopBox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NormalResultStopBox.Location = new System.Drawing.Point(153, 52);
            this.NormalResultStopBox.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NormalResultStopBox.Name = "NormalResultStopBox";
            this.NormalResultStopBox.Size = new System.Drawing.Size(43, 23);
            this.NormalResultStopBox.TabIndex = 18;
            this.NormalResultStopBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NormalResultStopBox.ValueChanged += new System.EventHandler(this.ChangeValueParams);
            this.NormalResultStopBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpParams);
            // 
            // NormalResultStartBox
            // 
            this.NormalResultStartBox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NormalResultStartBox.Location = new System.Drawing.Point(60, 52);
            this.NormalResultStartBox.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NormalResultStartBox.Name = "NormalResultStartBox";
            this.NormalResultStartBox.Size = new System.Drawing.Size(43, 23);
            this.NormalResultStartBox.TabIndex = 17;
            this.NormalResultStartBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.NormalResultStartBox.ValueChanged += new System.EventHandler(this.ChangeValueParams);
            this.NormalResultStartBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpParams);
            // 
            // GreatResultBox
            // 
            this.GreatResultBox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GreatResultBox.Location = new System.Drawing.Point(153, 22);
            this.GreatResultBox.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.GreatResultBox.Name = "GreatResultBox";
            this.GreatResultBox.Size = new System.Drawing.Size(43, 23);
            this.GreatResultBox.TabIndex = 15;
            this.GreatResultBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.GreatResultBox.ValueChanged += new System.EventHandler(this.ChangeValueParams);
            this.GreatResultBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpParams);
            // 
            // PicBoxRed
            // 
            this.PicBoxRed.Image = global::Corrector.Properties.Resources.red;
            this.PicBoxRed.Location = new System.Drawing.Point(8, 81);
            this.PicBoxRed.Name = "PicBoxRed";
            this.PicBoxRed.Size = new System.Drawing.Size(22, 22);
            this.PicBoxRed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBoxRed.TabIndex = 14;
            this.PicBoxRed.TabStop = false;
            // 
            // PicBoxYellow
            // 
            this.PicBoxYellow.Image = global::Corrector.Properties.Resources.yellow;
            this.PicBoxYellow.Location = new System.Drawing.Point(8, 52);
            this.PicBoxYellow.Name = "PicBoxYellow";
            this.PicBoxYellow.Size = new System.Drawing.Size(22, 22);
            this.PicBoxYellow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBoxYellow.TabIndex = 13;
            this.PicBoxYellow.TabStop = false;
            // 
            // PicBoxGreen
            // 
            this.PicBoxGreen.Image = global::Corrector.Properties.Resources.green;
            this.PicBoxGreen.Location = new System.Drawing.Point(8, 23);
            this.PicBoxGreen.Name = "PicBoxGreen";
            this.PicBoxGreen.Size = new System.Drawing.Size(22, 22);
            this.PicBoxGreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBoxGreen.TabIndex = 12;
            this.PicBoxGreen.TabStop = false;
            // 
            // tabContrSettings
            // 
            this.tabContrSettings.Controls.Add(this.tabCommon);
            this.tabContrSettings.Controls.Add(this.tabCalib);
            this.tabContrSettings.Controls.Add(this.tabAnalysis);
            this.tabContrSettings.Location = new System.Drawing.Point(3, 2);
            this.tabContrSettings.Name = "tabContrSettings";
            this.tabContrSettings.SelectedIndex = 0;
            this.tabContrSettings.Size = new System.Drawing.Size(289, 307);
            this.tabContrSettings.TabIndex = 45;
            // 
            // tabCommon
            // 
            this.tabCommon.BackColor = System.Drawing.SystemColors.Control;
            this.tabCommon.Controls.Add(this.GcGroupBox);
            this.tabCommon.Controls.Add(this.ParamPelengatorGroupBox);
            this.tabCommon.Location = new System.Drawing.Point(4, 22);
            this.tabCommon.Name = "tabCommon";
            this.tabCommon.Padding = new System.Windows.Forms.Padding(3);
            this.tabCommon.Size = new System.Drawing.Size(281, 281);
            this.tabCommon.TabIndex = 0;
            this.tabCommon.Text = "Общее";
            // 
            // GcGroupBox
            // 
            this.GcGroupBox.Controls.Add(this.CgReadyLab);
            this.GcGroupBox.Controls.Add(this.MaxNumSendLab);
            this.GcGroupBox.Controls.Add(this.TimeDelayHeterBox);
            this.GcGroupBox.Controls.Add(this.MaxCountSendBox);
            this.GcGroupBox.Controls.Add(this.IncludeShiftBox);
            this.GcGroupBox.Controls.Add(this.RadDurationLab);
            this.GcGroupBox.Controls.Add(this.DurationRadiationBox);
            this.GcGroupBox.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GcGroupBox.ForeColor = System.Drawing.Color.MidnightBlue;
            this.GcGroupBox.Location = new System.Drawing.Point(6, 137);
            this.GcGroupBox.Name = "GcGroupBox";
            this.GcGroupBox.Size = new System.Drawing.Size(269, 142);
            this.GcGroupBox.TabIndex = 44;
            this.GcGroupBox.TabStop = false;
            this.GcGroupBox.Text = "Контрольный гетеродин:";
            // 
            // MaxNumSendLab
            // 
            this.MaxNumSendLab.AutoSize = true;
            this.MaxNumSendLab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MaxNumSendLab.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaxNumSendLab.ForeColor = System.Drawing.Color.Black;
            this.MaxNumSendLab.Location = new System.Drawing.Point(6, 82);
            this.MaxNumSendLab.Name = "MaxNumSendLab";
            this.MaxNumSendLab.Size = new System.Drawing.Size(154, 16);
            this.MaxNumSendLab.TabIndex = 39;
            this.MaxNumSendLab.Text = "Max кол-во отправок";
            // 
            // MaxCountSendBox
            // 
            this.MaxCountSendBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MaxCountSendBox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaxCountSendBox.Location = new System.Drawing.Point(206, 80);
            this.MaxCountSendBox.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.MaxCountSendBox.Name = "MaxCountSendBox";
            this.MaxCountSendBox.Size = new System.Drawing.Size(56, 23);
            this.MaxCountSendBox.TabIndex = 38;
            this.MaxCountSendBox.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.MaxCountSendBox.ValueChanged += new System.EventHandler(this.ChangeValueParams);
            this.MaxCountSendBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpParams);
            // 
            // RadDurationLab
            // 
            this.RadDurationLab.AutoSize = true;
            this.RadDurationLab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RadDurationLab.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RadDurationLab.ForeColor = System.Drawing.Color.Black;
            this.RadDurationLab.Location = new System.Drawing.Point(6, 53);
            this.RadDurationLab.Name = "RadDurationLab";
            this.RadDurationLab.Size = new System.Drawing.Size(196, 16);
            this.RadDurationLab.TabIndex = 37;
            this.RadDurationLab.Text = "Длительность излучения, с";
            // 
            // DurationRadiationBox
            // 
            this.DurationRadiationBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DurationRadiationBox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DurationRadiationBox.Location = new System.Drawing.Point(206, 51);
            this.DurationRadiationBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.DurationRadiationBox.Name = "DurationRadiationBox";
            this.DurationRadiationBox.Size = new System.Drawing.Size(56, 23);
            this.DurationRadiationBox.TabIndex = 36;
            this.DurationRadiationBox.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.DurationRadiationBox.ValueChanged += new System.EventHandler(this.ChangeValueParams);
            this.DurationRadiationBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpParams);
            // 
            // tabCalib
            // 
            this.tabCalib.BackColor = System.Drawing.SystemColors.Control;
            this.tabCalib.Controls.Add(this.groupBoxCalib);
            this.tabCalib.Controls.Add(this.CriterExcludFreqGroupBox);
            this.tabCalib.Location = new System.Drawing.Point(4, 22);
            this.tabCalib.Name = "tabCalib";
            this.tabCalib.Padding = new System.Windows.Forms.Padding(3);
            this.tabCalib.Size = new System.Drawing.Size(281, 281);
            this.tabCalib.TabIndex = 2;
            this.tabCalib.Text = "Калибровка";
            // 
            // groupBoxCalib
            // 
            this.groupBoxCalib.Controls.Add(this.label7);
            this.groupBoxCalib.Controls.Add(this.label4);
            this.groupBoxCalib.Controls.Add(this.label5);
            this.groupBoxCalib.Controls.Add(this.pictureBox1);
            this.groupBoxCalib.Controls.Add(this.pictureBox2);
            this.groupBoxCalib.Controls.Add(this.pictureBox3);
            this.groupBoxCalib.Font = new System.Drawing.Font("Verdana", 9F);
            this.groupBoxCalib.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxCalib.Location = new System.Drawing.Point(6, 107);
            this.groupBoxCalib.Name = "groupBoxCalib";
            this.groupBoxCalib.Size = new System.Drawing.Size(247, 121);
            this.groupBoxCalib.TabIndex = 45;
            this.groupBoxCalib.TabStop = false;
            this.groupBoxCalib.Text = "Градация результатов Калибровки";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label7.Location = new System.Drawing.Point(74, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 18);
            this.label7.TabIndex = 26;
            this.label7.Text = "10% - 50%";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label4.Location = new System.Drawing.Point(89, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 18);
            this.label4.TabIndex = 25;
            this.label4.Text = "> 50%";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label5.Location = new System.Drawing.Point(89, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 18);
            this.label5.TabIndex = 24;
            this.label5.Text = "< 10%";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Corrector.Properties.Resources.red;
            this.pictureBox1.Location = new System.Drawing.Point(8, 92);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(22, 22);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Corrector.Properties.Resources.yellow;
            this.pictureBox2.Location = new System.Drawing.Point(8, 63);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(22, 22);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Corrector.Properties.Resources.green;
            this.pictureBox3.Location = new System.Drawing.Point(8, 34);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(22, 22);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 12;
            this.pictureBox3.TabStop = false;
            // 
            // tabAnalysis
            // 
            this.tabAnalysis.BackColor = System.Drawing.SystemColors.Control;
            this.tabAnalysis.Controls.Add(this.groupBoxAnalisis);
            this.tabAnalysis.Location = new System.Drawing.Point(4, 22);
            this.tabAnalysis.Name = "tabAnalysis";
            this.tabAnalysis.Padding = new System.Windows.Forms.Padding(3);
            this.tabAnalysis.Size = new System.Drawing.Size(281, 281);
            this.tabAnalysis.TabIndex = 1;
            this.tabAnalysis.Text = "Анализ";
            // 
            // FormParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 310);
            this.Controls.Add(this.tabContrSettings);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormParameters";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormParameters_FormClosing);
            this.Load += new System.EventHandler(this.FormParameters_Load);
            this.VisibleChanged += new System.EventHandler(this.FormParameters_VisibleChanged);
            this.CriterExcludFreqGroupBox.ResumeLayout(false);
            this.CriterExcludFreqGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SignalToNoisBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DispersionPhaseBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAveragePhaseBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAveragePelenBox)).EndInit();
            this.ParamPelengatorGroupBox.ResumeLayout(false);
            this.ParamPelengatorGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RelativeBearingBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeDelayHeterBox)).EndInit();
            this.groupBoxAnalisis.ResumeLayout(false);
            this.groupBoxAnalisis.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BadResultBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NormalResultStopBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NormalResultStartBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GreatResultBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxRed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxYellow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxGreen)).EndInit();
            this.tabContrSettings.ResumeLayout(false);
            this.tabCommon.ResumeLayout(false);
            this.GcGroupBox.ResumeLayout(false);
            this.GcGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaxCountSendBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurationRadiationBox)).EndInit();
            this.tabCalib.ResumeLayout(false);
            this.groupBoxCalib.ResumeLayout(false);
            this.groupBoxCalib.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tabAnalysis.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox CriterExcludFreqGroupBox;
        private System.Windows.Forms.NumericUpDown SignalToNoisBox;
        private System.Windows.Forms.NumericUpDown DispersionPhaseBox;
        private System.Windows.Forms.Label thresholdSignalNoiseLab;
        private System.Windows.Forms.Label ThreadshDispPhaseLab;
        private System.Windows.Forms.NumericUpDown NumAveragePhaseBox;
        private System.Windows.Forms.Label NumAverPhaseLab;
        private System.Windows.Forms.NumericUpDown NumAveragePelenBox;
        private System.Windows.Forms.Label NumAverBearingLab;
        private System.Windows.Forms.GroupBox ParamPelengatorGroupBox;
        private System.Windows.Forms.CheckBox IncludeShiftBox;
        private System.Windows.Forms.Label CgReadyLab;
        private System.Windows.Forms.NumericUpDown TimeDelayHeterBox;
        private System.Windows.Forms.GroupBox groupBoxAnalisis;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown BadResultBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown NormalResultStopBox;
        private System.Windows.Forms.NumericUpDown NormalResultStartBox;
        private System.Windows.Forms.NumericUpDown GreatResultBox;
        private System.Windows.Forms.PictureBox PicBoxRed;
        private System.Windows.Forms.PictureBox PicBoxYellow;
        private System.Windows.Forms.PictureBox PicBoxGreen;
        private System.Windows.Forms.TabControl tabContrSettings;
        private System.Windows.Forms.TabPage tabCommon;
        private System.Windows.Forms.TabPage tabCalib;
        private System.Windows.Forms.TabPage tabAnalysis;
        private System.Windows.Forms.GroupBox groupBoxCalib;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox GcGroupBox;
        private System.Windows.Forms.Label MaxNumSendLab;
        private System.Windows.Forms.NumericUpDown MaxCountSendBox;
        private System.Windows.Forms.Label RadDurationLab;
        private System.Windows.Forms.NumericUpDown DurationRadiationBox;
        private System.Windows.Forms.NumericUpDown RelativeBearingBox;
        private System.Windows.Forms.Label RelativeBearingLab;
    }
}