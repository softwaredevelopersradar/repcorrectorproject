﻿namespace Corrector
{
    partial class FormCorrector
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCorrector));
            this.LabAzimut = new System.Windows.Forms.Label();
            this.NumBoxAzimut = new System.Windows.Forms.NumericUpDown();
            this.RadButFileFreq = new System.Windows.Forms.RadioButton();
            this.RadButFreq = new System.Windows.Forms.RadioButton();
            this.LabEPOStart = new System.Windows.Forms.Label();
            this.GroupBoxFileFreq = new System.Windows.Forms.GroupBox();
            this.NumBoxStepFileFreq = new System.Windows.Forms.ComboBox();
            this.LabRangeStop = new System.Windows.Forms.Label();
            this.LabRangeStart = new System.Windows.Forms.Label();
            this.LabStepFileFreq = new System.Windows.Forms.Label();
            this.NumBoxEPOStop = new System.Windows.Forms.NumericUpDown();
            this.LabEPOStop = new System.Windows.Forms.Label();
            this.NumBoxEPOStart = new System.Windows.Forms.NumericUpDown();
            this.GroupBoxFreq = new System.Windows.Forms.GroupBox();
            this.NumBoxStepFreq = new System.Windows.Forms.NumericUpDown();
            this.LabStepFreq = new System.Windows.Forms.Label();
            this.NumBoxFreqStop = new System.Windows.Forms.NumericUpDown();
            this.LabFreqStop = new System.Windows.Forms.Label();
            this.NumBoxFreqStart = new System.Windows.Forms.NumericUpDown();
            this.LabFreqStart = new System.Windows.Forms.Label();
            this.PanelTypeReq = new System.Windows.Forms.Panel();
            this.ProgressWorkCalibrarion = new System.Windows.Forms.ProgressBar();
            this.ChekBoxProgress = new System.Windows.Forms.CheckBox();
            this.CheckShowRichBoxHex = new System.Windows.Forms.CheckBox();
            this.GroupBoxView = new System.Windows.Forms.GroupBox();
            this.GroupBoxStatistics = new System.Windows.Forms.GroupBox();
            this.LabCountBadFreq = new System.Windows.Forms.Label();
            this.LabSumTime = new System.Windows.Forms.Label();
            this.LabNumRestart = new System.Windows.Forms.Label();
            this.RichBoxHex = new System.Windows.Forms.RichTextBox();
            this.ButClear = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TPageCalib = new System.Windows.Forms.TabPage();
            this.tStripWorkButton = new System.Windows.Forms.ToolStrip();
            this.ButAddCalibrarionRange = new System.Windows.Forms.ToolStripButton();
            this.ButDeleteCalibrarionRange = new System.Windows.Forms.ToolStripButton();
            this.ButStartCalibrarion = new System.Windows.Forms.ToolStripButton();
            this.ButStopCalibrarion = new System.Windows.Forms.ToolStripButton();
            this.ButPauseCalibrarion = new System.Windows.Forms.ToolStripButton();
            this.ButContinueCalibrarion = new System.Windows.Forms.ToolStripButton();
            this.ButSendData = new System.Windows.Forms.ToolStripButton();
            this.ButOnHeter = new System.Windows.Forms.ToolStripButton();
            this.ButOffHeter = new System.Windows.Forms.ToolStripButton();
            this.ObjListViewCalibrarion = new BrightIdeasSoftware.ObjectListView();
            this.ColmChoice = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColmRange = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColmStep = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColmResult = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.RendererResult = new BrightIdeasSoftware.MultiImageRenderer();
            this.ImagListResult = new System.Windows.Forms.ImageList(this.components);
            this.ColmTime = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.TPageAnalysis = new System.Windows.Forms.TabPage();
            this.GroupBoxStatisticsAnalys = new System.Windows.Forms.GroupBox();
            this.labStd = new System.Windows.Forms.Label();
            this.ProgressAnalysis = new System.Windows.Forms.ProgressBar();
            this.groupBoxStateAnalysResult = new System.Windows.Forms.GroupBox();
            this.CheckBoxProgressAnalysis = new System.Windows.Forms.CheckBox();
            this.CheckShowDataExchange = new System.Windows.Forms.CheckBox();
            this.TStripAnalysis = new System.Windows.Forms.ToolStrip();
            this.ButAddAnalysisRange = new System.Windows.Forms.ToolStripButton();
            this.ButClearAnalysis = new System.Windows.Forms.ToolStripButton();
            this.ButDeleteAnalysisRange = new System.Windows.Forms.ToolStripButton();
            this.ButStartAnalysis = new System.Windows.Forms.ToolStripButton();
            this.ButStopAnalysis = new System.Windows.Forms.ToolStripButton();
            this.ButPauseAnalysis = new System.Windows.Forms.ToolStripButton();
            this.ButContinueAnalysis = new System.Windows.Forms.ToolStripButton();
            this.ButSaveDataAnalysis = new System.Windows.Forms.ToolStripButton();
            this.LabAzimutAnalys = new System.Windows.Forms.Label();
            this.NumBoxAnalysisAzimut = new System.Windows.Forms.NumericUpDown();
            this.PanelTypeReqAnalys = new System.Windows.Forms.Panel();
            this.RadButAnalysisFreq = new System.Windows.Forms.RadioButton();
            this.RadButAnalysisFileFreq = new System.Windows.Forms.RadioButton();
            this.ObjListViewAnalysis = new BrightIdeasSoftware.ObjectListView();
            this.ColmFrequency = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColmDirection = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColmAnalysisResult = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColmDelta = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColmHiddenRange = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColmHiddenStep = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.GroupBoxFileFreqAnalysis = new System.Windows.Forms.GroupBox();
            this.LabAnalysisRangeStop = new System.Windows.Forms.Label();
            this.LabAnalysisRangeStart = new System.Windows.Forms.Label();
            this.NumBoxAnalysisEPOStep = new System.Windows.Forms.NumericUpDown();
            this.LabStepFileFreqAnalys = new System.Windows.Forms.Label();
            this.NumBoxAnalysisEPOStop = new System.Windows.Forms.NumericUpDown();
            this.LabEPOStopAnalys = new System.Windows.Forms.Label();
            this.NumBoxAnalysisEPOStart = new System.Windows.Forms.NumericUpDown();
            this.LabEPOStartAnalys = new System.Windows.Forms.Label();
            this.GroupBoxFreqAnalysis = new System.Windows.Forms.GroupBox();
            this.NumBoxAnalysisFreqStep = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.NumBoxAnalysisFreqStop = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.NumBoxAnalysisFreqStart = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.GroupBoxStateHeter = new System.Windows.Forms.GroupBox();
            this.ProgressChargeHeter = new System.Windows.Forms.ProgressBar();
            this.ButTestRadio = new System.Windows.Forms.Button();
            this.LabChargeHeter = new System.Windows.Forms.Label();
            this.LabHumidHeter = new System.Windows.Forms.Label();
            this.LabTempHeter = new System.Windows.Forms.Label();
            this.GroupBoxConnectHeter = new System.Windows.Forms.GroupBox();
            this.ButRefresh = new System.Windows.Forms.Button();
            this.ComSpeed = new System.Windows.Forms.ComboBox();
            this.ComPortName = new System.Windows.Forms.ComboBox();
            this.ButHeterConnect = new System.Windows.Forms.Button();
            this.PicBoxHeterWrite = new System.Windows.Forms.PictureBox();
            this.PicBoxHeterRead = new System.Windows.Forms.PictureBox();
            this.LabHeterConnect = new System.Windows.Forms.Label();
            this.GroupBoxConnectPL = new System.Windows.Forms.GroupBox();
            this.ButSettings = new System.Windows.Forms.Button();
            this.PicBoxPLRead = new System.Windows.Forms.PictureBox();
            this.PicBoxPLWrite = new System.Windows.Forms.PictureBox();
            this.LabPLConnect = new System.Windows.Forms.Label();
            this.ButPLConnect = new System.Windows.Forms.Button();
            this.ExchangeLogGroupBox = new System.Windows.Forms.GroupBox();
            this.ButLanguage = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAzimut)).BeginInit();
            this.GroupBoxFileFreq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxEPOStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxEPOStart)).BeginInit();
            this.GroupBoxFreq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxStepFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxFreqStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxFreqStart)).BeginInit();
            this.PanelTypeReq.SuspendLayout();
            this.GroupBoxView.SuspendLayout();
            this.GroupBoxStatistics.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TPageCalib.SuspendLayout();
            this.tStripWorkButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ObjListViewCalibrarion)).BeginInit();
            this.TPageAnalysis.SuspendLayout();
            this.GroupBoxStatisticsAnalys.SuspendLayout();
            this.groupBoxStateAnalysResult.SuspendLayout();
            this.TStripAnalysis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisAzimut)).BeginInit();
            this.PanelTypeReqAnalys.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ObjListViewAnalysis)).BeginInit();
            this.GroupBoxFileFreqAnalysis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisEPOStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisEPOStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisEPOStart)).BeginInit();
            this.GroupBoxFreqAnalysis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisFreqStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisFreqStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisFreqStart)).BeginInit();
            this.GroupBoxStateHeter.SuspendLayout();
            this.GroupBoxConnectHeter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxHeterWrite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxHeterRead)).BeginInit();
            this.GroupBoxConnectPL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxPLRead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxPLWrite)).BeginInit();
            this.ExchangeLogGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // LabAzimut
            // 
            this.LabAzimut.AutoSize = true;
            this.LabAzimut.Font = new System.Drawing.Font("Verdana", 11.25F);
            this.LabAzimut.Location = new System.Drawing.Point(18, 17);
            this.LabAzimut.Name = "LabAzimut";
            this.LabAzimut.Size = new System.Drawing.Size(79, 18);
            this.LabAzimut.TabIndex = 0;
            this.LabAzimut.Text = "Азимут, °";
            // 
            // NumBoxAzimut
            // 
            this.NumBoxAzimut.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxAzimut.Location = new System.Drawing.Point(200, 17);
            this.NumBoxAzimut.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NumBoxAzimut.Name = "NumBoxAzimut";
            this.NumBoxAzimut.Size = new System.Drawing.Size(71, 23);
            this.NumBoxAzimut.TabIndex = 1;
            // 
            // RadButFileFreq
            // 
            this.RadButFileFreq.AutoSize = true;
            this.RadButFileFreq.Checked = true;
            this.RadButFileFreq.Font = new System.Drawing.Font("Verdana", 11.25F);
            this.RadButFileFreq.Location = new System.Drawing.Point(10, 4);
            this.RadButFileFreq.Name = "RadButFileFreq";
            this.RadButFileFreq.Size = new System.Drawing.Size(126, 22);
            this.RadButFileFreq.TabIndex = 2;
            this.RadButFileFreq.TabStop = true;
            this.RadButFileFreq.Text = "Сетка частот";
            this.RadButFileFreq.UseVisualStyleBackColor = true;
            this.RadButFileFreq.CheckedChanged += new System.EventHandler(this.ChangeCheckedTypeReq);
            // 
            // RadButFreq
            // 
            this.RadButFreq.AutoSize = true;
            this.RadButFreq.Font = new System.Drawing.Font("Verdana", 11.25F);
            this.RadButFreq.Location = new System.Drawing.Point(185, 4);
            this.RadButFreq.Name = "RadButFreq";
            this.RadButFreq.Size = new System.Drawing.Size(59, 22);
            this.RadButFreq.TabIndex = 3;
            this.RadButFreq.Text = "ФРЧ";
            this.RadButFreq.UseVisualStyleBackColor = true;
            this.RadButFreq.CheckedChanged += new System.EventHandler(this.ChangeCheckedTypeReq);
            // 
            // LabEPOStart
            // 
            this.LabEPOStart.AutoSize = true;
            this.LabEPOStart.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabEPOStart.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabEPOStart.Location = new System.Drawing.Point(3, 20);
            this.LabEPOStart.Name = "LabEPOStart";
            this.LabEPOStart.Size = new System.Drawing.Size(88, 18);
            this.LabEPOStart.TabIndex = 4;
            this.LabEPOStart.Text = "ПОО старт";
            // 
            // GroupBoxFileFreq
            // 
            this.GroupBoxFileFreq.Controls.Add(this.NumBoxStepFileFreq);
            this.GroupBoxFileFreq.Controls.Add(this.LabRangeStop);
            this.GroupBoxFileFreq.Controls.Add(this.LabRangeStart);
            this.GroupBoxFileFreq.Controls.Add(this.LabStepFileFreq);
            this.GroupBoxFileFreq.Controls.Add(this.NumBoxEPOStop);
            this.GroupBoxFileFreq.Controls.Add(this.LabEPOStop);
            this.GroupBoxFileFreq.Controls.Add(this.NumBoxEPOStart);
            this.GroupBoxFileFreq.Controls.Add(this.LabEPOStart);
            this.GroupBoxFileFreq.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GroupBoxFileFreq.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.GroupBoxFileFreq.Location = new System.Drawing.Point(15, 106);
            this.GroupBoxFileFreq.Name = "GroupBoxFileFreq";
            this.GroupBoxFileFreq.Size = new System.Drawing.Size(272, 121);
            this.GroupBoxFileFreq.TabIndex = 5;
            this.GroupBoxFileFreq.TabStop = false;
            this.GroupBoxFileFreq.Text = "Сетка частот";
            // 
            // NumBoxStepFileFreq
            // 
            this.NumBoxStepFileFreq.FormattingEnabled = true;
            this.NumBoxStepFileFreq.Location = new System.Drawing.Point(194, 79);
            this.NumBoxStepFileFreq.Name = "NumBoxStepFileFreq";
            this.NumBoxStepFileFreq.Size = new System.Drawing.Size(71, 24);
            this.NumBoxStepFileFreq.TabIndex = 12;
            // 
            // LabRangeStop
            // 
            this.LabRangeStop.AutoSize = true;
            this.LabRangeStop.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabRangeStop.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabRangeStop.Location = new System.Drawing.Point(93, 55);
            this.LabRangeStop.Name = "LabRangeStop";
            this.LabRangeStop.Size = new System.Drawing.Size(95, 13);
            this.LabRangeStop.TabIndex = 11;
            this.LabRangeStop.Text = "3000-6000 МГц";
            // 
            // LabRangeStart
            // 
            this.LabRangeStart.AutoSize = true;
            this.LabRangeStart.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabRangeStart.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabRangeStart.Location = new System.Drawing.Point(93, 25);
            this.LabRangeStart.Name = "LabRangeStart";
            this.LabRangeStart.Size = new System.Drawing.Size(95, 13);
            this.LabRangeStart.TabIndex = 10;
            this.LabRangeStart.Text = "3000-6000 МГц";
            // 
            // LabStepFileFreq
            // 
            this.LabStepFileFreq.AutoSize = true;
            this.LabStepFileFreq.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabStepFileFreq.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabStepFileFreq.Location = new System.Drawing.Point(3, 83);
            this.LabStepFileFreq.Name = "LabStepFileFreq";
            this.LabStepFileFreq.Size = new System.Drawing.Size(81, 18);
            this.LabStepFileFreq.TabIndex = 8;
            this.LabStepFileFreq.Text = "Шаг, МГц";
            // 
            // NumBoxEPOStop
            // 
            this.NumBoxEPOStop.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxEPOStop.Location = new System.Drawing.Point(194, 50);
            this.NumBoxEPOStop.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NumBoxEPOStop.Name = "NumBoxEPOStop";
            this.NumBoxEPOStop.Size = new System.Drawing.Size(71, 23);
            this.NumBoxEPOStop.TabIndex = 7;
            this.NumBoxEPOStop.ValueChanged += new System.EventHandler(this.NumBoxEPOStop_ValueChanged);
            // 
            // LabEPOStop
            // 
            this.LabEPOStop.AutoSize = true;
            this.LabEPOStop.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabEPOStop.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabEPOStop.Location = new System.Drawing.Point(3, 51);
            this.LabEPOStop.Name = "LabEPOStop";
            this.LabEPOStop.Size = new System.Drawing.Size(82, 18);
            this.LabEPOStop.TabIndex = 6;
            this.LabEPOStop.Text = "ПОО стоп";
            // 
            // NumBoxEPOStart
            // 
            this.NumBoxEPOStart.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxEPOStart.Location = new System.Drawing.Point(194, 20);
            this.NumBoxEPOStart.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NumBoxEPOStart.Name = "NumBoxEPOStart";
            this.NumBoxEPOStart.Size = new System.Drawing.Size(71, 23);
            this.NumBoxEPOStart.TabIndex = 5;
            this.NumBoxEPOStart.ValueChanged += new System.EventHandler(this.NumBoxEPOStart_ValueChanged);
            // 
            // GroupBoxFreq
            // 
            this.GroupBoxFreq.Controls.Add(this.NumBoxStepFreq);
            this.GroupBoxFreq.Controls.Add(this.LabStepFreq);
            this.GroupBoxFreq.Controls.Add(this.NumBoxFreqStop);
            this.GroupBoxFreq.Controls.Add(this.LabFreqStop);
            this.GroupBoxFreq.Controls.Add(this.NumBoxFreqStart);
            this.GroupBoxFreq.Controls.Add(this.LabFreqStart);
            this.GroupBoxFreq.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GroupBoxFreq.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.GroupBoxFreq.Location = new System.Drawing.Point(16, 106);
            this.GroupBoxFreq.Name = "GroupBoxFreq";
            this.GroupBoxFreq.Size = new System.Drawing.Size(271, 120);
            this.GroupBoxFreq.TabIndex = 6;
            this.GroupBoxFreq.TabStop = false;
            this.GroupBoxFreq.Text = "ФРЧ";
            // 
            // NumBoxStepFreq
            // 
            this.NumBoxStepFreq.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxStepFreq.Location = new System.Drawing.Point(184, 80);
            this.NumBoxStepFreq.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.NumBoxStepFreq.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumBoxStepFreq.Name = "NumBoxStepFreq";
            this.NumBoxStepFreq.Size = new System.Drawing.Size(71, 23);
            this.NumBoxStepFreq.TabIndex = 9;
            this.NumBoxStepFreq.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // LabStepFreq
            // 
            this.LabStepFreq.AutoSize = true;
            this.LabStepFreq.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabStepFreq.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabStepFreq.Location = new System.Drawing.Point(6, 85);
            this.LabStepFreq.Name = "LabStepFreq";
            this.LabStepFreq.Size = new System.Drawing.Size(81, 18);
            this.LabStepFreq.TabIndex = 8;
            this.LabStepFreq.Text = "Шаг, МГц";
            // 
            // NumBoxFreqStop
            // 
            this.NumBoxFreqStop.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxFreqStop.Location = new System.Drawing.Point(184, 50);
            this.NumBoxFreqStop.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NumBoxFreqStop.Name = "NumBoxFreqStop";
            this.NumBoxFreqStop.Size = new System.Drawing.Size(71, 23);
            this.NumBoxFreqStop.TabIndex = 7;
            // 
            // LabFreqStop
            // 
            this.LabFreqStop.AutoSize = true;
            this.LabFreqStop.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabFreqStop.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabFreqStop.Location = new System.Drawing.Point(6, 55);
            this.LabFreqStop.Name = "LabFreqStop";
            this.LabFreqStop.Size = new System.Drawing.Size(147, 18);
            this.LabFreqStop.TabIndex = 6;
            this.LabFreqStop.Text = "Частота конечная";
            // 
            // NumBoxFreqStart
            // 
            this.NumBoxFreqStart.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxFreqStart.Location = new System.Drawing.Point(184, 20);
            this.NumBoxFreqStart.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NumBoxFreqStart.Name = "NumBoxFreqStart";
            this.NumBoxFreqStart.Size = new System.Drawing.Size(71, 23);
            this.NumBoxFreqStart.TabIndex = 5;
            // 
            // LabFreqStart
            // 
            this.LabFreqStart.AutoSize = true;
            this.LabFreqStart.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabFreqStart.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabFreqStart.Location = new System.Drawing.Point(6, 25);
            this.LabFreqStart.Name = "LabFreqStart";
            this.LabFreqStart.Size = new System.Drawing.Size(155, 18);
            this.LabFreqStart.TabIndex = 4;
            this.LabFreqStart.Text = "Частота начальная";
            // 
            // PanelTypeReq
            // 
            this.PanelTypeReq.Controls.Add(this.RadButFreq);
            this.PanelTypeReq.Controls.Add(this.RadButFileFreq);
            this.PanelTypeReq.Location = new System.Drawing.Point(15, 66);
            this.PanelTypeReq.Name = "PanelTypeReq";
            this.PanelTypeReq.Size = new System.Drawing.Size(272, 29);
            this.PanelTypeReq.TabIndex = 7;
            // 
            // ProgressWorkCalibrarion
            // 
            this.ProgressWorkCalibrarion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProgressWorkCalibrarion.Location = new System.Drawing.Point(304, 233);
            this.ProgressWorkCalibrarion.Name = "ProgressWorkCalibrarion";
            this.ProgressWorkCalibrarion.Size = new System.Drawing.Size(382, 27);
            this.ProgressWorkCalibrarion.TabIndex = 15;
            // 
            // ChekBoxProgress
            // 
            this.ChekBoxProgress.AutoSize = true;
            this.ChekBoxProgress.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChekBoxProgress.Location = new System.Drawing.Point(6, 12);
            this.ChekBoxProgress.Name = "ChekBoxProgress";
            this.ChekBoxProgress.Size = new System.Drawing.Size(170, 18);
            this.ChekBoxProgress.TabIndex = 16;
            this.ChekBoxProgress.Text = "Отображать прогресс";
            this.ChekBoxProgress.UseVisualStyleBackColor = true;
            this.ChekBoxProgress.CheckedChanged += new System.EventHandler(this.ChekBoxProgress_CheckedChanged);
            // 
            // CheckShowRichBoxHex
            // 
            this.CheckShowRichBoxHex.AutoSize = true;
            this.CheckShowRichBoxHex.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckShowRichBoxHex.Location = new System.Drawing.Point(6, 36);
            this.CheckShowRichBoxHex.Name = "CheckShowRichBoxHex";
            this.CheckShowRichBoxHex.Size = new System.Drawing.Size(135, 18);
            this.CheckShowRichBoxHex.TabIndex = 19;
            this.CheckShowRichBoxHex.Text = "Отображать ЖО";
            this.CheckShowRichBoxHex.UseVisualStyleBackColor = true;
            this.CheckShowRichBoxHex.CheckedChanged += new System.EventHandler(this.CheckShowRichBoxHex_CheckedChanged);
            // 
            // GroupBoxView
            // 
            this.GroupBoxView.Controls.Add(this.ChekBoxProgress);
            this.GroupBoxView.Controls.Add(this.CheckShowRichBoxHex);
            this.GroupBoxView.Location = new System.Drawing.Point(16, 279);
            this.GroupBoxView.Name = "GroupBoxView";
            this.GroupBoxView.Size = new System.Drawing.Size(189, 57);
            this.GroupBoxView.TabIndex = 20;
            this.GroupBoxView.TabStop = false;
            // 
            // GroupBoxStatistics
            // 
            this.GroupBoxStatistics.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxStatistics.Controls.Add(this.LabCountBadFreq);
            this.GroupBoxStatistics.Controls.Add(this.LabSumTime);
            this.GroupBoxStatistics.Controls.Add(this.LabNumRestart);
            this.GroupBoxStatistics.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.GroupBoxStatistics.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.GroupBoxStatistics.Location = new System.Drawing.Point(304, 266);
            this.GroupBoxStatistics.Name = "GroupBoxStatistics";
            this.GroupBoxStatistics.Size = new System.Drawing.Size(288, 129);
            this.GroupBoxStatistics.TabIndex = 21;
            this.GroupBoxStatistics.TabStop = false;
            this.GroupBoxStatistics.Text = "Статистика";
            // 
            // LabCountBadFreq
            // 
            this.LabCountBadFreq.AutoSize = true;
            this.LabCountBadFreq.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabCountBadFreq.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabCountBadFreq.Location = new System.Drawing.Point(6, 91);
            this.LabCountBadFreq.Name = "LabCountBadFreq";
            this.LabCountBadFreq.Size = new System.Drawing.Size(155, 14);
            this.LabCountBadFreq.TabIndex = 7;
            this.LabCountBadFreq.Text = "% поражённых частот";
            // 
            // LabSumTime
            // 
            this.LabSumTime.AutoSize = true;
            this.LabSumTime.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabSumTime.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabSumTime.Location = new System.Drawing.Point(6, 56);
            this.LabSumTime.Name = "LabSumTime";
            this.LabSumTime.Size = new System.Drawing.Size(121, 14);
            this.LabSumTime.TabIndex = 6;
            this.LabSumTime.Text = "Суммарное время";
            // 
            // LabNumRestart
            // 
            this.LabNumRestart.AutoSize = true;
            this.LabNumRestart.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabNumRestart.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabNumRestart.Location = new System.Drawing.Point(5, 20);
            this.LabNumRestart.Name = "LabNumRestart";
            this.LabNumRestart.Size = new System.Drawing.Size(194, 14);
            this.LabNumRestart.TabIndex = 5;
            this.LabNumRestart.Text = "Количество перезапусков КГ";
            // 
            // RichBoxHex
            // 
            this.RichBoxHex.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RichBoxHex.BackColor = System.Drawing.Color.OldLace;
            this.RichBoxHex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RichBoxHex.Location = new System.Drawing.Point(6, 22);
            this.RichBoxHex.Name = "RichBoxHex";
            this.RichBoxHex.ReadOnly = true;
            this.RichBoxHex.Size = new System.Drawing.Size(262, 351);
            this.RichBoxHex.TabIndex = 22;
            this.RichBoxHex.Text = "";
            // 
            // ButClear
            // 
            this.ButClear.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ButClear.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.ButClear.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.ButClear.Location = new System.Drawing.Point(96, 379);
            this.ButClear.Name = "ButClear";
            this.ButClear.Size = new System.Drawing.Size(85, 26);
            this.ButClear.TabIndex = 23;
            this.ButClear.Text = "Очистить";
            this.ButClear.UseVisualStyleBackColor = true;
            this.ButClear.Click += new System.EventHandler(this.ButClear_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.TPageCalib);
            this.tabControl1.Controls.Add(this.TPageAnalysis);
            this.tabControl1.Location = new System.Drawing.Point(2, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(707, 429);
            this.tabControl1.TabIndex = 25;
            // 
            // TPageCalib
            // 
            this.TPageCalib.BackColor = System.Drawing.SystemColors.Control;
            this.TPageCalib.Controls.Add(this.tStripWorkButton);
            this.TPageCalib.Controls.Add(this.ObjListViewCalibrarion);
            this.TPageCalib.Controls.Add(this.GroupBoxStatistics);
            this.TPageCalib.Controls.Add(this.LabAzimut);
            this.TPageCalib.Controls.Add(this.NumBoxAzimut);
            this.TPageCalib.Controls.Add(this.GroupBoxView);
            this.TPageCalib.Controls.Add(this.PanelTypeReq);
            this.TPageCalib.Controls.Add(this.ProgressWorkCalibrarion);
            this.TPageCalib.Controls.Add(this.GroupBoxFileFreq);
            this.TPageCalib.Controls.Add(this.GroupBoxFreq);
            this.TPageCalib.Location = new System.Drawing.Point(4, 22);
            this.TPageCalib.Name = "TPageCalib";
            this.TPageCalib.Padding = new System.Windows.Forms.Padding(3);
            this.TPageCalib.Size = new System.Drawing.Size(699, 403);
            this.TPageCalib.TabIndex = 0;
            this.TPageCalib.Text = "Калибровка";
            // 
            // tStripWorkButton
            // 
            this.tStripWorkButton.Dock = System.Windows.Forms.DockStyle.None;
            this.tStripWorkButton.ImageScalingSize = new System.Drawing.Size(22, 22);
            this.tStripWorkButton.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ButAddCalibrarionRange,
            this.ButDeleteCalibrarionRange,
            this.ButStartCalibrarion,
            this.ButStopCalibrarion,
            this.ButPauseCalibrarion,
            this.ButContinueCalibrarion,
            this.ButSendData,
            this.ButOnHeter,
            this.ButOffHeter});
            this.tStripWorkButton.Location = new System.Drawing.Point(15, 233);
            this.tStripWorkButton.Name = "tStripWorkButton";
            this.tStripWorkButton.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tStripWorkButton.Size = new System.Drawing.Size(228, 39);
            this.tStripWorkButton.TabIndex = 29;
            this.tStripWorkButton.Text = "toolStrip1";
            // 
            // ButAddCalibrarionRange
            // 
            this.ButAddCalibrarionRange.AutoSize = false;
            this.ButAddCalibrarionRange.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButAddCalibrarionRange.Image = global::Corrector.Properties.Resources.Add;
            this.ButAddCalibrarionRange.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButAddCalibrarionRange.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButAddCalibrarionRange.Name = "ButAddCalibrarionRange";
            this.ButAddCalibrarionRange.Size = new System.Drawing.Size(36, 36);
            this.ButAddCalibrarionRange.Text = "tlSButAdd";
            this.ButAddCalibrarionRange.ToolTipText = "Добавить в таблицу";
            this.ButAddCalibrarionRange.Click += new System.EventHandler(this.ButAdd_Click);
            // 
            // ButDeleteCalibrarionRange
            // 
            this.ButDeleteCalibrarionRange.AutoSize = false;
            this.ButDeleteCalibrarionRange.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButDeleteCalibrarionRange.Image = global::Corrector.Properties.Resources.Dell;
            this.ButDeleteCalibrarionRange.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButDeleteCalibrarionRange.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButDeleteCalibrarionRange.Name = "ButDeleteCalibrarionRange";
            this.ButDeleteCalibrarionRange.Size = new System.Drawing.Size(36, 36);
            this.ButDeleteCalibrarionRange.Text = "toolStripButton2";
            this.ButDeleteCalibrarionRange.ToolTipText = "Удалить из таблицы";
            this.ButDeleteCalibrarionRange.Click += new System.EventHandler(this.ButDelete_Click);
            // 
            // ButStartCalibrarion
            // 
            this.ButStartCalibrarion.AutoSize = false;
            this.ButStartCalibrarion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButStartCalibrarion.Image = global::Corrector.Properties.Resources.Start;
            this.ButStartCalibrarion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButStartCalibrarion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButStartCalibrarion.Name = "ButStartCalibrarion";
            this.ButStartCalibrarion.Size = new System.Drawing.Size(36, 36);
            this.ButStartCalibrarion.Text = "toolStripButton3";
            this.ButStartCalibrarion.ToolTipText = "Начать Опрос";
            this.ButStartCalibrarion.Click += new System.EventHandler(this.ButStart_Click);
            // 
            // ButStopCalibrarion
            // 
            this.ButStopCalibrarion.AutoSize = false;
            this.ButStopCalibrarion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButStopCalibrarion.Enabled = false;
            this.ButStopCalibrarion.Image = global::Corrector.Properties.Resources.Stop;
            this.ButStopCalibrarion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButStopCalibrarion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButStopCalibrarion.Name = "ButStopCalibrarion";
            this.ButStopCalibrarion.Size = new System.Drawing.Size(36, 36);
            this.ButStopCalibrarion.Text = "toolStripButton4";
            this.ButStopCalibrarion.ToolTipText = "Остановить Опрос";
            this.ButStopCalibrarion.Visible = false;
            this.ButStopCalibrarion.Click += new System.EventHandler(this.ButStop_Click);
            // 
            // ButPauseCalibrarion
            // 
            this.ButPauseCalibrarion.AutoSize = false;
            this.ButPauseCalibrarion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButPauseCalibrarion.Enabled = false;
            this.ButPauseCalibrarion.Image = global::Corrector.Properties.Resources.Pause;
            this.ButPauseCalibrarion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButPauseCalibrarion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButPauseCalibrarion.Name = "ButPauseCalibrarion";
            this.ButPauseCalibrarion.Size = new System.Drawing.Size(36, 36);
            this.ButPauseCalibrarion.Text = "toolStripButton5";
            this.ButPauseCalibrarion.ToolTipText = "Приостановить Опрос";
            this.ButPauseCalibrarion.Visible = false;
            this.ButPauseCalibrarion.Click += new System.EventHandler(this.ButPause_Click);
            // 
            // ButContinueCalibrarion
            // 
            this.ButContinueCalibrarion.AutoSize = false;
            this.ButContinueCalibrarion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButContinueCalibrarion.Image = global::Corrector.Properties.Resources.Continue;
            this.ButContinueCalibrarion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButContinueCalibrarion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButContinueCalibrarion.Name = "ButContinueCalibrarion";
            this.ButContinueCalibrarion.Size = new System.Drawing.Size(36, 36);
            this.ButContinueCalibrarion.Text = "toolStripButton6";
            this.ButContinueCalibrarion.ToolTipText = "Продолжить Опрос";
            this.ButContinueCalibrarion.Visible = false;
            this.ButContinueCalibrarion.Click += new System.EventHandler(this.ButContinue_Click);
            // 
            // ButSendData
            // 
            this.ButSendData.AutoSize = false;
            this.ButSendData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButSendData.Image = global::Corrector.Properties.Resources.SendData;
            this.ButSendData.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButSendData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButSendData.Name = "ButSendData";
            this.ButSendData.Size = new System.Drawing.Size(36, 36);
            this.ButSendData.Text = "toolStripButton8";
            this.ButSendData.ToolTipText = "Отправить данные на сервер";
            this.ButSendData.Click += new System.EventHandler(this.ButSendData_Click);
            // 
            // ButOnHeter
            // 
            this.ButOnHeter.AutoSize = false;
            this.ButOnHeter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButOnHeter.Image = global::Corrector.Properties.Resources.OnSignal;
            this.ButOnHeter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButOnHeter.Name = "ButOnHeter";
            this.ButOnHeter.Size = new System.Drawing.Size(36, 36);
            this.ButOnHeter.Text = "toolStripButton1";
            this.ButOnHeter.ToolTipText = "Включить излучение";
            this.ButOnHeter.Click += new System.EventHandler(this.ButOnHeter_Click);
            // 
            // ButOffHeter
            // 
            this.ButOffHeter.AutoSize = false;
            this.ButOffHeter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButOffHeter.Image = global::Corrector.Properties.Resources.OffSignal;
            this.ButOffHeter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButOffHeter.Name = "ButOffHeter";
            this.ButOffHeter.Size = new System.Drawing.Size(36, 36);
            this.ButOffHeter.Text = "toolStripButton2";
            this.ButOffHeter.ToolTipText = "Выключить излучение";
            this.ButOffHeter.Click += new System.EventHandler(this.ButOffHeter_Click);
            // 
            // ObjListViewCalibrarion
            // 
            this.ObjListViewCalibrarion.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.ObjListViewCalibrarion.AllColumns.Add(this.ColmChoice);
            this.ObjListViewCalibrarion.AllColumns.Add(this.ColmRange);
            this.ObjListViewCalibrarion.AllColumns.Add(this.ColmStep);
            this.ObjListViewCalibrarion.AllColumns.Add(this.ColmResult);
            this.ObjListViewCalibrarion.AllColumns.Add(this.ColmTime);
            this.ObjListViewCalibrarion.AllowColumnReorder = true;
            this.ObjListViewCalibrarion.AllowDrop = true;
            this.ObjListViewCalibrarion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ObjListViewCalibrarion.CellEditUseWholeCell = false;
            this.ObjListViewCalibrarion.CheckBoxes = true;
            this.ObjListViewCalibrarion.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColmChoice,
            this.ColmRange,
            this.ColmStep,
            this.ColmResult,
            this.ColmTime});
            this.ObjListViewCalibrarion.Cursor = System.Windows.Forms.Cursors.Default;
            this.ObjListViewCalibrarion.FullRowSelect = true;
            this.ObjListViewCalibrarion.GridLines = true;
            this.ObjListViewCalibrarion.HideSelection = false;
            this.ObjListViewCalibrarion.IncludeColumnHeadersInCopy = true;
            this.ObjListViewCalibrarion.Location = new System.Drawing.Point(304, 13);
            this.ObjListViewCalibrarion.Name = "ObjListViewCalibrarion";
            this.ObjListViewCalibrarion.PersistentCheckBoxes = false;
            this.ObjListViewCalibrarion.ShowGroups = false;
            this.ObjListViewCalibrarion.ShowHeaderInAllViews = false;
            this.ObjListViewCalibrarion.ShowImagesOnSubItems = true;
            this.ObjListViewCalibrarion.ShowItemToolTips = true;
            this.ObjListViewCalibrarion.Size = new System.Drawing.Size(382, 214);
            this.ObjListViewCalibrarion.TabIndex = 24;
            this.ObjListViewCalibrarion.UseAlternatingBackColors = true;
            this.ObjListViewCalibrarion.UseCellFormatEvents = true;
            this.ObjListViewCalibrarion.UseCompatibleStateImageBehavior = false;
            this.ObjListViewCalibrarion.UseFilterIndicator = true;
            this.ObjListViewCalibrarion.UseFiltering = true;
            this.ObjListViewCalibrarion.UseHotItem = true;
            this.ObjListViewCalibrarion.View = System.Windows.Forms.View.Details;
            // 
            // ColmChoice
            // 
            this.ColmChoice.AspectName = "Choice";
            this.ColmChoice.HeaderCheckBox = true;
            this.ColmChoice.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ColmChoice.Text = "ВКЛ";
            this.ColmChoice.Width = 75;
            // 
            // ColmRange
            // 
            this.ColmRange.AspectName = "Range";
            this.ColmRange.Text = "Диапазон, МГц";
            this.ColmRange.Width = 82;
            // 
            // ColmStep
            // 
            this.ColmStep.AspectName = "Step";
            this.ColmStep.Text = "Шаг, МГц";
            this.ColmStep.Width = 71;
            // 
            // ColmResult
            // 
            this.ColmResult.AspectName = "Result";
            this.ColmResult.IsEditable = false;
            this.ColmResult.Renderer = this.RendererResult;
            this.ColmResult.Text = "Результат";
            this.ColmResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ColmResult.Width = 78;
            // 
            // RendererResult
            // 
            this.RendererResult.ImageList = this.ImagListResult;
            // 
            // ImagListResult
            // 
            this.ImagListResult.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImagListResult.ImageStream")));
            this.ImagListResult.TransparentColor = System.Drawing.Color.Transparent;
            this.ImagListResult.Images.SetKeyName(0, "gray.png");
            this.ImagListResult.Images.SetKeyName(1, "green.png");
            this.ImagListResult.Images.SetKeyName(2, "red.png");
            this.ImagListResult.Images.SetKeyName(3, "yellow.png");
            // 
            // ColmTime
            // 
            this.ColmTime.AspectName = "Time";
            this.ColmTime.AspectToStringFormat = "{0:t}";
            this.ColmTime.Text = "Время";
            this.ColmTime.Width = 70;
            // 
            // TPageAnalysis
            // 
            this.TPageAnalysis.BackColor = System.Drawing.SystemColors.Control;
            this.TPageAnalysis.Controls.Add(this.GroupBoxStatisticsAnalys);
            this.TPageAnalysis.Controls.Add(this.ProgressAnalysis);
            this.TPageAnalysis.Controls.Add(this.groupBoxStateAnalysResult);
            this.TPageAnalysis.Controls.Add(this.TStripAnalysis);
            this.TPageAnalysis.Controls.Add(this.LabAzimutAnalys);
            this.TPageAnalysis.Controls.Add(this.NumBoxAnalysisAzimut);
            this.TPageAnalysis.Controls.Add(this.PanelTypeReqAnalys);
            this.TPageAnalysis.Controls.Add(this.ObjListViewAnalysis);
            this.TPageAnalysis.Controls.Add(this.GroupBoxFileFreqAnalysis);
            this.TPageAnalysis.Controls.Add(this.GroupBoxFreqAnalysis);
            this.TPageAnalysis.Location = new System.Drawing.Point(4, 22);
            this.TPageAnalysis.Name = "TPageAnalysis";
            this.TPageAnalysis.Padding = new System.Windows.Forms.Padding(3);
            this.TPageAnalysis.Size = new System.Drawing.Size(699, 403);
            this.TPageAnalysis.TabIndex = 1;
            this.TPageAnalysis.Text = "Анализ ";
            // 
            // GroupBoxStatisticsAnalys
            // 
            this.GroupBoxStatisticsAnalys.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxStatisticsAnalys.Controls.Add(this.labStd);
            this.GroupBoxStatisticsAnalys.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.GroupBoxStatisticsAnalys.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.GroupBoxStatisticsAnalys.Location = new System.Drawing.Point(15, 350);
            this.GroupBoxStatisticsAnalys.Name = "GroupBoxStatisticsAnalys";
            this.GroupBoxStatisticsAnalys.Size = new System.Drawing.Size(188, 45);
            this.GroupBoxStatisticsAnalys.TabIndex = 38;
            this.GroupBoxStatisticsAnalys.TabStop = false;
            this.GroupBoxStatisticsAnalys.Text = "Статистика";
            // 
            // labStd
            // 
            this.labStd.AutoSize = true;
            this.labStd.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labStd.ForeColor = System.Drawing.SystemColors.Desktop;
            this.labStd.Location = new System.Drawing.Point(6, 19);
            this.labStd.Name = "labStd";
            this.labStd.Size = new System.Drawing.Size(34, 14);
            this.labStd.TabIndex = 5;
            this.labStd.Text = "СКО";
            // 
            // ProgressAnalysis
            // 
            this.ProgressAnalysis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProgressAnalysis.Location = new System.Drawing.Point(358, 370);
            this.ProgressAnalysis.Name = "ProgressAnalysis";
            this.ProgressAnalysis.Size = new System.Drawing.Size(299, 27);
            this.ProgressAnalysis.TabIndex = 37;
            // 
            // groupBoxStateAnalysResult
            // 
            this.groupBoxStateAnalysResult.Controls.Add(this.CheckBoxProgressAnalysis);
            this.groupBoxStateAnalysResult.Controls.Add(this.CheckShowDataExchange);
            this.groupBoxStateAnalysResult.Location = new System.Drawing.Point(15, 277);
            this.groupBoxStateAnalysResult.Name = "groupBoxStateAnalysResult";
            this.groupBoxStateAnalysResult.Size = new System.Drawing.Size(188, 57);
            this.groupBoxStateAnalysResult.TabIndex = 36;
            this.groupBoxStateAnalysResult.TabStop = false;
            // 
            // CheckBoxProgressAnalysis
            // 
            this.CheckBoxProgressAnalysis.AutoSize = true;
            this.CheckBoxProgressAnalysis.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckBoxProgressAnalysis.Location = new System.Drawing.Point(6, 10);
            this.CheckBoxProgressAnalysis.Name = "CheckBoxProgressAnalysis";
            this.CheckBoxProgressAnalysis.Size = new System.Drawing.Size(170, 18);
            this.CheckBoxProgressAnalysis.TabIndex = 16;
            this.CheckBoxProgressAnalysis.Text = "Отображать прогресс";
            this.CheckBoxProgressAnalysis.UseVisualStyleBackColor = true;
            this.CheckBoxProgressAnalysis.CheckedChanged += new System.EventHandler(this.CheckBoxProgressAnalysis_CheckedChanged);
            // 
            // CheckShowDataExchange
            // 
            this.CheckShowDataExchange.AutoSize = true;
            this.CheckShowDataExchange.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckShowDataExchange.Location = new System.Drawing.Point(6, 31);
            this.CheckShowDataExchange.Name = "CheckShowDataExchange";
            this.CheckShowDataExchange.Size = new System.Drawing.Size(135, 18);
            this.CheckShowDataExchange.TabIndex = 19;
            this.CheckShowDataExchange.Text = "Отображать ЖО";
            this.CheckShowDataExchange.UseVisualStyleBackColor = true;
            this.CheckShowDataExchange.CheckedChanged += new System.EventHandler(this.CheckShowDataExchange_CheckedChanged);
            // 
            // TStripAnalysis
            // 
            this.TStripAnalysis.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TStripAnalysis.Dock = System.Windows.Forms.DockStyle.None;
            this.TStripAnalysis.ImageScalingSize = new System.Drawing.Size(22, 22);
            this.TStripAnalysis.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ButAddAnalysisRange,
            this.ButClearAnalysis,
            this.ButDeleteAnalysisRange,
            this.ButStartAnalysis,
            this.ButStopAnalysis,
            this.ButPauseAnalysis,
            this.ButContinueAnalysis,
            this.ButSaveDataAnalysis});
            this.TStripAnalysis.Location = new System.Drawing.Point(16, 223);
            this.TStripAnalysis.Name = "TStripAnalysis";
            this.TStripAnalysis.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.TStripAnalysis.Size = new System.Drawing.Size(228, 39);
            this.TStripAnalysis.TabIndex = 35;
            this.TStripAnalysis.Text = "toolStrip1";
            // 
            // ButAddAnalysisRange
            // 
            this.ButAddAnalysisRange.AutoSize = false;
            this.ButAddAnalysisRange.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButAddAnalysisRange.Image = global::Corrector.Properties.Resources.Add;
            this.ButAddAnalysisRange.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButAddAnalysisRange.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButAddAnalysisRange.Name = "ButAddAnalysisRange";
            this.ButAddAnalysisRange.Size = new System.Drawing.Size(36, 36);
            this.ButAddAnalysisRange.Text = "tlSButAdd";
            this.ButAddAnalysisRange.ToolTipText = "Добавить в таблицу";
            this.ButAddAnalysisRange.Click += new System.EventHandler(this.AddRangeToAnalysisListView);
            // 
            // ButClearAnalysis
            // 
            this.ButClearAnalysis.AutoSize = false;
            this.ButClearAnalysis.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButClearAnalysis.Image = global::Corrector.Properties.Resources.Clear;
            this.ButClearAnalysis.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButClearAnalysis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButClearAnalysis.Name = "ButClearAnalysis";
            this.ButClearAnalysis.Size = new System.Drawing.Size(36, 36);
            this.ButClearAnalysis.Text = "toolStripButton1";
            this.ButClearAnalysis.ToolTipText = "Очистить таблицу";
            this.ButClearAnalysis.Click += new System.EventHandler(this.ButClearAnalysis_Click);
            // 
            // ButDeleteAnalysisRange
            // 
            this.ButDeleteAnalysisRange.AutoSize = false;
            this.ButDeleteAnalysisRange.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButDeleteAnalysisRange.Image = global::Corrector.Properties.Resources.Dell;
            this.ButDeleteAnalysisRange.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButDeleteAnalysisRange.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButDeleteAnalysisRange.Name = "ButDeleteAnalysisRange";
            this.ButDeleteAnalysisRange.Size = new System.Drawing.Size(36, 36);
            this.ButDeleteAnalysisRange.Text = "toolStripButton2";
            this.ButDeleteAnalysisRange.ToolTipText = "Удалить из таблицы";
            this.ButDeleteAnalysisRange.Visible = false;
            this.ButDeleteAnalysisRange.Click += new System.EventHandler(this.ButDeleteAnalysisRange_Click);
            // 
            // ButStartAnalysis
            // 
            this.ButStartAnalysis.AutoSize = false;
            this.ButStartAnalysis.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButStartAnalysis.Image = global::Corrector.Properties.Resources.Start;
            this.ButStartAnalysis.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButStartAnalysis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButStartAnalysis.Name = "ButStartAnalysis";
            this.ButStartAnalysis.Size = new System.Drawing.Size(36, 36);
            this.ButStartAnalysis.Text = "toolStripButton3";
            this.ButStartAnalysis.ToolTipText = "Начать Опрос";
            this.ButStartAnalysis.Click += new System.EventHandler(this.ButStartAnalysis_Click);
            // 
            // ButStopAnalysis
            // 
            this.ButStopAnalysis.AutoSize = false;
            this.ButStopAnalysis.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButStopAnalysis.Enabled = false;
            this.ButStopAnalysis.Image = global::Corrector.Properties.Resources.Stop;
            this.ButStopAnalysis.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButStopAnalysis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButStopAnalysis.Name = "ButStopAnalysis";
            this.ButStopAnalysis.Size = new System.Drawing.Size(36, 36);
            this.ButStopAnalysis.Text = "toolStripButton4";
            this.ButStopAnalysis.ToolTipText = "Остановить Опрос";
            this.ButStopAnalysis.Click += new System.EventHandler(this.ButStopAnalysis_Click);
            // 
            // ButPauseAnalysis
            // 
            this.ButPauseAnalysis.AutoSize = false;
            this.ButPauseAnalysis.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButPauseAnalysis.Enabled = false;
            this.ButPauseAnalysis.Image = global::Corrector.Properties.Resources.Pause;
            this.ButPauseAnalysis.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButPauseAnalysis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButPauseAnalysis.Name = "ButPauseAnalysis";
            this.ButPauseAnalysis.Size = new System.Drawing.Size(36, 36);
            this.ButPauseAnalysis.Text = "toolStripButton5";
            this.ButPauseAnalysis.ToolTipText = "Приостановить Опрос";
            this.ButPauseAnalysis.Click += new System.EventHandler(this.ButPauseAnalysis_Click);
            // 
            // ButContinueAnalysis
            // 
            this.ButContinueAnalysis.AutoSize = false;
            this.ButContinueAnalysis.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButContinueAnalysis.Image = global::Corrector.Properties.Resources.Continue;
            this.ButContinueAnalysis.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButContinueAnalysis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButContinueAnalysis.Name = "ButContinueAnalysis";
            this.ButContinueAnalysis.Size = new System.Drawing.Size(36, 36);
            this.ButContinueAnalysis.Text = "toolStripButton6";
            this.ButContinueAnalysis.ToolTipText = "Продолжить Опрос";
            this.ButContinueAnalysis.Visible = false;
            this.ButContinueAnalysis.Click += new System.EventHandler(this.ButContinueAnalysis_Click);
            // 
            // ButSaveDataAnalysis
            // 
            this.ButSaveDataAnalysis.AutoSize = false;
            this.ButSaveDataAnalysis.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButSaveDataAnalysis.Image = global::Corrector.Properties.Resources.Save;
            this.ButSaveDataAnalysis.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButSaveDataAnalysis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButSaveDataAnalysis.Name = "ButSaveDataAnalysis";
            this.ButSaveDataAnalysis.Size = new System.Drawing.Size(36, 36);
            this.ButSaveDataAnalysis.Text = "toolStripButton8";
            this.ButSaveDataAnalysis.ToolTipText = "Сохранить данные";
            this.ButSaveDataAnalysis.Click += new System.EventHandler(this.ButSaveDataAnalysis_Click);
            // 
            // LabAzimutAnalys
            // 
            this.LabAzimutAnalys.AutoSize = true;
            this.LabAzimutAnalys.Font = new System.Drawing.Font("Verdana", 11.25F);
            this.LabAzimutAnalys.Location = new System.Drawing.Point(18, 13);
            this.LabAzimutAnalys.Name = "LabAzimutAnalys";
            this.LabAzimutAnalys.Size = new System.Drawing.Size(79, 18);
            this.LabAzimutAnalys.TabIndex = 30;
            this.LabAzimutAnalys.Text = "Азимут, °";
            // 
            // NumBoxAnalysisAzimut
            // 
            this.NumBoxAnalysisAzimut.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxAnalysisAzimut.Location = new System.Drawing.Point(200, 13);
            this.NumBoxAnalysisAzimut.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NumBoxAnalysisAzimut.Name = "NumBoxAnalysisAzimut";
            this.NumBoxAnalysisAzimut.Size = new System.Drawing.Size(71, 23);
            this.NumBoxAnalysisAzimut.TabIndex = 31;
            // 
            // PanelTypeReqAnalys
            // 
            this.PanelTypeReqAnalys.Controls.Add(this.RadButAnalysisFreq);
            this.PanelTypeReqAnalys.Controls.Add(this.RadButAnalysisFileFreq);
            this.PanelTypeReqAnalys.Location = new System.Drawing.Point(15, 52);
            this.PanelTypeReqAnalys.Name = "PanelTypeReqAnalys";
            this.PanelTypeReqAnalys.Size = new System.Drawing.Size(272, 29);
            this.PanelTypeReqAnalys.TabIndex = 34;
            // 
            // RadButAnalysisFreq
            // 
            this.RadButAnalysisFreq.AutoSize = true;
            this.RadButAnalysisFreq.Font = new System.Drawing.Font("Verdana", 11.25F);
            this.RadButAnalysisFreq.Location = new System.Drawing.Point(185, 4);
            this.RadButAnalysisFreq.Name = "RadButAnalysisFreq";
            this.RadButAnalysisFreq.Size = new System.Drawing.Size(59, 22);
            this.RadButAnalysisFreq.TabIndex = 3;
            this.RadButAnalysisFreq.Text = "ФРЧ";
            this.RadButAnalysisFreq.UseVisualStyleBackColor = true;
            this.RadButAnalysisFreq.CheckedChanged += new System.EventHandler(this.ChangeCheckedTypeReqAnalysis);
            // 
            // RadButAnalysisFileFreq
            // 
            this.RadButAnalysisFileFreq.AutoSize = true;
            this.RadButAnalysisFileFreq.Checked = true;
            this.RadButAnalysisFileFreq.Font = new System.Drawing.Font("Verdana", 11.25F);
            this.RadButAnalysisFileFreq.Location = new System.Drawing.Point(10, 4);
            this.RadButAnalysisFileFreq.Name = "RadButAnalysisFileFreq";
            this.RadButAnalysisFileFreq.Size = new System.Drawing.Size(126, 22);
            this.RadButAnalysisFileFreq.TabIndex = 2;
            this.RadButAnalysisFileFreq.TabStop = true;
            this.RadButAnalysisFileFreq.Text = "Сетка частот";
            this.RadButAnalysisFileFreq.UseVisualStyleBackColor = true;
            this.RadButAnalysisFileFreq.CheckedChanged += new System.EventHandler(this.ChangeCheckedTypeReqAnalysis);
            // 
            // ObjListViewAnalysis
            // 
            this.ObjListViewAnalysis.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.ObjListViewAnalysis.AllColumns.Add(this.ColmFrequency);
            this.ObjListViewAnalysis.AllColumns.Add(this.ColmDirection);
            this.ObjListViewAnalysis.AllColumns.Add(this.ColmAnalysisResult);
            this.ObjListViewAnalysis.AllColumns.Add(this.ColmDelta);
            this.ObjListViewAnalysis.AllColumns.Add(this.ColmHiddenRange);
            this.ObjListViewAnalysis.AllColumns.Add(this.ColmHiddenStep);
            this.ObjListViewAnalysis.AllowColumnReorder = true;
            this.ObjListViewAnalysis.AllowDrop = true;
            this.ObjListViewAnalysis.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ObjListViewAnalysis.CellEditUseWholeCell = false;
            this.ObjListViewAnalysis.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColmFrequency,
            this.ColmDirection,
            this.ColmAnalysisResult,
            this.ColmDelta});
            this.ObjListViewAnalysis.Cursor = System.Windows.Forms.Cursors.Default;
            this.ObjListViewAnalysis.FullRowSelect = true;
            this.ObjListViewAnalysis.GridLines = true;
            this.ObjListViewAnalysis.HideSelection = false;
            this.ObjListViewAnalysis.IncludeColumnHeadersInCopy = true;
            this.ObjListViewAnalysis.Location = new System.Drawing.Point(358, 13);
            this.ObjListViewAnalysis.Name = "ObjListViewAnalysis";
            this.ObjListViewAnalysis.PersistentCheckBoxes = false;
            this.ObjListViewAnalysis.ShowGroups = false;
            this.ObjListViewAnalysis.ShowHeaderInAllViews = false;
            this.ObjListViewAnalysis.ShowImagesOnSubItems = true;
            this.ObjListViewAnalysis.ShowItemToolTips = true;
            this.ObjListViewAnalysis.Size = new System.Drawing.Size(299, 351);
            this.ObjListViewAnalysis.TabIndex = 25;
            this.ObjListViewAnalysis.UseAlternatingBackColors = true;
            this.ObjListViewAnalysis.UseCellFormatEvents = true;
            this.ObjListViewAnalysis.UseCompatibleStateImageBehavior = false;
            this.ObjListViewAnalysis.UseFilterIndicator = true;
            this.ObjListViewAnalysis.UseFiltering = true;
            this.ObjListViewAnalysis.UseHotItem = true;
            this.ObjListViewAnalysis.View = System.Windows.Forms.View.Details;
            // 
            // ColmFrequency
            // 
            this.ColmFrequency.AspectName = "ColmFrequency";
            this.ColmFrequency.Text = "Частота, КГц";
            this.ColmFrequency.Width = 84;
            // 
            // ColmDirection
            // 
            this.ColmDirection.AspectName = "ColmDirection";
            this.ColmDirection.Text = "Пеленг";
            this.ColmDirection.Width = 70;
            // 
            // ColmAnalysisResult
            // 
            this.ColmAnalysisResult.AspectName = "ColmAnalysisResult";
            this.ColmAnalysisResult.IsEditable = false;
            this.ColmAnalysisResult.Renderer = this.RendererResult;
            this.ColmAnalysisResult.Text = "Результат";
            this.ColmAnalysisResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ColmAnalysisResult.Width = 78;
            // 
            // ColmDelta
            // 
            this.ColmDelta.AspectName = "ColmDelta";
            this.ColmDelta.Text = "Дельта";
            this.ColmDelta.Width = 62;
            // 
            // ColmHiddenRange
            // 
            this.ColmHiddenRange.AspectName = "ColmHiddenRange";
            this.ColmHiddenRange.DisplayIndex = 4;
            this.ColmHiddenRange.IsVisible = false;
            // 
            // ColmHiddenStep
            // 
            this.ColmHiddenStep.AspectName = "ColmHiddenStep";
            this.ColmHiddenStep.AspectToStringFormat = "";
            this.ColmHiddenStep.IsVisible = false;
            // 
            // GroupBoxFileFreqAnalysis
            // 
            this.GroupBoxFileFreqAnalysis.Controls.Add(this.LabAnalysisRangeStop);
            this.GroupBoxFileFreqAnalysis.Controls.Add(this.LabAnalysisRangeStart);
            this.GroupBoxFileFreqAnalysis.Controls.Add(this.NumBoxAnalysisEPOStep);
            this.GroupBoxFileFreqAnalysis.Controls.Add(this.LabStepFileFreqAnalys);
            this.GroupBoxFileFreqAnalysis.Controls.Add(this.NumBoxAnalysisEPOStop);
            this.GroupBoxFileFreqAnalysis.Controls.Add(this.LabEPOStopAnalys);
            this.GroupBoxFileFreqAnalysis.Controls.Add(this.NumBoxAnalysisEPOStart);
            this.GroupBoxFileFreqAnalysis.Controls.Add(this.LabEPOStartAnalys);
            this.GroupBoxFileFreqAnalysis.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GroupBoxFileFreqAnalysis.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.GroupBoxFileFreqAnalysis.Location = new System.Drawing.Point(15, 92);
            this.GroupBoxFileFreqAnalysis.Name = "GroupBoxFileFreqAnalysis";
            this.GroupBoxFileFreqAnalysis.Size = new System.Drawing.Size(272, 121);
            this.GroupBoxFileFreqAnalysis.TabIndex = 32;
            this.GroupBoxFileFreqAnalysis.TabStop = false;
            this.GroupBoxFileFreqAnalysis.Text = "Сетка частот";
            // 
            // LabAnalysisRangeStop
            // 
            this.LabAnalysisRangeStop.AutoSize = true;
            this.LabAnalysisRangeStop.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabAnalysisRangeStop.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabAnalysisRangeStop.Location = new System.Drawing.Point(93, 55);
            this.LabAnalysisRangeStop.Name = "LabAnalysisRangeStop";
            this.LabAnalysisRangeStop.Size = new System.Drawing.Size(95, 13);
            this.LabAnalysisRangeStop.TabIndex = 11;
            this.LabAnalysisRangeStop.Text = "3000-6000 МГц";
            // 
            // LabAnalysisRangeStart
            // 
            this.LabAnalysisRangeStart.AutoSize = true;
            this.LabAnalysisRangeStart.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabAnalysisRangeStart.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabAnalysisRangeStart.Location = new System.Drawing.Point(93, 25);
            this.LabAnalysisRangeStart.Name = "LabAnalysisRangeStart";
            this.LabAnalysisRangeStart.Size = new System.Drawing.Size(95, 13);
            this.LabAnalysisRangeStart.TabIndex = 10;
            this.LabAnalysisRangeStart.Text = "3000-6000 МГц";
            // 
            // NumBoxAnalysisEPOStep
            // 
            this.NumBoxAnalysisEPOStep.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxAnalysisEPOStep.Location = new System.Drawing.Point(194, 80);
            this.NumBoxAnalysisEPOStep.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.NumBoxAnalysisEPOStep.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumBoxAnalysisEPOStep.Name = "NumBoxAnalysisEPOStep";
            this.NumBoxAnalysisEPOStep.Size = new System.Drawing.Size(71, 23);
            this.NumBoxAnalysisEPOStep.TabIndex = 9;
            this.NumBoxAnalysisEPOStep.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // LabStepFileFreqAnalys
            // 
            this.LabStepFileFreqAnalys.AutoSize = true;
            this.LabStepFileFreqAnalys.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabStepFileFreqAnalys.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabStepFileFreqAnalys.Location = new System.Drawing.Point(3, 83);
            this.LabStepFileFreqAnalys.Name = "LabStepFileFreqAnalys";
            this.LabStepFileFreqAnalys.Size = new System.Drawing.Size(81, 18);
            this.LabStepFileFreqAnalys.TabIndex = 8;
            this.LabStepFileFreqAnalys.Text = "Шаг, МГц";
            // 
            // NumBoxAnalysisEPOStop
            // 
            this.NumBoxAnalysisEPOStop.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxAnalysisEPOStop.Location = new System.Drawing.Point(194, 50);
            this.NumBoxAnalysisEPOStop.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NumBoxAnalysisEPOStop.Name = "NumBoxAnalysisEPOStop";
            this.NumBoxAnalysisEPOStop.Size = new System.Drawing.Size(71, 23);
            this.NumBoxAnalysisEPOStop.TabIndex = 7;
            this.NumBoxAnalysisEPOStop.ValueChanged += new System.EventHandler(this.NumBoxAnalysisEPOStop_ValueChanged);
            // 
            // LabEPOStopAnalys
            // 
            this.LabEPOStopAnalys.AutoSize = true;
            this.LabEPOStopAnalys.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabEPOStopAnalys.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabEPOStopAnalys.Location = new System.Drawing.Point(3, 51);
            this.LabEPOStopAnalys.Name = "LabEPOStopAnalys";
            this.LabEPOStopAnalys.Size = new System.Drawing.Size(82, 18);
            this.LabEPOStopAnalys.TabIndex = 6;
            this.LabEPOStopAnalys.Text = "ПОО стоп";
            // 
            // NumBoxAnalysisEPOStart
            // 
            this.NumBoxAnalysisEPOStart.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxAnalysisEPOStart.Location = new System.Drawing.Point(194, 20);
            this.NumBoxAnalysisEPOStart.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NumBoxAnalysisEPOStart.Name = "NumBoxAnalysisEPOStart";
            this.NumBoxAnalysisEPOStart.Size = new System.Drawing.Size(71, 23);
            this.NumBoxAnalysisEPOStart.TabIndex = 5;
            this.NumBoxAnalysisEPOStart.ValueChanged += new System.EventHandler(this.NumBoxAnalysisEPOStart_ValueChanged);
            // 
            // LabEPOStartAnalys
            // 
            this.LabEPOStartAnalys.AutoSize = true;
            this.LabEPOStartAnalys.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabEPOStartAnalys.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabEPOStartAnalys.Location = new System.Drawing.Point(3, 20);
            this.LabEPOStartAnalys.Name = "LabEPOStartAnalys";
            this.LabEPOStartAnalys.Size = new System.Drawing.Size(88, 18);
            this.LabEPOStartAnalys.TabIndex = 4;
            this.LabEPOStartAnalys.Text = "ПОО старт";
            // 
            // GroupBoxFreqAnalysis
            // 
            this.GroupBoxFreqAnalysis.Controls.Add(this.NumBoxAnalysisFreqStep);
            this.GroupBoxFreqAnalysis.Controls.Add(this.label7);
            this.GroupBoxFreqAnalysis.Controls.Add(this.NumBoxAnalysisFreqStop);
            this.GroupBoxFreqAnalysis.Controls.Add(this.label8);
            this.GroupBoxFreqAnalysis.Controls.Add(this.NumBoxAnalysisFreqStart);
            this.GroupBoxFreqAnalysis.Controls.Add(this.label9);
            this.GroupBoxFreqAnalysis.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GroupBoxFreqAnalysis.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.GroupBoxFreqAnalysis.Location = new System.Drawing.Point(16, 92);
            this.GroupBoxFreqAnalysis.Name = "GroupBoxFreqAnalysis";
            this.GroupBoxFreqAnalysis.Size = new System.Drawing.Size(271, 120);
            this.GroupBoxFreqAnalysis.TabIndex = 33;
            this.GroupBoxFreqAnalysis.TabStop = false;
            this.GroupBoxFreqAnalysis.Text = "ФРЧ";
            // 
            // NumBoxAnalysisFreqStep
            // 
            this.NumBoxAnalysisFreqStep.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxAnalysisFreqStep.Location = new System.Drawing.Point(184, 80);
            this.NumBoxAnalysisFreqStep.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.NumBoxAnalysisFreqStep.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumBoxAnalysisFreqStep.Name = "NumBoxAnalysisFreqStep";
            this.NumBoxAnalysisFreqStep.Size = new System.Drawing.Size(71, 23);
            this.NumBoxAnalysisFreqStep.TabIndex = 9;
            this.NumBoxAnalysisFreqStep.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label7.Location = new System.Drawing.Point(6, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 18);
            this.label7.TabIndex = 8;
            this.label7.Text = "Шаг, МГц";
            // 
            // NumBoxAnalysisFreqStop
            // 
            this.NumBoxAnalysisFreqStop.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxAnalysisFreqStop.Location = new System.Drawing.Point(184, 50);
            this.NumBoxAnalysisFreqStop.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NumBoxAnalysisFreqStop.Name = "NumBoxAnalysisFreqStop";
            this.NumBoxAnalysisFreqStop.Size = new System.Drawing.Size(71, 23);
            this.NumBoxAnalysisFreqStop.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label8.Location = new System.Drawing.Point(6, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(147, 18);
            this.label8.TabIndex = 6;
            this.label8.Text = "Частота конечная";
            // 
            // NumBoxAnalysisFreqStart
            // 
            this.NumBoxAnalysisFreqStart.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBoxAnalysisFreqStart.Location = new System.Drawing.Point(184, 20);
            this.NumBoxAnalysisFreqStart.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NumBoxAnalysisFreqStart.Name = "NumBoxAnalysisFreqStart";
            this.NumBoxAnalysisFreqStart.Size = new System.Drawing.Size(71, 23);
            this.NumBoxAnalysisFreqStart.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label9.Location = new System.Drawing.Point(6, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(155, 18);
            this.label9.TabIndex = 4;
            this.label9.Text = "Частота начальная";
            // 
            // GroupBoxStateHeter
            // 
            this.GroupBoxStateHeter.Controls.Add(this.ProgressChargeHeter);
            this.GroupBoxStateHeter.Controls.Add(this.ButTestRadio);
            this.GroupBoxStateHeter.Controls.Add(this.LabChargeHeter);
            this.GroupBoxStateHeter.Controls.Add(this.LabHumidHeter);
            this.GroupBoxStateHeter.Controls.Add(this.LabTempHeter);
            this.GroupBoxStateHeter.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.GroupBoxStateHeter.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.GroupBoxStateHeter.Location = new System.Drawing.Point(342, 433);
            this.GroupBoxStateHeter.Name = "GroupBoxStateHeter";
            this.GroupBoxStateHeter.Size = new System.Drawing.Size(367, 67);
            this.GroupBoxStateHeter.TabIndex = 27;
            this.GroupBoxStateHeter.TabStop = false;
            this.GroupBoxStateHeter.Text = "Состояние КГ";
            // 
            // ProgressChargeHeter
            // 
            this.ProgressChargeHeter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProgressChargeHeter.Cursor = System.Windows.Forms.Cursors.Default;
            this.ProgressChargeHeter.Location = new System.Drawing.Point(232, 53);
            this.ProgressChargeHeter.MarqueeAnimationSpeed = 0;
            this.ProgressChargeHeter.Maximum = 168;
            this.ProgressChargeHeter.Minimum = 128;
            this.ProgressChargeHeter.Name = "ProgressChargeHeter";
            this.ProgressChargeHeter.Size = new System.Drawing.Size(135, 11);
            this.ProgressChargeHeter.TabIndex = 38;
            this.ProgressChargeHeter.Value = 128;
            // 
            // ButTestRadio
            // 
            this.ButTestRadio.BackgroundImage = global::Corrector.Properties.Resources.TestRadio;
            this.ButTestRadio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButTestRadio.Location = new System.Drawing.Point(331, 10);
            this.ButTestRadio.Name = "ButTestRadio";
            this.ButTestRadio.Size = new System.Drawing.Size(36, 36);
            this.ButTestRadio.TabIndex = 124;
            this.ButTestRadio.UseVisualStyleBackColor = true;
            this.ButTestRadio.Click += new System.EventHandler(this.ButTestRadio_Click);
            // 
            // LabChargeHeter
            // 
            this.LabChargeHeter.AutoSize = true;
            this.LabChargeHeter.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LabChargeHeter.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabChargeHeter.Location = new System.Drawing.Point(228, 20);
            this.LabChargeHeter.Name = "LabChargeHeter";
            this.LabChargeHeter.Size = new System.Drawing.Size(78, 16);
            this.LabChargeHeter.TabIndex = 123;
            this.LabChargeHeter.Text = "Заряд АКБ\r\n";
            this.LabChargeHeter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabHumidHeter
            // 
            this.LabHumidHeter.AutoSize = true;
            this.LabHumidHeter.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabHumidHeter.Location = new System.Drawing.Point(123, 21);
            this.LabHumidHeter.Name = "LabHumidHeter";
            this.LabHumidHeter.Size = new System.Drawing.Size(81, 16);
            this.LabHumidHeter.TabIndex = 122;
            this.LabHumidHeter.Text = "Влажность\r\n";
            this.LabHumidHeter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabTempHeter
            // 
            this.LabTempHeter.AutoSize = true;
            this.LabTempHeter.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabTempHeter.Location = new System.Drawing.Point(5, 20);
            this.LabTempHeter.Name = "LabTempHeter";
            this.LabTempHeter.Size = new System.Drawing.Size(97, 16);
            this.LabTempHeter.TabIndex = 121;
            this.LabTempHeter.Text = "Температура\r\n";
            this.LabTempHeter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GroupBoxConnectHeter
            // 
            this.GroupBoxConnectHeter.Controls.Add(this.ButRefresh);
            this.GroupBoxConnectHeter.Controls.Add(this.ComSpeed);
            this.GroupBoxConnectHeter.Controls.Add(this.ComPortName);
            this.GroupBoxConnectHeter.Controls.Add(this.ButHeterConnect);
            this.GroupBoxConnectHeter.Controls.Add(this.PicBoxHeterWrite);
            this.GroupBoxConnectHeter.Controls.Add(this.PicBoxHeterRead);
            this.GroupBoxConnectHeter.Controls.Add(this.LabHeterConnect);
            this.GroupBoxConnectHeter.Location = new System.Drawing.Point(172, 435);
            this.GroupBoxConnectHeter.Name = "GroupBoxConnectHeter";
            this.GroupBoxConnectHeter.Size = new System.Drawing.Size(164, 65);
            this.GroupBoxConnectHeter.TabIndex = 26;
            this.GroupBoxConnectHeter.TabStop = false;
            // 
            // ButRefresh
            // 
            this.ButRefresh.BackgroundImage = global::Corrector.Properties.Resources.refresh1;
            this.ButRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButRefresh.Location = new System.Drawing.Point(6, 11);
            this.ButRefresh.Name = "ButRefresh";
            this.ButRefresh.Size = new System.Drawing.Size(26, 26);
            this.ButRefresh.TabIndex = 36;
            this.ButRefresh.UseVisualStyleBackColor = true;
            this.ButRefresh.Click += new System.EventHandler(this.GetPortName);
            // 
            // ComSpeed
            // 
            this.ComSpeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.ComSpeed.FormattingEnabled = true;
            this.ComSpeed.Items.AddRange(new object[] {
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.ComSpeed.Location = new System.Drawing.Point(95, 41);
            this.ComSpeed.Name = "ComSpeed";
            this.ComSpeed.Size = new System.Drawing.Size(65, 21);
            this.ComSpeed.TabIndex = 32;
            this.ComSpeed.SelectedIndexChanged += new System.EventHandler(this.ComSpeed_SelectedIndexChanged);
            // 
            // ComPortName
            // 
            this.ComPortName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComPortName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.ComPortName.FormattingEnabled = true;
            this.ComPortName.Location = new System.Drawing.Point(6, 41);
            this.ComPortName.Name = "ComPortName";
            this.ComPortName.Size = new System.Drawing.Size(62, 21);
            this.ComPortName.TabIndex = 31;
            this.ComPortName.SelectedIndexChanged += new System.EventHandler(this.ComPortName_SelectedIndexChanged);
            // 
            // ButHeterConnect
            // 
            this.ButHeterConnect.BackColor = System.Drawing.Color.Maroon;
            this.ButHeterConnect.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButHeterConnect.Location = new System.Drawing.Point(77, 11);
            this.ButHeterConnect.Name = "ButHeterConnect";
            this.ButHeterConnect.Size = new System.Drawing.Size(26, 26);
            this.ButHeterConnect.TabIndex = 16;
            this.ButHeterConnect.UseVisualStyleBackColor = false;
            this.ButHeterConnect.Click += new System.EventHandler(this.ButHeterConnect_Click);
            // 
            // PicBoxHeterWrite
            // 
            this.PicBoxHeterWrite.Image = global::Corrector.Properties.Resources.gray;
            this.PicBoxHeterWrite.Location = new System.Drawing.Point(109, 13);
            this.PicBoxHeterWrite.Name = "PicBoxHeterWrite";
            this.PicBoxHeterWrite.Size = new System.Drawing.Size(22, 22);
            this.PicBoxHeterWrite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBoxHeterWrite.TabIndex = 14;
            this.PicBoxHeterWrite.TabStop = false;
            // 
            // PicBoxHeterRead
            // 
            this.PicBoxHeterRead.Image = global::Corrector.Properties.Resources.gray;
            this.PicBoxHeterRead.Location = new System.Drawing.Point(138, 13);
            this.PicBoxHeterRead.Name = "PicBoxHeterRead";
            this.PicBoxHeterRead.Size = new System.Drawing.Size(22, 22);
            this.PicBoxHeterRead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBoxHeterRead.TabIndex = 15;
            this.PicBoxHeterRead.TabStop = false;
            // 
            // LabHeterConnect
            // 
            this.LabHeterConnect.AutoSize = true;
            this.LabHeterConnect.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabHeterConnect.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabHeterConnect.Location = new System.Drawing.Point(40, 14);
            this.LabHeterConnect.Name = "LabHeterConnect";
            this.LabHeterConnect.Size = new System.Drawing.Size(27, 18);
            this.LabHeterConnect.TabIndex = 13;
            this.LabHeterConnect.Text = "КГ";
            this.LabHeterConnect.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LabHeterConnect_MouseHover);
            // 
            // GroupBoxConnectPL
            // 
            this.GroupBoxConnectPL.Controls.Add(this.ButSettings);
            this.GroupBoxConnectPL.Controls.Add(this.PicBoxPLRead);
            this.GroupBoxConnectPL.Controls.Add(this.PicBoxPLWrite);
            this.GroupBoxConnectPL.Controls.Add(this.LabPLConnect);
            this.GroupBoxConnectPL.Controls.Add(this.ButPLConnect);
            this.GroupBoxConnectPL.Location = new System.Drawing.Point(6, 435);
            this.GroupBoxConnectPL.Name = "GroupBoxConnectPL";
            this.GroupBoxConnectPL.Size = new System.Drawing.Size(164, 65);
            this.GroupBoxConnectPL.TabIndex = 25;
            this.GroupBoxConnectPL.TabStop = false;
            // 
            // ButSettings
            // 
            this.ButSettings.BackgroundImage = global::Corrector.Properties.Resources.parameters;
            this.ButSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButSettings.Location = new System.Drawing.Point(10, 17);
            this.ButSettings.Name = "ButSettings";
            this.ButSettings.Size = new System.Drawing.Size(26, 26);
            this.ButSettings.TabIndex = 35;
            this.ButSettings.UseVisualStyleBackColor = true;
            this.ButSettings.Click += new System.EventHandler(this.ChoseParam);
            // 
            // PicBoxPLRead
            // 
            this.PicBoxPLRead.Image = global::Corrector.Properties.Resources.gray;
            this.PicBoxPLRead.Location = new System.Drawing.Point(138, 17);
            this.PicBoxPLRead.Name = "PicBoxPLRead";
            this.PicBoxPLRead.Size = new System.Drawing.Size(22, 22);
            this.PicBoxPLRead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBoxPLRead.TabIndex = 12;
            this.PicBoxPLRead.TabStop = false;
            // 
            // PicBoxPLWrite
            // 
            this.PicBoxPLWrite.Image = global::Corrector.Properties.Resources.gray;
            this.PicBoxPLWrite.Location = new System.Drawing.Point(109, 17);
            this.PicBoxPLWrite.Name = "PicBoxPLWrite";
            this.PicBoxPLWrite.Size = new System.Drawing.Size(22, 22);
            this.PicBoxPLWrite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBoxPLWrite.TabIndex = 11;
            this.PicBoxPLWrite.TabStop = false;
            // 
            // LabPLConnect
            // 
            this.LabPLConnect.AutoSize = true;
            this.LabPLConnect.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabPLConnect.ForeColor = System.Drawing.SystemColors.Desktop;
            this.LabPLConnect.Location = new System.Drawing.Point(40, 19);
            this.LabPLConnect.Name = "LabPLConnect";
            this.LabPLConnect.Size = new System.Drawing.Size(30, 18);
            this.LabPLConnect.TabIndex = 10;
            this.LabPLConnect.Text = "ПЛ";
            this.LabPLConnect.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LabPLConnect_MouseMove);
            // 
            // ButPLConnect
            // 
            this.ButPLConnect.BackColor = System.Drawing.Color.Maroon;
            this.ButPLConnect.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButPLConnect.Location = new System.Drawing.Point(79, 16);
            this.ButPLConnect.Name = "ButPLConnect";
            this.ButPLConnect.Size = new System.Drawing.Size(26, 26);
            this.ButPLConnect.TabIndex = 9;
            this.ButPLConnect.UseVisualStyleBackColor = false;
            this.ButPLConnect.Click += new System.EventHandler(this.ButPLConnect_Click);
            // 
            // ExchangeLogGroupBox
            // 
            this.ExchangeLogGroupBox.Controls.Add(this.RichBoxHex);
            this.ExchangeLogGroupBox.Controls.Add(this.ButClear);
            this.ExchangeLogGroupBox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ExchangeLogGroupBox.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.ExchangeLogGroupBox.Location = new System.Drawing.Point(711, 12);
            this.ExchangeLogGroupBox.Name = "ExchangeLogGroupBox";
            this.ExchangeLogGroupBox.Size = new System.Drawing.Size(274, 417);
            this.ExchangeLogGroupBox.TabIndex = 30;
            this.ExchangeLogGroupBox.TabStop = false;
            this.ExchangeLogGroupBox.Text = "Журнал Обмена";
            // 
            // ButLanguage
            // 
            this.ButLanguage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButLanguage.Font = new System.Drawing.Font("Verdana", 11.25F);
            this.ButLanguage.Location = new System.Drawing.Point(951, 467);
            this.ButLanguage.Name = "ButLanguage";
            this.ButLanguage.Size = new System.Drawing.Size(39, 36);
            this.ButLanguage.TabIndex = 125;
            this.ButLanguage.Text = "EN";
            this.ButLanguage.UseVisualStyleBackColor = true;
            this.ButLanguage.Click += new System.EventHandler(this.ButLanguage_Click);
            // 
            // FormCorrector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(993, 506);
            this.Controls.Add(this.ButLanguage);
            this.Controls.Add(this.ExchangeLogGroupBox);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.GroupBoxStateHeter);
            this.Controls.Add(this.GroupBoxConnectHeter);
            this.Controls.Add(this.GroupBoxConnectPL);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(857, 454);
            this.Name = "FormCorrector";
            this.Text = "Corrector";
            this.Load += new System.EventHandler(this.FormCorrector_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAzimut)).EndInit();
            this.GroupBoxFileFreq.ResumeLayout(false);
            this.GroupBoxFileFreq.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxEPOStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxEPOStart)).EndInit();
            this.GroupBoxFreq.ResumeLayout(false);
            this.GroupBoxFreq.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxStepFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxFreqStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxFreqStart)).EndInit();
            this.PanelTypeReq.ResumeLayout(false);
            this.PanelTypeReq.PerformLayout();
            this.GroupBoxView.ResumeLayout(false);
            this.GroupBoxView.PerformLayout();
            this.GroupBoxStatistics.ResumeLayout(false);
            this.GroupBoxStatistics.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.TPageCalib.ResumeLayout(false);
            this.TPageCalib.PerformLayout();
            this.tStripWorkButton.ResumeLayout(false);
            this.tStripWorkButton.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ObjListViewCalibrarion)).EndInit();
            this.TPageAnalysis.ResumeLayout(false);
            this.TPageAnalysis.PerformLayout();
            this.GroupBoxStatisticsAnalys.ResumeLayout(false);
            this.GroupBoxStatisticsAnalys.PerformLayout();
            this.groupBoxStateAnalysResult.ResumeLayout(false);
            this.groupBoxStateAnalysResult.PerformLayout();
            this.TStripAnalysis.ResumeLayout(false);
            this.TStripAnalysis.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisAzimut)).EndInit();
            this.PanelTypeReqAnalys.ResumeLayout(false);
            this.PanelTypeReqAnalys.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ObjListViewAnalysis)).EndInit();
            this.GroupBoxFileFreqAnalysis.ResumeLayout(false);
            this.GroupBoxFileFreqAnalysis.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisEPOStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisEPOStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisEPOStart)).EndInit();
            this.GroupBoxFreqAnalysis.ResumeLayout(false);
            this.GroupBoxFreqAnalysis.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisFreqStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisFreqStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxAnalysisFreqStart)).EndInit();
            this.GroupBoxStateHeter.ResumeLayout(false);
            this.GroupBoxStateHeter.PerformLayout();
            this.GroupBoxConnectHeter.ResumeLayout(false);
            this.GroupBoxConnectHeter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxHeterWrite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxHeterRead)).EndInit();
            this.GroupBoxConnectPL.ResumeLayout(false);
            this.GroupBoxConnectPL.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxPLRead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxPLWrite)).EndInit();
            this.ExchangeLogGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LabAzimut;
        private System.Windows.Forms.NumericUpDown NumBoxAzimut;
        private System.Windows.Forms.RadioButton RadButFreq;
        private System.Windows.Forms.Label LabEPOStart;
        private System.Windows.Forms.GroupBox GroupBoxFileFreq;
        private System.Windows.Forms.Label LabStepFileFreq;
        private System.Windows.Forms.NumericUpDown NumBoxEPOStop;
        private System.Windows.Forms.Label LabEPOStop;
        private System.Windows.Forms.NumericUpDown NumBoxEPOStart;
        private System.Windows.Forms.GroupBox GroupBoxFreq;
        private System.Windows.Forms.Label LabStepFreq;
        private System.Windows.Forms.NumericUpDown NumBoxFreqStop;
        private System.Windows.Forms.Label LabFreqStop;
        private System.Windows.Forms.NumericUpDown NumBoxFreqStart;
        private System.Windows.Forms.Label LabFreqStart;
        private System.Windows.Forms.RadioButton RadButFileFreq;
        private System.Windows.Forms.Panel PanelTypeReq;
        private System.Windows.Forms.ProgressBar ProgressWorkCalibrarion;
        private System.Windows.Forms.CheckBox ChekBoxProgress;
        private System.Windows.Forms.CheckBox CheckShowRichBoxHex;
        private System.Windows.Forms.GroupBox GroupBoxView;
        private System.Windows.Forms.GroupBox GroupBoxStatistics;
        private System.Windows.Forms.Label LabCountBadFreq;
        private System.Windows.Forms.Label LabSumTime;
        private System.Windows.Forms.Label LabNumRestart;
        private System.Windows.Forms.RichTextBox RichBoxHex;
        private System.Windows.Forms.Button ButClear;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TPageCalib;
        private System.Windows.Forms.TabPage TPageAnalysis;
        private BrightIdeasSoftware.ObjectListView ObjListViewCalibrarion;
        public BrightIdeasSoftware.OLVColumn ColmChoice;
        public BrightIdeasSoftware.OLVColumn ColmRange;
        public BrightIdeasSoftware.OLVColumn ColmStep;
        public BrightIdeasSoftware.OLVColumn ColmResult;
        public BrightIdeasSoftware.OLVColumn ColmTime;
        private System.Windows.Forms.GroupBox GroupBoxConnectPL;
        private System.Windows.Forms.PictureBox PicBoxHeterRead;
        private System.Windows.Forms.PictureBox PicBoxHeterWrite;
        private System.Windows.Forms.Label LabHeterConnect;
        private System.Windows.Forms.PictureBox PicBoxPLRead;
        private System.Windows.Forms.PictureBox PicBoxPLWrite;
        private System.Windows.Forms.Label LabPLConnect;
        private System.Windows.Forms.Button ButPLConnect;
        private System.Windows.Forms.GroupBox GroupBoxConnectHeter;
        private System.Windows.Forms.Button ButHeterConnect;
        private System.Windows.Forms.ComboBox ComPortName;
        private System.Windows.Forms.ImageList ImagListResult;
        private BrightIdeasSoftware.MultiImageRenderer RendererResult;
        private System.Windows.Forms.ComboBox ComSpeed;
        private System.Windows.Forms.GroupBox GroupBoxStateHeter;
        private System.Windows.Forms.Label LabChargeHeter;
        private System.Windows.Forms.Label LabHumidHeter;
        private System.Windows.Forms.Label LabTempHeter;
        private System.Windows.Forms.ToolStrip tStripWorkButton;
        private System.Windows.Forms.ToolStripButton ButAddCalibrarionRange;
        private System.Windows.Forms.ToolStripButton ButDeleteCalibrarionRange;
        private System.Windows.Forms.ToolStripButton ButStartCalibrarion;
        private System.Windows.Forms.ToolStripButton ButStopCalibrarion;
        private System.Windows.Forms.ToolStripButton ButPauseCalibrarion;
        private System.Windows.Forms.ToolStripButton ButContinueCalibrarion;
        private System.Windows.Forms.ToolStripButton ButSendData;
        private System.Windows.Forms.Label LabRangeStop;
        private System.Windows.Forms.Label LabRangeStart;
        private BrightIdeasSoftware.ObjectListView ObjListViewAnalysis;
        public BrightIdeasSoftware.OLVColumn ColmFrequency;
        public BrightIdeasSoftware.OLVColumn ColmDirection;
        public BrightIdeasSoftware.OLVColumn ColmAnalysisResult;
        private System.Windows.Forms.ProgressBar ProgressAnalysis;
        private System.Windows.Forms.GroupBox groupBoxStateAnalysResult;
        private System.Windows.Forms.CheckBox CheckBoxProgressAnalysis;
        private System.Windows.Forms.CheckBox CheckShowDataExchange;
        private System.Windows.Forms.ToolStrip TStripAnalysis;
        private System.Windows.Forms.ToolStripButton ButAddAnalysisRange;
        private System.Windows.Forms.ToolStripButton ButDeleteAnalysisRange;
        private System.Windows.Forms.ToolStripButton ButStartAnalysis;
        private System.Windows.Forms.ToolStripButton ButStopAnalysis;
        private System.Windows.Forms.ToolStripButton ButPauseAnalysis;
        private System.Windows.Forms.ToolStripButton ButContinueAnalysis;
        private System.Windows.Forms.ToolStripButton ButSaveDataAnalysis;
        private System.Windows.Forms.Label LabAzimutAnalys;
        private System.Windows.Forms.NumericUpDown NumBoxAnalysisAzimut;
        private System.Windows.Forms.Panel PanelTypeReqAnalys;
        private System.Windows.Forms.RadioButton RadButAnalysisFreq;
        private System.Windows.Forms.RadioButton RadButAnalysisFileFreq;
        private System.Windows.Forms.GroupBox GroupBoxFileFreqAnalysis;
        private System.Windows.Forms.Label LabAnalysisRangeStop;
        private System.Windows.Forms.Label LabAnalysisRangeStart;
        private System.Windows.Forms.NumericUpDown NumBoxAnalysisEPOStep;
        private System.Windows.Forms.Label LabStepFileFreqAnalys;
        private System.Windows.Forms.NumericUpDown NumBoxAnalysisEPOStop;
        private System.Windows.Forms.Label LabEPOStopAnalys;
        private System.Windows.Forms.NumericUpDown NumBoxAnalysisEPOStart;
        private System.Windows.Forms.Label LabEPOStartAnalys;
        private System.Windows.Forms.GroupBox GroupBoxFreqAnalysis;
        private System.Windows.Forms.NumericUpDown NumBoxAnalysisFreqStep;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown NumBoxAnalysisFreqStop;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown NumBoxAnalysisFreqStart;
        private System.Windows.Forms.Label label9;
        private BrightIdeasSoftware.OLVColumn ColmDelta;
        private BrightIdeasSoftware.OLVColumn ColmHiddenRange;
        private System.Windows.Forms.Button ButRefresh;
        private System.Windows.Forms.Button ButSettings;
        private BrightIdeasSoftware.OLVColumn ColmHiddenStep;
        private System.Windows.Forms.GroupBox ExchangeLogGroupBox;
        private System.Windows.Forms.Button ButTestRadio;
        private System.Windows.Forms.ToolStripButton ButOnHeter;
        private System.Windows.Forms.ToolStripButton ButOffHeter;
        private System.Windows.Forms.ProgressBar ProgressChargeHeter;
        private System.Windows.Forms.GroupBox GroupBoxStatisticsAnalys;
        private System.Windows.Forms.Label labStd;
        private System.Windows.Forms.ToolStripButton ButClearAnalysis;
        private System.Windows.Forms.Button ButLanguage;
        private System.Windows.Forms.ComboBox NumBoxStepFileFreq;
        private System.Windows.Forms.NumericUpDown NumBoxStepFreq;
    }
}

