﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DspDataModel;
using DspProtocols;
using System.Drawing;
using System.Collections;

namespace Corrector
{
    partial class FormCorrector
    {
        AWPtoBearingDSPprotocolNew ClientServer = null;

        System.Threading.Timer tmWriteServer;
        System.Threading.Timer tmReadServer;
        static ushort usTimeIndicateServer = 300;

         void ConnectDisConnectServer()
        {
            //Подключение к серверу

            string IP = IniSettings.IniReadValue("Server", "IP");
            int port = Convert.ToInt32(IniSettings.IniReadValue("Server", "Port"));

            if (!IsConnectServer())//(ButPLConnect.BackColor == Color.Maroon)
            {
                ClientServer = new AWPtoBearingDSPprotocolNew();
                ClientServer.IsConnected += ClientServer_IsConnected;
                ClientServer.IsRead += ClientServer_IsRead;
                ClientServer.IsWrite += ClientServer_IsWrite;
                Task.Run(async () => await ClientServer.ConnectToBearingDSP(IP, port)).GetAwaiter();
            }
            else
            {
                try
                {
                    ClientServer.DisconnectFromBearingDSP();
                    ClientServer = null;
                }
                catch
                {
                    ClientServer = null;
                    ClientServer_IsConnected(false);
                }
            }


        }

        void ClientServer_IsWrite(bool isWrite)
        {
            Invoke((MethodInvoker)(() => PicBoxPLWrite.Image = Properties.Resources.green));
            Invoke((MethodInvoker)(() => tmWriteServer = new System.Threading.Timer(TimerWriteCallBackServer, null, usTimeIndicateServer, 0)));
        }

        void ClientServer_IsRead(bool isRead)
        {
            Invoke((MethodInvoker)(() => PicBoxPLRead.Image = Properties.Resources.red));
            Invoke((MethodInvoker)(() => tmReadServer = new System.Threading.Timer(TimerReadCallBackServer, null, usTimeIndicateServer, 0)));
        }

        void ClientServer_IsConnected(bool isConnected)
        {
            if (isConnected)
            {
                ShowColorButton(ButPLConnect, Color.Green);
                SetStartRelativeBearing();
            }
            else
            {
                ClientServer.IsConnected -= ClientServer_IsConnected;
                ClientServer.IsRead -= ClientServer_IsRead;
                ClientServer.IsWrite -= ClientServer_IsWrite;
                ShowColorButton(ButPLConnect, Color.Maroon);
                ClientServer = null;
            }
        }


        private void SetStartRelativeBearing()
        {
            //Запрос курсвого угла от сервера
            var answerCourseAngle = Task.Run(async () => await ClientServer.GetDirectionCorrection()).GetAwaiter().GetResult();
            //Обработка
            if (answerCourseAngle.DirectionCorrection != -1)
            {
                objParam.RelativeBearing = (int)Math.Round(answerCourseAngle.DirectionCorrection / 10.0d);
                    
            }
            else
            {
                Task.Run(async () => await ClientServer.SetDirectionCorrection(objParam.RelativeBearing, true)).GetAwaiter();
            }
        }
        private void TimerReadCallBackServer(object o)
        {
            if (PicBoxPLRead.InvokeRequired)
            {
                PicBoxPLRead.Invoke((MethodInvoker)(delegate()
                {
                    PicBoxPLRead.Image = Properties.Resources.gray;
                    tmReadServer.Dispose();
                }));

            }
            else
            {
                PicBoxPLRead.Image = Properties.Resources.gray;
                tmReadServer.Dispose();
            }
        }
        private void TimerWriteCallBackServer(object o)
        {
            if (PicBoxPLWrite.InvokeRequired)
            {
                PicBoxPLWrite.Invoke((MethodInvoker)(delegate()
                {
                    PicBoxPLWrite.Image = Properties.Resources.gray;
                    tmWriteServer.Dispose();
                }));

            }
            else
            {
                PicBoxPLWrite.Image = Properties.Resources.gray;
                tmWriteServer.Dispose();
            }
        }

        bool IsConnectServer() // проверка подключения к Серверу
        {
            if (ClientServer != null)
                return true;
            else
                return false;
        }
        string[] ErrorMess = new string[5]{
       /*0*/"запрос успешно выполнен", 
       /*1*/"нету соединения с сервером",
       /*2*/"невозможно обработать ответ от сервера",
       /*3*/"сервер находиться в неподходящем режиме для выполнения данного запроса",
       /*4*/"невалидные параметры запроса"}; //расшифровка ошибок сервера

        public struct KeyAzFreq
        {
            public int Freq { get; private set; }
            public int Azimut { get; private set; }
            public KeyAzFreq(int freq, int azimut)
            {
                Freq = (int)Math.Round(freq / 1000.0d) * 1000;
                Azimut = azimut;
            }
        } 

        Protocols.CalibrationCorrectionSignal[] LoadData()
        {
            Dictionary<KeyAzFreq, Protocols.CalibrationCorrectionSignal> BufDic = new Dictionary<KeyAzFreq, Protocols.CalibrationCorrectionSignal>();
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\\DATA" + @"\";
            if (!System.IO.Directory.Exists(path))
                return null;
            string[] listFiles = System.IO.Directory.GetFiles(path);
            int CountPhases = 10, CountElementsLine = 12;
            if (listFiles.Length == 0)
                return null;
            foreach (string pathFile in listFiles)
            {
                using (StreamReader fstream = new StreamReader(pathFile))
                {
                    string block;
                    while ((block = fstream.ReadLine()) != null)
                    {
                        Int16[] phase = new Int16[10];
                        string[] buf = block.Split(new char[] { ' ', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                        if (buf.Length < CountElementsLine)
                            continue;
                        Protocols.CalibrationCorrectionSignal temStruct = new Protocols.CalibrationCorrectionSignal();
                        temStruct.Frequency = Convert.ToInt32(buf[0]);
                        temStruct.Direction = Convert.ToInt16(buf[1]);
                        temStruct.Phases = new Int16[CountPhases];
                        for (int i = 0; i < temStruct.Phases.Length; i++)
                            temStruct.Phases[i] = Convert.ToInt16(buf[i + 2]);
                        KeyAzFreq key = new KeyAzFreq(temStruct.Frequency, temStruct.Direction);
                        if (BufDic.ContainsKey(key)) // BufDic.Keys.Contains(key)
                            BufDic[key] = temStruct;
                        else
                            BufDic.Add(new KeyAzFreq(temStruct.Frequency, temStruct.Direction), temStruct);
                    }
                }
            }
            Protocols.CalibrationCorrectionSignal[] MassCorrectData = new Protocols.CalibrationCorrectionSignal[BufDic.Count];
            
            MassCorrectData = BufDic.Values.ToArray();
            return MassCorrectData;
        }
    }
    
}
